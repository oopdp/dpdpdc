package biz.igg.oopdp.hellodroid.tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import java.util.ArrayList;

/**
 * Created by salvix on 19/01/18.
 */

public class GenericListViewAdapter extends ArrayAdapter<ListViewItem> {

    public GenericListViewAdapter(Context context, ArrayList<ListViewItem> CollectionReports) {
        super(context, 0, CollectionReports);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListViewItem cr = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(android.R.layout.simple_list_item_1, parent, false);
        }
        TextView label = convertView.findViewById(android.R.id.text1);
        label.setText(cr.presentLabelForView());
        return convertView;
    }
}