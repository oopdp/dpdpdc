package biz.igg.oopdp.hellodroid.tools;

import android.util.Log;

/**
 * Created by salvix on 04/01/18.
 */

public class Misc {

    public static void info(String tag, String message)
    {
        Log.i(tag, message);
    }

    public static void error(String tag, String message)
    {
        Log.e(tag, message);
    }

    public static void debug(String tag, String message)
    {
        Log.d(tag, message);
    }
}
