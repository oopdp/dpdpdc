package biz.igg.oopdp.hellodroid;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by salvix on 19/01/18.
 */

abstract public class MyAppCompatActivity extends AppCompatActivity {

    public static final String COLLECTION_RETRIEVAL_INTENT_ACTION = "biz.igg.oopdp.hellodroid.COLLECTION_RETRIEVAL_INTENT_ACTION";
    public static final String COLLECTION_RETRIEVAL_ID = "biz.igg.oopdp.hellodroid.COLLECTION_RETRIEVAL_ID";

    public void toast(String message) {
        Context context = getApplicationContext();
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }

}
