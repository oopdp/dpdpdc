package biz.igg.oopdp.hellodroid.tools;

/**
 * Created by salvix on 19/01/18.
 */

public interface ListViewItem {
    String presentLabelForView();
}
