package biz.igg.oopdp.hellodroid.tools;

/**
 * Created by salvix on 19/01/18.
 */

public class CollectionReport implements ListViewItem {


    public String id;
    public String name;
    public String content;

    public CollectionReport(String id, String name, String content) {
        this.id = id;
        this.name = name;
        this.content = content;
    }

    public String presentLabelForView() {
        return name;
    }
}
