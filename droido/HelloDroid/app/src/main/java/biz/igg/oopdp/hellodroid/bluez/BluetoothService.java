package biz.igg.oopdp.hellodroid.bluez;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.ActivityCompat;

import biz.igg.oopdp.hellodroid.tools.Misc;

import static android.support.v4.app.ActivityCompat.startActivityForResult;


/**
 * Created by salvix on 04/01/18.
 */

public class BluetoothService {

    private static final int REQUEST_ENABLE_BT = 2;
    private static final String LOG_TAG = BluetoothService.class.toString();

    public static BluetoothAdapter getAdapter()
    {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Misc.info(LOG_TAG, "Device doesn't support Bluetooth");
        }
        Misc.info(LOG_TAG, "Found Bluetooth support");
        return mBluetoothAdapter;
    }

    public static void enable(BluetoothAdapter mBluetoothAdapter, Activity activity)
    {
        if(mBluetoothAdapter != null) {
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(activity, enableBtIntent, REQUEST_ENABLE_BT, null);
            }
        }
    }

    public static void stopDiscovery(BluetoothAdapter mBluetoothAdapter) {
        if (mBluetoothAdapter.isDiscovering()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    public static void discover(BluetoothAdapter mBluetoothAdapter) {
        Misc.info(LOG_TAG, "Discovering devices...");
        stopDiscovery(mBluetoothAdapter);
        mBluetoothAdapter.startDiscovery();
    }

    public static void setBluetoothAdapterDiscoveryFinishedReceiver(Activity activity, BroadcastReceiver receiver)
    {
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        activity.registerReceiver(receiver, filter);
    }

    public static void setBluetoothAdapterDiscoveryStartedReceiver(Activity activity, BroadcastReceiver receiver)
    {
        IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        activity.registerReceiver(receiver, filter);
    }

    public static void setBluetoothDeviceFoundReceiver(Activity activity, BroadcastReceiver receiver)
    {
        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        activity.registerReceiver(receiver, filter);
    }

    public static void unregisterReceiver(Activity activity, BroadcastReceiver receiver) {
        activity.unregisterReceiver(receiver);
    }

}
