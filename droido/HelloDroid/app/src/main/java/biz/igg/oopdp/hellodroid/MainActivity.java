package biz.igg.oopdp.hellodroid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import biz.igg.oopdp.hellodroid.bluez.BluetoothService;
import biz.igg.oopdp.hellodroid.tools.Misc;

import static android.widget.Toast.*;

public class MainActivity extends MyAppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void startBluetoothActivity(View view) {
        Intent intent = new Intent(this, BluetoothActivity.class);
        startActivity(intent);
    }

    public void startCollectionRetrieval(View view) {
        toast("Downloading data...");
        EditText collectionEditText = (EditText) findViewById(R.id.collection_id);
        String message = collectionEditText.getText().toString();
        Intent intent = new Intent();
        intent.setAction(COLLECTION_RETRIEVAL_INTENT_ACTION);
        intent.putExtra(COLLECTION_RETRIEVAL_ID, message);
        sendBroadcast(intent);
    }






}
