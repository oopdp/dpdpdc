package biz.igg.oopdp.hellodroid;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import biz.igg.oopdp.hellodroid.bluez.BluetoothService;
import biz.igg.oopdp.hellodroid.tools.Misc;

public class BluetoothActivity extends MyAppCompatActivity {


    public static final int REQUEST_CODE_LOC = 1000;
    public ListView devicesListView;
    public ArrayAdapter<String> adapter;
    public List<String> devicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth);


        devicesListView = findViewById(R.id.devices_list);

        devicesList = new ArrayList<String>();


        adapter = new ArrayAdapter<String>(BluetoothActivity.this,
                android.R.layout.simple_list_item_1,
                devicesList
        );

        devicesListView.setAdapter(adapter);
    }

    public void startScan(View view) {
        toast("Bluetooth scan in progress...");
        Misc.info("BTZ", "Scanning here?");
        bzScan();
    }

    public void bzScan() {
        accessLocationPermission();

        BluetoothService.setBluetoothAdapterDiscoveryStartedReceiver(this, mReceiver);
        BluetoothService.setBluetoothAdapterDiscoveryFinishedReceiver(this, mReceiver);
        BluetoothService.setBluetoothDeviceFoundReceiver(this, mReceiver);

        BluetoothAdapter adapter = BluetoothService.getAdapter();
        BluetoothService.enable(adapter, this);
        BluetoothService.discover(adapter);

    }

    public void stopBzScan() {
        BluetoothAdapter adapter = BluetoothService.getAdapter();
        BluetoothService.stopDiscovery(adapter);
        BluetoothService.unregisterReceiver(this, mReceiver);
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Misc.info("BTZ", action);
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                String deviceDesc = deviceName + "@" + deviceHardwareAddress;
                appendText(deviceDesc);
                devicesList.add(deviceName);
                adapter.notifyDataSetChanged();
                Misc.info("BTZ", "FOUND " + deviceName + "@" + deviceHardwareAddress);
            } else if (BluetoothAdapter.ACTION_DISCOVERY_STARTED.equals(action)) {
                appendText("Discovery started...");
                Misc.info("BTZ", "Discovery started...");
            } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                appendText("Discovery completed...");
                Misc.info("BTZ", "Discovery completed!");
            }
        }
    };

    public void appendText(String message) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopBzScan();

    }

    private void accessLocationPermission() {
        int accessCoarseLocation = checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION);
//        int accessFineLocation   = checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION);

        List<String> listRequestPermission = new ArrayList<String>();

        if (accessCoarseLocation != PackageManager.PERMISSION_GRANTED) {
            listRequestPermission.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
//        if (accessFineLocation != PackageManager.PERMISSION_GRANTED) {
//            listRequestPermission.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
//        }

        if (!listRequestPermission.isEmpty()) {
            String[] strRequestPermission = listRequestPermission.toArray(new String[listRequestPermission.size()]);
            requestPermissions(strRequestPermission, REQUEST_CODE_LOC);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_LOC:
                if (grantResults.length > 0) {
                    for (int gr : grantResults) {
                        // Check if request is granted or not
                        if (gr != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                    }

                    //TODO - Add your code here to start Discovery

                }
                break;
            default:
                return;
        }
    }
}
