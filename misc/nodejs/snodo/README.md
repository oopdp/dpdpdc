Simple TODO app based on

https://scotch.io/tutorials/creating-a-single-page-todo-app-with-node-and-angular


Tools:

nodejs
angular
mongoose
mlab.com



express framework
mongoose orm
morgan logger

Actual deps:
    "express"    : "~4.7.2",
    "mongoose"   : "~3.6.2",
    "morgan"     : "~1.2.2",
    "body-parser": "~1.5.2",
    "method-override": "~2.1.2"


npm init...

npm install express --save
npm install mongoose --save
npm install morgan --save
npm install body-parser --save
npm install method-override --save




