var server = require("./server");
server.init();
server.start();
server.connect();

var Schema = server.mongoose.Schema;
var todoSchema = new Schema({
    text: String,
    completed: Boolean,
    completedAt: Date
},{
    timestamps: true
});

var Todo = server.mongoose.model('Todo', todoSchema);

server.app.get('/api/todos', function(req, res){
    Todo.find(function(err, todos){
        if(err){
            res.send(err);
        }
        res.json(todos);
    });
});

server.app.post('/api/todos/', function(req, res){
    Todo.create({
        text: req.body.text,
        completed: false,
        completedAt: null
    }, function(err, todo){
        if(err)
            res.send(err);
        res.json(todo);
    });
});

server.app.delete('/api/todos/:todo_id', function(req, res){
    Todo.remove({
        _id: req.params.todo_id
    },function(err, todo){
        if(err)
            res.send(err);
        res.json(todo);
    });
});
