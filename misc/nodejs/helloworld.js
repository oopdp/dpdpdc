console.log("hello world");

asyncForEach = async function (array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const waitFor = (ms) => new Promise(r => setTimeout(r, ms))

// var elements = [1, 2, 3];

// elements.forEach(async (num) => {
//   await waitFor(50)
//   console.log(num)
// });

// [1, 2, 3].forEach(async (num) => {
//   await waitFor(50)
//   console.log(num)
// });

console.log('Done 1');
//
// asyncForEach([1, 2, 3], async (num) => {
//   await waitFor(50)
//   console.log(num)
// })

const start = async () => {
  await asyncForEach([1,2,3], async (num) => {
    await waitFor(50),
    console.log(num)
  })
  console.log("Done")
}

start()
//
console.log('Done 2')
