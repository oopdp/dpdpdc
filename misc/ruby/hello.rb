require 'pry'

puts "Ruby Recap"


def hr
  puts "x" * 72
end

def sum(a,b)
  a+b
end

def sub(a,b)
  a-b
end

def a()
  "a"
end

module Foo
  module Bar
    class A
      def initialize
      end

      def self.a(a)
        r = "*" + a
        if a == "a" then
          r += "a"
        elsif a == "b"
          r += "b"
        else
          r += "*"
        end

      end

      def a(a)
        puts "b" + a
        b
      end

      def b
        __LINE__
      end
    end
  end
end

def fib(n)
  if n.zero?
    0
  elsif n == 1
    1
  else
    fib(n-1) + fib(n-2)
  end
end

puts hr
puts Foo::Bar::A.a "1"
puts Foo::Bar::A.a "a"
puts Foo::Bar::A.a "b"
puts Foo::Bar::A.a "c"

puts hr

puts a

a = Foo::Bar::A.new
puts a.a "h"

puts hr
puts fib(10)


module Stuff
  class Person
    attr_accessor :name, :age
    def full_info
      return "#{@name} is #{@age} years old"
    end
  end
end

class Integer
  def fib
    if self.zero?
      0
    elsif self == 1
      1
    else
      (self-1).fib + (self-2).fib
    end
  end
end

class Integer
  def aoc_1_0
    sum = 0
    chars = self.to_s.split ""
    chars.push chars[0]
    chars.each_with_index do |v,i|
      if v == chars[i+1]
        sum += chars[i+1].to_i
      end
    end
    sum
  end
  def aoc_1_1
    sum = 0
    chars = self.to_s.split ""
    offset = chars.length / 2
    chars += chars.slice 0, offset
    chars.each_with_index do |v,i|
      if v == chars[i+offset]
        sum += chars[i+offset].to_i
      end
    end
    sum
  end
end

module Stuff
  class A
    attr_reader :a

    def initialize
       @a = %w{a b c d e and some other words}
    end

    def m
      @a
    end
  end
end

def call_block
  puts "START"
  yield
  yield
  yield("i", "j", "k")
  puts "END"
end

module Stuff
  class Song
    def initialize(name, artist, duration)
      @name = name
      @artist = artist
      @duration = duration
    end

    def m
      @m = "M"
    end

    def to_s
      "Song #{@name} played by #{@artist} for #{@duration} seconds"
    end
  end

  class KaraokeSong < Song
    def initialize(name, artist, duration, lyrics)
      super(name, artist, duration)
      @lyrics = lyrics
    end
    def to_s
      super + " [#{@lyrics}]"
    end
  end

  class AnotherSong < KaraokeSong

    @another_var
    @@plays = 0

    def initialize(name, artist, duration, lyrics)
      super(name, artist, duration, lyrics)
      @another_var = 44
      @plays = 0
    end
    def another=(value)
      @another = value
    end
    def play
      @@plays += 1
      @plays += 1
    end

    def self.plays
      @@plays
    end
  end

  class MyLogga
    private_class_method :new
    @@logga = nil

    def self.create
      @@logga = new unless @@logga
      @@logga
    end
  end

  class Vat
    class SimpleVat
      attr_accessor :amount
      @amount = 0
      VAT_RATE = 20
      def calculate
        @amount * VAT_RATE / 100
      end
    end
  end

  class C
    protected
    def foo
      __LINE__
    end

    private
    def foop
      __LINE__
    end

    public
    def barr
      __LINE__
    end
  end

  class D < C
    def bar
      foo
    end
    def barf
      foop
    end
  end

  class E < D
  end
end

module Music
  class Song
    attr_reader :name, :artist, :duration

    def initialize(name, artist, duration)
      @name = name
      @artist = artist
      @duration = duration
    end

    def m
      @m = "M"
    end

    def to_s
      "Song #{@name} played by #{@artist} for #{@duration} seconds"
    end
  end
  class SongList
    def initialize
      @songs = Array.new
    end

    def append song
      @songs.push song
    end

    def delete_first
      @songs.shift
    end

    def delete_last
      @songs.pop
    end

    def [] index
      @songs[index]
    end

    def with_title title
      @songs.find { |song| title == song.name }
    end
    def to_s
      @songs.join ", "
    end

    def play
      "PLAY"
    end

    def pause
      "PAUSE"
    end
  end

  class Button
    attr_accessor :label
    def initialize label
      @label = label
    end
  end

  class Jukebox < SongList
  end

  class JukeboxButton < Button
    def initialize label, &action
      super(label)
      @action = action
    end

    def press
      @action.call
    end
  end
end

s1 = Music::Song.new "S1", "Queen", 659
s2 = Music::Song.new "S2", "Queen", 182
s3 = Music::Song.new "S3", "Queen", 243

pl1 = Music::Jukebox.new
pl1.append s1
pl1.append s2
pl1.append s3

playB = Music::JukeboxButton.new("PLAY") {pl1.play}
pauseB = Music::JukeboxButton.new("PAUSE") {pl1.pause}


binding.pry
