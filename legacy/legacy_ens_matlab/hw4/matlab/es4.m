% pulizia environment
clear all;
close all;
clc;

% coefficienti d (y(n))
num=[1 -1.9198 0.9252];
% coefficienti p (x(n))
den=[0.0108 -0.0168 0.0108];
% # campioni da calcolare
n_c=10;

% calcolo della risposta impulsiva:
[h,m]=impz(den,num,n_c);


new_num=0*ones(1,n_c);
for i=1:length(num)
	new_num(i)=new_num(i)+num(i);
end
new_den=0*ones(1,n_c);
for i=1:length(den)
	new_den(i)=new_den(i)+den(i);
end


y=new_den;
x=new_num;
my_h(1)=y(1)/x(1);
temp=0;

for i=2:n_c
	temp=0;
	for j=1:(i-1)
		temp=temp+my_h(j)*x(i-j+1);
	end
	my_h(i)=[y(i)-temp]/x(1);
end

% grafico risposta impulsiva mediante impz e sys_ident:
figure;
plot(m,h,m,my_h);
xlabel('n');                  
ylabel('h(n)');               
title('h(n) - Risp. implusiva del sistema');     
%~ legend('h(n) (impz)', 'h(n) (sys id)');
%~ grid;
my_h'
