% pulizia environment
clear all;
close all;
clc;

f_0=5e2;
Fs=44.1e3;

w_0=360*(f_0/Fs);

w_0_rad=w_0*2*pi/360;
zeros(1)=1*exp(j*w_0_rad);
zeros(2)=conj(zeros(1));

%~ Modulo dei poli
r=0.9986;
%~ r=0.7;
poles(1)=r*zeros(1);
poles(2)=conj(poles(1));
magic=360/(2*pi);
disp('Modulo di z_1,z_2');
abs(zeros(1))
disp('Fase di z_1,z_2 (gradi)');
angle(zeros(1))*magic
disp('Fase di z_1,z_2 (radianti)');
angle(zeros(1))
disp('Modulo di p_1,p_2');
abs(poles(2))
%~ disp('Fase di p_1,p_2');
%~ angle(poles(1))*magic


%~ k=0.9;
k=r;
num=k*poly(zeros);
den=poly(poles);
disp('Numeratore e denominatore trasformata z eq diff:');
num
den
[imp_resp,times]=impz(num,den);
N=1024;
[freq_resp,omega]=freqz(num,den,N);

%~ Modulo della risposta in frequenza
figure;
plot(omega, abs(freq_resp),omega,1/sqrt(2));
title('|H(e^{j\omega})|');
%~ axis([0 pi min(abs(freq_resp))-0.1 max(abs(freq_resp))+0.1]);
axis([0 pi -0.1 1.1]);
grid;

%~ Modulo della risposta in frequenza in dB
%~ figure;
%~ plot(omega, 20*log10(abs(freq_resp)));
%~ title('|H(e^{jw})|_{dB}');
%~ axis([0 pi min(20*log10(abs(freq_resp)))-10 max(20*log10(abs(freq_resp)))+10]);
%~ grid;

%~ Fase srotolata della risposta in frequenza
%~ figure;
%~ plot(omega, unwrap(phase(freq_resp)));
%~ title('arg_u H(e^{jw})');
%~ axis([0 pi min(phase(freq_resp))-1 max(phase(freq_resp))+1]);
%~ grid;

%~ Trasformata di Fourier della risposta impulsiva
% campioni fft e asse frequenze
%~ N campioni fisso!!!!!!
N=4000;
% modulo trasformata di x 
H = fftshift(fft(imp_resp,N));
F = -length(H)/2:length(H)/2-1;
F = -Fs/2:Fs/N:Fs/2-Fs/N;

figure;
plot(F,20*log10(abs(H)));
grid;
%~ axis([-length(H)/2 length(H)/2 min(20*log10(abs(H)))-10 max(20*log10(abs(H)))+10]);
%~ axis([-Fs/2 Fs/2 min(20*log10(abs(H)))-10 max(20*log10(abs(H)))+10]);
axis([-1000 1000 min(20*log10(abs(H)))-10 max(20*log10(abs(H)))+10]);
%~ legend('|H(j\Omega)|_{dB}');
title('|H(j\Omega)|_{dB} - modulo della trasformata del segnale originario');


%~ Fase della risposta in frequenza
%~ figure
%~ plot(omega, phase(freq_resp));
%~ title('arg H(e^{jw})');
disp('Zeri:');
roots(num)
disp('Poli:');
roots(den)

w_c_1_num=1+r^2-2*k*sqrt(2);
w_c_1_den=2*r-2*k*sqrt(2);
w_c_1=0.5*acos(w_c_1_num/w_c_1_den);

disp('Freq cut off w_c_1:');
w_c_1
