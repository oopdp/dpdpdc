% pulizia environment
clear all;
close all;
clc;

%~ Frequenza di taglio
f_c=3e3;
%~ Frequenza di campionamento
Fs=44.1e3;
%~ Pulsazione di taglio (naturale,radianti,normalizzata).

w_c_rad=(f_c/Fs)*2*pi

M_num=log10(sqrt(2)/2)
M_denom=log10(0.5*sqrt(2+2*cos(w_c_rad)))
M=M_num/M_denom;
disp('Ordine stimato del filtro:');
M=ceil(M)
%~ H_0(z)=1+z^-1
%~ num_s = [1 1];
%~ den_s = [2 0];

%~ num=[1];
%~ den=[1];
%~ for i=1:M 
	%~ num=conv(num,num_s);
	%~ den=conv(den,den_s);
%~ end

% Diagramma zeri e poli
%~ Da num, den a zeri e poli
%~ [z, p] = tf2zp(num, den);

%~ Da num, den a zeri e poli
vect=ones(M,1);
z=-1*vect;
p=0*vect;
[num, den] = zp2tf(z, p, (1/2)^M);

figure;
zplane(z, p);
title('Diagramma Poli e zeri di H_0(z)=((z+1)/(2z))^M')
ylabel('Asse immaginario');
xlabel('Asse reale');

%~ Calcolo della risposta in frequenza del filtro
[freq_resp,omega]=freqz(num,den);
%~ LINEA DI RIFERIMENTO:
ref_line=1/sqrt(2)*ones(length(freq_resp),1);

%~ PER LAVORARE CON LE PULSAZIONI NORMALIZZATE!
omega=omega*(1/pi);

%~ Modulo della risposta in frequenza
figure;
plot(omega, abs(freq_resp), omega, ref_line);
hold on;
title('|H(e^{j\omega})|');
%~ axis([0 pi min(abs(freq_resp)) max(abs(freq_resp))]);
axis([0 1 0 1]);
grid;

%~ Modulo della risposta in frequenza in dB
figure;
plot(omega, 20*log10(abs(freq_resp)), omega, 20*log10(ref_line));
title('|H(e^{j\omega})|_{dB}');
axis([0 1 min(20*log10(abs(freq_resp)))-10 max(20*log10(abs(freq_resp)))+10]);
grid;

%~ Fase srotolata della risposta in frequenza
figure;
plot(omega, unwrap(phase(freq_resp)));
title('arg_u H(e^{j\omega})');
axis([0 1 min(phase(freq_resp))-1 max(phase(freq_resp))+1]);
grid;

%~ Delay di gruppo
[group_delay, w]=grpdelay(num,den);
w=w*(1/pi);
figure;
plot(w,group_delay);
axis([0 1 7.5 8.5]);
title('Group Delay H(e^{j\omega})');

