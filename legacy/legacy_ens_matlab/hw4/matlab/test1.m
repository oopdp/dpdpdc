% pulizia environment
clear all;
close all;
clc;


%~ Frequenza di taglio
f_0=500;
%~ f_0=10e3;
%~ Frequenza di campionamento
Fs=44.1e3;
%~ Pulsazione di taglio (naturale,radianti,normalizzata).
w_0_rad=(f_0/Fs)*2*pi

%~ w_0_rad=0;

zeros(1)=1*exp(j*w_0_rad);
zeros(2)=conj(zeros(1));

%~ Modulo dei poli
r=0.7;
poles(1)=r*zeros(1);
poles(2)=conj(poles(1));
magic=360/(2*pi);
disp('Modulo di z_1,z_2');
abs(zeros(1))
disp('Modulo di p_1,p_2');
abs(poles(2))

disp('Fase di z_1,z_2,p_1,p_2 (gradi)');
angle(zeros(1))*magic
disp('Fase di z_1,z_2,p_1,p_2 (radianti)');
angle(zeros(1))

%~ disp('Fase di p_1,p_2');
%~ angle(poles(1))*magic

%~ k=0.9;
k=1;
num=k*poly(zeros);
den=poly(poles);
disp('Numeratore e denominatore trasformata z eq diff:');
num
den
%~ [imp_resp,times]=impz(num,den);
%~ N campioni risposta in frequenza
N=1024;
%~ Risposta in frequenza  e dominio
[freq_resp,omega]=freqz(num,den,N);
%~ freq_resp
%~ omega
%~ Passo freqz
%~ 3.14/512

%~ (i*pi)/512 = w_d

%~ Modulo della risposta in frequenza
%~ figure;
%~ plot(omega, abs(freq_resp));
%~ title('|H(e^{j\omega})|');
%~ axis([0 pi min(abs(freq_resp))-0.1 max(abs(freq_resp))+0.1]);
%~ grid;


%~ VALUTAZIONE RISPOSTA IN FREQUENZA
%~ w_d=w_0_rad;
%~ disp('Modulo quadro a w_d radianti:')
%~ i=ceil(w_d*N/pi);
%~ val=abs(freq_resp(i))^2;

%~ MIA FORMULA MODULO QUADRO
%~ term=[(1+r^2)-2*r*cos(w_0 -w)]
w_val=omega;
%~ my_mod_num=k^2*[2-2*r*cos(w_0_rad -w_val)]*[2-2*r*cos(-w_0_rad -w_val)];
my_mod_num=k^2*[2-2*cos(w_0_rad -w_val)].*[2-2*cos(w_0_rad + w_val)];
my_mod_denum=[(1+r^2)-2*r*cos(w_0_rad -w_val)].*[(1+r^2)-2*r*cos(w_0_rad + w_val)];
my_mod=my_mod_num./my_mod_denum;

%~ STIMA RADICE DI A
%~ w_d=0;
%~ disp('Modulo quadro a w_d radianti:')
%~ i=ceil(w_d*N/pi)+1;
%~ a=abs(freq_resp(i))^2;



%~ MIA FORMULA STIMA K at 0
my_k_num=(1+r^2)-2*r*cos(w_0_rad);
my_k_denum=2-2*cos(w_0_rad);
my_k=my_k_num./my_k_denum;
disp('Il k da me stimato:');
my_k

%~ MIA FORMULA STIMA K at pi
my_k_num_end=[(1+r^2)-2*r*cos(w_0_rad-pi)].*[(1+r^2)-2*r*cos(w_0_rad+pi)];
my_k_denum_end=[2-2*cos(w_0_rad-pi)].*[2-2*cos(w_0_rad-pi)];
my_k_end=sqrt(my_k_num_end./my_k_denum_end);
disp('Il k finale da me stimato:');
my_k_end

lambda=w_0_rad/pi
k_end=((1-lambda)*my_k_end +lambda*my_k)
k_end=r
%~ RISPOSTA IN FREQUENZA COL MIO K
my_num=k_end*poly(zeros);
my_den=poly(poles);
disp('Numeratore e denominatore trasformata z eq diff:');
[my_freq_resp,my_omega]=freqz(my_num,my_den,N);

figure;
%~ plot(omega, abs(freq_resp).^2, omega, 1.01*my_mod);
plot(omega, abs(freq_resp),'m');
legend('Freq resp k=1');
hold on;
plot(my_omega, abs(my_freq_resp));
legend('Freq resp k=1','Freq resp my_k');

title('My Freq Resp');
hold off;

%~ axis([0 pi min(abs(freq_resp))-0.1 max(abs(freq_resp))+0.1]);
grid;



