% pulizia environment
clear all;
close all;
clc;


z=[1/4 4]
p=[0 0]


[num_h, den_h] = zp2tf(z', p', 1);
[num_h_inv, den_h_inv] = zp2tf(p', z', 1);

figure;
zplane(poly(z), poly(p));
title('Diagramma Poli e zeri di H(z)');
ylabel('Asse immaginario');
xlabel('Asse reale');


figure;
zplane(poly(p), poly(z));
title('Diagramma Poli e zeri di H_{inv}(z)');
ylabel('Asse immaginario');
xlabel('Asse reale');

[res,poles,proper]=residuez(num_h_inv,den_h_inv)

alpha=res(1);
beta=-res(2);

