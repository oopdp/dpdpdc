% deconvoluzione.m
%
% Esempio di determinazione del' ingresso e di identificazione del sistema tramite deconvoluzione 
%
% 

close all; clear all;

% 1) Generazione di ingresso x(n), filtro h(n)  e uscita y(n)

% Genero un filtro passa basso (la funzione ellip ed il suo funzionamento
% sara' spiegato in seguito)
% (ordine N = M = 4, "ripple" in banda passante 1 dB
% attenuazione 40 dB e banda passante di Fs/5)

[b, a] = ellip(4, 1, 40, (1/5)/2);

% Genero un segnale in ingresso
f0 = 1/30; %frequenz nella banda passante del filtro paassbasso
f1 = 3/5;  %frequenz nella banda attenuata del filtro paassbasso

n = 0:1:1000;
x = 2*sin(2*pi*f0*n) + cos(2*pi*f1*n);

% Lo filtro con il filtro di ordine 4 in forma diretta 
y = filter(b, a, x);

% Visualizzo i segnali di ingresso e uscita
figure(1)
plot(n, x, 'b', n, y, 'r');
legend('ingresso', 'uscita (forma diretta)');
title (' ingresso=2*sin(2*pi*f0*n) + cos(2*pi*f1*n) con f0 = 1/30 e f1 = 3/5');
axis([0 150 -2.1 2.1])
grid;
xlabel('n');
ylabel('ampiezza');

% Determino la risposta in frequenza e realtiva risposta impulsiva

[H, f] = freqz(b, a, 1024, 1, 'whole');
h=impz(b,a,100);         % calcolo 100 campioni della risposta impulsiva

% Visualizzo la risposta in frequenza
figure

subplot(3,1,1);
plot(f, 20*log10(abs(H)));
title ('Fc=1') 
grid
xlabel('f (Hz)')
ylabel('|H(f)| (dB)');

subplot(3,1,2);
plot(f, unwrap(angle(H))/pi);
grid
xlabel('f (Hz)')
ylabel('angle(H(f))/\pi (rad)');

subplot(3,1,3);
plot(f, grpdelay (b, a, 1024,'whole'));
grid
xlabel('f (Hz)')
ylabel('Group delay (H(f))(s)');

% 2)  Stima dell' ingresso x(n) per deconvoluzione

[x1,R] = deconv(y,h);

% Visualizzo i segnali x e x1
n=n(1:length(x1));
x=x(1:length(x1));
e=x-x1;


figure
plot(n, e, 'b');
title('errore tra ingresso e ingresso ottenuto per deconvoluzine');
axis([0 length(x1) min(e) max(e)])
grid;
xlabel('n');
ylabel('e(n)');

% 3) Identificazione del sistema  h(n) da ingresso e uscita

[h1,R] = deconv(y,x);

% Visualizzo le rispote impulsive h e h1

n=n(1:length(h1));
h=h(1:length(h1));
e1= h' - h1 ;

figure
plot(n, e1, 'b');
title('errore tra h e h1 ottenuto per deconvoluzine');
axis([0 length(h1) min(e1) max(e1)])
grid;
xlabel('n');
ylabel('e1(n)');

