% TRACCIA Es_5_HWK4.m
% P.B e P.A elementari 

clear all;                      % Pulisce il workspace
clc;                            % Pulisce il command window
close all;                      % Chiude le figure

% 1) Costruzione di un segnale: sviluppo in serie di onda quadra troncato all'ordine N

Fc = 10000; Tc = 1 / Fc;        % Frequenza e periodo di campionamento
n=0:100;
n=0:100;
t = n * Tc;                     % Base temporale in secondi

f0 = 100;                       % Frequenza della fondamentale
N = 10;                         % Ordine dello sviluppo in serie

% Sviluppo in serie troncato all'ordine N
x = zeros(1, length(n));
for k = 0:N
   x = x + cos(2*pi*(2*k+1)*f0*n*Tc) * (-1)^k / (2*k+1);
end
x = x * 4 / pi;


plot(t*1000, x);                % mostra 1 periodo del segnale
grid on;                        % Disegna la griglia
xlabel('tempo (ms)');           % Etichetta dell'asse x
ylabel('s(t)');                 % Etichetta dell'asse y
title('Sviluppo in serie di un''onda quadra')    % Titolo della figura

% 2) pasabasso elementare del 1 ordine 

% Progetto
fC=100;                          %frequenza di taglio
omegaC= 2*pi*fC/Fc;              %pulsazione normalizzata di taglio
alpha=(1-sin(omegaC))/cos(omegaC);
%~ 
%~ fC=100;                          %frequenza di taglio
%~ omegaC= 2*pi*fC/Fc;              %pulsazione normaizzata di taglio
%~ 
%~ alpha= 1 - omegaC ;

% Funzione di trasferimento del passabasso


num=(1-alpha)*[1 1];
den=2*[1 -alpha];

figure;
zplane(roots(num), roots(den));
title('Diagramma Poli e zeri di H(z)');
ylabel('Asse immaginario');
xlabel('Asse reale');


% Determino la risposta in frequenza e relativa risposta impulsiva
N=1024;
[H,f]=freqz(num,den,N);


% Visualizzo la risposta in frequenza
figure

subplot(3,1,1);
plot(f, 20*log10(abs(H)));
title ('Fc=1') 
grid
xlabel('f (Hz)')
ylabel('|H(f)| (dB)');

subplot(3,1,2);
plot(f, unwrap(angle(H))/pi);
grid
xlabel('f (Hz)')
ylabel('angle(H(f))/\pi (rad)');

subplot(3,1,3);
plot(f, grpdelay (num, den, N,'whole'));
grid
xlabel('f (Hz)')
ylabel('Group delay (H(f))(s)');

% Filtra il segnale 
[y,sf] = filter (num,den,x);

figure;
plot(t*1000, y);                % mostra 1 periodo del segnale
grid on;                        % Disegna la griglia
xlabel('tempo (ms)');           % Etichetta dell'asse x
ylabel('s(t)');                 % Etichetta dell'asse y
title('Prima armonica di un''onda quadra')    % Titolo della figura


% 3) pasa-altoelementare
num=(1-alpha)*[1 1];
den=2*[1 -alpha];

N=1024;
[H,f]=freqz(den,num,N);


% Visualizzo la risposta in frequenza
figure

subplot(3,1,1);
plot(f, 20*log10(abs(H)));
title ('Fc=1') 
grid
xlabel('f (Hz)')
ylabel('|H(f)| (dB)');

subplot(3,1,2);
plot(f, unwrap(angle(H))/pi);
grid
xlabel('f (Hz)')
ylabel('angle(H(f))/\pi (rad)');

subplot(3,1,3);
plot(f, grpdelay (num, den, N,'whole'));
grid
xlabel('f (Hz)')
ylabel('Group delay (H(f))(s)');

% Filtra il segnale 
[y,sf] = filter (den,num,x);

figure;
plot(t*1000, y);                % mostra 1 periodo del segnale
grid on;                        % Disegna la griglia
xlabel('tempo (ms)');           % Etichetta dell'asse x
ylabel('s(t)');                 % Etichetta dell'asse y
title('Ultima armonica di un''onda quadra')    % Titolo della figura


