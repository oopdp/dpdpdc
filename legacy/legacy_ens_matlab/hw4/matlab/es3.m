% pulizia environment
clear all;
close all;
clc;

%~ Frequenza di taglio
f_c=3e3;
%~ Frequenza di campionamento
Fs=44.1e3;
%~ Pulsazione di taglio (naturale,radianti,normalizzata).


z=poly([1/4 4]);
%~ z=[1 2 3 4 5 6 7 8 9 10];
p=[1];
[num_h, den_h] = zp2tf(z', p', 1);
[num_h_inv, den_h_inv] = zp2tf(p', z', 1);

disp('Coefficienti equazione alle differenze H(z):');
num_h 
den_h

disp('Coefficienti equazione alle differenze H_1(z):');
num_h_inv
den_h_inv

figure;
zplane(z, p);
title('Diagramma Poli e zeri di H(z)');
ylabel('Asse immaginario');
xlabel('Asse reale');


figure;
zplane(p, z);
title('Diagramma Poli e zeri di H_{inv}(z)');
ylabel('Asse immaginario');
xlabel('Asse reale');

%~ Calcolo della risposta in frequenza del filtro
[freq_resp,omega]=freqz(num_h,den_h);
[freq_resp_inv,omega_inv]=freqz(num_h_inv,den_h_inv);


%~ PER LAVORARE CON LE PULSAZIONI NORMALIZZATE!
%~ omega=omega*(1/pi);

%~ Modulo della risposta in frequenza
figure;
plot(omega, abs(freq_resp));
title('|H(e^{j\omega})|');
axis([0 pi min(abs(freq_resp)) max(abs(freq_resp))]);
grid;

%~ Modulo della risposta in frequenza filtro inverso
figure;
plot(omega_inv, abs(freq_resp_inv));
title('|H_{inv}(e^{j\omega})|');
axis([0 pi min(abs(freq_resp_inv)) max(abs(freq_resp_inv))]);
grid;

%~ Modulo della risposta in frequenza in dB
%~ figure;
%~ plot(omega, 20*log10(abs(freq_resp)), omega, 20*log10(ref_line));
%~ title('|H(e^{j\omega})|_{dB}');
%~ axis([0 1 min(20*log10(abs(freq_resp)))-10 max(20*log10(abs(freq_resp)))+10]);
%~ grid;

%~ Fase srotolata della risposta in frequenza
figure;
plot(omega, unwrap(phase(freq_resp)));
title('arg_u H(e^{j\omega})');
axis([0 pi min(phase(freq_resp))-1 max(phase(freq_resp))+1]);
grid;

%~ Fase srotolata della risposta in frequenza del filtro inverso
figure;
plot(omega_inv, unwrap(phase(freq_resp_inv)));
title('arg_u H_{inv}(e^{j\omega})');
axis([0 pi min(phase(freq_resp_inv))-1 max(phase(freq_resp_inv))+1]);
grid;

%~ Delay di gruppo
%~ [group_delay, w]=grpdelay(num,den);
%~ w=w*(1/pi);
%~ figure;
%~ plot(w,group_delay);
%~ axis([0 1 7.5 8.5]);
%~ title('Group Delay H(e^{j\omega})');
%~ 

