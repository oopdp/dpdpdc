clear all;
close all;
clc;

step=1e-2;
ext=1.06;
%~ Definzione intervalli polinomi di chebyshev
w_sx=-ext:step:-1-step;
w_center=-1:step:1;
w_dx=1+step:step:ext;


%~ for i=1:6
	%~ N=i;
	%~ cheb_center=cos(N*acos(w_center));
	%~ cheb_dx=cosh(N*acosh(w_dx));
	%~ cheb_sx=cosh(N*acosh(w_sx));
%~ 
	%~ w=[w_sx w_center w_dx];
	%~ cheby=[cheb_sx cheb_center cheb_dx];
%~ 
	%~ plot(w, real(cheby));
	%~ hold on;
%~ end

figure;
N=1;

cheb_center=cos(N*acos(w_center));
cheb_dx=cosh(N*acosh(w_dx));
cheb_sx=cosh(N*acosh(w_sx));
w=[w_sx w_center w_dx];
cheby=[cheb_sx cheb_center cheb_dx];
plot(w, real(cheby), 'k');
hold on;

N=2;
cheb_center=cos(N*acos(w_center));
cheb_dx=cosh(N*acosh(w_dx));
cheb_sx=cosh(N*acosh(w_sx));
w=[w_sx w_center w_dx];
cheby=[cheb_sx cheb_center cheb_dx];
plot(w, real(cheby));

N=3;
cheb_center=cos(N*acos(w_center));
cheb_dx=cosh(N*acosh(w_dx));
cheb_sx=cosh(N*acosh(w_sx));
w=[w_sx w_center w_dx];
cheby=[cheb_sx cheb_center cheb_dx];
plot(w, real(cheby),'m');

N=4;
cheb_center=cos(N*acos(w_center));
cheb_dx=cosh(N*acosh(w_dx));
cheb_sx=cosh(N*acosh(w_sx));
w=[w_sx w_center w_dx];
cheby=[cheb_sx cheb_center cheb_dx];
plot(w, real(cheby),'r');

N=5;
cheb_center=cos(N*acos(w_center));
cheb_dx=cosh(N*acosh(w_dx));
cheb_sx=cosh(N*acosh(w_sx));
w=[w_sx w_center w_dx];
cheby=[cheb_sx cheb_center cheb_dx];
plot(w, real(cheby),'g');

%~ N=6;
%~ cheb_center=cos(N*acos(w_center));
%~ cheb_dx=cosh(N*acosh(w_dx));
%~ cheb_sx=cosh(N*acosh(w_sx));
%~ w=[w_sx w_center w_dx];
%~ cheby=[cheb_sx cheb_center cheb_dx];
%~ plot(w, real(cheby),'y');
%~ 
%~ N=7;
%~ cheb_center=cos(N*acos(w_center));
%~ cheb_dx=cosh(N*acosh(w_dx));
%~ cheb_sx=cosh(N*acosh(w_sx));
%~ w=[w_sx w_center w_dx];
%~ cheby=[cheb_sx cheb_center cheb_dx];
%~ plot(w, real(cheby),'b');


%~ Assi grafico
c_min=min(real(cheby))-1;
c_max=max(real(cheby))+1;
axis([-ext ext c_min c_max]);


line([-ext ext],[1 1],'LineStyle','-','Color','k');
line([-ext ext],[-1 -1],'LineStyle','-','Color','k');
line([-1 -1],[c_min c_max],'LineStyle','-','Color','k');
line([1 1],[c_min c_max],'LineStyle','-','Color','k');

grid;
