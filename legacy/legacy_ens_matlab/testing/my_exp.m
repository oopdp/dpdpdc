% pulizia environment
clear all;
close all;
clc;

%~ Parametri
Fc = 10^3;
T = 1/Fc;
r_0 = 0.8;
w_0 = atan(0.75);
b_0=1;


%~ Supporto
w=0:T:2*pi;

%~ Definizione analitica
%~ h_1=1-r_0*exp(j*w_0)*exp(-j*w);
%~ h_2=1-r_0*exp(-j*w_0)*exp(-j*w);

w_0=pi/2;
%~ w_1=pi-w_0;
w_2=pi/2;

r_num = 1;
r_den = 0.99;
%~ h_tubo=(1./(1 - r_0*exp(j*w_2)*exp(-j*w)));

%~ num=(1 - r_num*exp(j*w_0)*exp(-j*w)).*(1 - r_num*exp(j*w_1)*exp(-j*w));
num=(1 - r_num*exp(j*w_0)*exp(-j*w));
den=(1 - r_den*exp(j*w_2)*exp(-j*w));

h_tubo=num./den;

%~ h_tubo=(1 - r_0*exp(j*w_0)*exp(-j*w));

%~ h=abs(1-r_0*exp(j*w_0)*exp(-j*w)).^2;
%~ h=abs(b_0./(1-r_0*exp(j*w_0)*exp(-j*w))).^2;

h=abs(h_tubo);
%~ Stimato manualmente
%~ man_h=1+r_0^2-2*r_0*cos(w - w_0);

figure;

%~ subplot(2,1,1);
%~ plot(w, man_h);
%~ axis([0 , 2*pi, min(man_h), max(man_h)]);
%~ xlabel('\omega');                  
%~ ylabel('|H(e^{j\omega})|^2');               
%~ title('Modulo quadro risposta in frequenza stimato a mano');     
%~ legend('|H(e^{j\omega})|^2');


   
%~ subplot(2,1,2);
plot(w, h);               
axis([0 , pi, min(h)-1/10*max(h), max(h)+1/10*max(h)]);
line([w_0 w_0], [0 max(h)], 'Color', 'r', 'Marker', '.','LineStyle', '-', 'LineWidth', 2);
line([0 2*pi], [0 0], 'Color', 'black', 'LineStyle', '--', 'LineWidth', 1);
xlabel('\omega');                  
ylabel('|H(e^{j\omega})|^2');               
title('Modulo quadro risposta in frequenza stimato con matlab');     
legend('|H(e^{j\omega})|^2');
