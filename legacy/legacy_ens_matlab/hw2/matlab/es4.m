close all
clear all
clc

% ex 6.11
% X(z)=z/(z-1)^2
disp('es 6.11');
num=[1 0];
denom=poly([1 1]);
[r,p,k]=residuez(num,denom)

% ex 6.12
% X(z)=0.5z/z^2-z+0.25
disp('es 6.12');
num=[0.5 0];
denom=[1 -1 0.25];
[r,p,k]=residuez(num,denom)

% ex 6.14
% X(z)=1+2.0z^-1/(1-0.2z^-1)(1+0.6z^-1)
disp('es 6.14');
num=[1 2];
denom=poly([0.2 -0.6]);
[r,p,k]=residuez(num,denom)

% ex 6.15
disp('es 6.15 vedi es 6.14');

% ex 6.16
% X(z)=z^3/18z^3+3z^2-4z-1
disp('es 6.16');
num=[18 0 0 0];
denom=[18 3 -4 -1];
[r,p,k]=residuez(num,denom);

% ex 6.17
% da residui a polinomio
r=[0.36 0.24 0.4];
p=[0.5 -0.3333 -0.3333];
k=[0];
[num,denom]=residuez(r,p,k)

% ex 6.18
% espansione in serie di potenze
disp('es 6.18');
%num=poly([0]);
num=[0 1];
denom=poly([1 1]);
[h,n]=impz(num,denom);

% grafico risposta impulsiva:
figure;
stem(n, h);               
xlabel('n');                  
ylabel('h(n)');               
title('Risp. implusiva del sistema');     
legend('h(n)');

% ex 6.19
% espansione in serie di potenze
disp('es 6.19');
%num=poly([0]);
num=[1 2];
denom=[1 0.4 -0.12];
[h,n]=impz(num,denom);

% grafico risposta impulsiva:
figure;
stem(n, h);               
xlabel('n');                  
ylabel('h(n)');               
title('Risp. implusiva del sistema');     
legend('h(n)');


% ex 6.20
% grafico risp impulsiva
disp('es 6.20');
%num=poly([0]);
num=[1 2];
denom=[1 0.4 -0.12];
[h,n]=impz(num,denom);
h

% ex 6.34
% f di trasferimento
disp('es 6.34');
num=[1 -1.2 1];
denom=[1 -1.3 1.04 -0.222];
[z,p,k]=tf2zp(num,denom);
zplane(z,p);
