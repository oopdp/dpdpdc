close all;
clear all;
clc;

n=1000;
num_camp=5000;
num_camp_y=5e5;

[s,f_c,Nbits]=wavread('bach_gould.wav');

% Assi frequenze
freq_h=-f_c / 2 : f_c /num_camp: f_c / 2 - f_c / num_camp;
freq_y=-f_c / 2 : f_c /num_camp_y: f_c / 2 - f_c / num_camp_y;

%numero di bit di quantizzazione
nq=3;
%valore di fondoscala del quantizzatore
mx=max(s);
mn=min(abs(s));
M=max(mx,mn);
%passo di quantizzazione
delta=2 * M / (2^(nq+1));

%approssimazione per arrotondamento
s_quant_arr=delta * round(s / delta);
%approssimazione per troncamento
s_quant_tronc=delta * fix(s / delta);

%tf Fourier
S_quant_arr=fftshift(fft(s_quant_arr));
S_quant_tronc=fftshift(fft(s_quant_tronc));

%calcolo del rumore di quantizzazione 
%caso arrotondamento
err_quant_arr=s_quant_arr - s;
%caso troncamento
err_quant_tronc=s_quant_tronc - s;

%Fourier rumore
%arrotondamento
E_quant_arr=fftshift(fft(err_quant_arr));
%troncamento
E_quant_tronc=fftshift(fft(err_quant_tronc));

%SNR
%calcolo della potenza del segnale 
Ts=length(s);
Es=sqrt(norm(s));
Ps= Es / Ts;

%potenza rumore di quantizzazione
%caso arrotondamento
Perr_quant_arr=var(err_quant_arr);
%caso troncamento
Perr_quant_tronc=var(err_quant_tronc);

%stima del SNR_q
%caso arrotondamento
SNR_quant_arr=10 * log10(Ps / Perr_quant_arr);
%caso troncamento
SNR_quant_tronc=10 * log10(Ps / Perr_quant_tronc);

p=[0.0108 -0.0168 0.0108];
d=[1 -1.9198 0.9252];
y_a=filter(p,d,s_quant_arr);
y_t=filter(p,d,s_quant_tronc);

%scrittura file 
wavwrite(s_quant_arr,f_c,Nbits,'bach_gould_sqa.wav');
wavwrite(s_quant_tronc,f_c,Nbits,'bach_gould_sqt.wav');
wavwrite(y_a,f_c,Nbits,'bach_gould_sqaf');
wavwrite(y_t,f_c,Nbits,'bach_gould_sqtf');

%riproduzione dei file
soundsc(s_quant_arr,f_c,Nbits);
soundsc(s_quant_tronc,f_c,Nbits);
soundsc(y_a,f_c,Nbits);
soundsc(y_t,f_c,Nbits);

%risposta impulsiva
h=impz(p,d,n);

%trasformata di Fourier segnali e risp imp
Y_a=fftshift(fft(y_a,num_camp_y));
Y_t=fftshift(fft(y_t,num_camp_y));
H=fftshift(fft(h,num_camp));

%risposta in frequenza
figure;
plot(freq_h,abs(H));
axis([-5000 5000 min(abs(H)) max(abs(H))]);
xlabel('\Omega');
ylabel('|H(i\Omega)|');
title('Modulo della risposta in frequenza');
grid;

%segnali filtrati
figure;
plot(freq_y,abs(Y_a));
axis([-5000 5000 min(abs(Y_a)) max(abs(Y_a))]);
xlabel('j\Omega');
ylabel('|Ya(i\Omega)|');
title('Modulo della trasformata di Fourier del segnale filtrato (arrotondamento)');
grid;

figure;
plot(freq_y,abs(Y_t));
axis([-5000 5000 min(abs(Y_t)) max(abs(Y_t))]);
xlabel('j\Omega');
ylabel('|Yt(i\Omega)|');
title('Modulo della trasformata di Fourier del segnale filtrato (troncamento)');
grid;

%conv caso arrotondamento
err_quant_arr_conv_risp=conv(err_quant_arr,h);
%conv caso troncamento
err_quant_tronc_conv_risp=conv(err_quant_tronc,h);
%potenza
Supp_err_quant_arr_conv_risp=length(err_quant_arr_conv_risp);
Supp_err_quant_tronc_conv_risp=length(err_quant_tronc_conv_risp);

Energia_err_quant_arr_conv_risp=sqrt(norm(err_quant_arr_conv_risp));
Energia_err_quant_tronc_conv_risp=sqrt(norm(err_quant_tronc_conv_risp));

Potenza_err_quant_arr_conv_risp=Energia_err_quant_arr_conv_risp / Supp_err_quant_arr_conv_risp;
Potenza_err_quant_tronc_conv_risp=Energia_err_quant_tronc_conv_risp / Supp_err_quant_tronc_conv_risp;

%potenza y
Supp_y_a=length(y_a);
Energia_y_a=sqrt(norm(y_a));
Potenza_y_a=Energia_y_a / Supp_y_a;

Supp_y_t=length(y_t);
Energia_y_t=sqrt(norm(y_t));
Potenza_y_t=Energia_y_t / Supp_y_t;

%SNR
SNR_a=10 * log10(Potenza_y_a / Potenza_err_quant_arr_conv_risp);
SNR_t=10 * log10(Potenza_y_t / Potenza_err_quant_tronc_conv_risp);

