% Pulisce il workspace
% Pulisce il command window
% Chiude le figure

clear all;                      
close all;                      
clc;                         

% Z-transform:
% G(z) = N(z)/D(z)
% N(z) = 2 + 0.8z^-1 + 0.5z^-2 + 0.3z^-3
% D(z) = 1 + 0.8z^-1 + 0.2z^-2

num = [2 0.8 0.5 0.3];
den = [1 0.8 0.2];

[r,p,k] = residuez(num,den)

