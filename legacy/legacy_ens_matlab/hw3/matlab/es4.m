%~ Pulizia environment
close all; clear all; clc;

%~ A(z)
b = [0.923 -1.359 1]; % [b0 b1 b2]  numeratore 
a = [1 -1.359 0.923]; % [a0 a1 a2]   denominatore

% Diagramma zeri e poli
[z, p] = tf2zp(b, a);
figure(1);
zplane(z, p);

%~ Griglia piano complesso
N = 100;
punti = linspace(-1.2, 1.2, N);
[re, im] = meshgrid(punti, punti);

%~ Valutazione H(z)
zeta = re + j*im;
Hz = polyval(b, zeta)./polyval(a, zeta);

%~ Diag modulo di H(z)
figure(2);
mesh(punti, punti, 20*log10(abs(Hz)));

%~ FFT con freqz (T=F=1)
N = 1024;
[H, f] = freqz(b, a, N, 1, 'whole');

%~ Aggiungo sopra al graf 3d la FFT
hold on;
plot3(cos(2*pi*f), sin(2*pi*f), 20*log10(abs(H)), 'k.');
xlabel('parte reale');
ylabel('parte immaginaria');
zlabel('|H(z)| [dB]');
axis([-1.2 1.2 -1.2 1.2 -70 10]);

%~ Approssimazione di IIR con un FIR
M = 35;
h_ = filter(b, a, [1, zeros(1, M)]);

%~ Diag zeri poli FIR
z = roots(h_);
figure(3);
zplane(z, []);

%~ Grafico Hz
Hz_ = polyval(h_, zeta).*zeta.^(-M);
figure(4);
%~ modulo
modulo = 20*log10(abs(Hz_));
%~ via i punti esplosivi
modulo( find(modulo>50) ) = NaN;
%~ Visualizzo il modulo in dB
mesh(punti, punti, modulo); 
xlabel('parte reale');
ylabel('parte immaginaria');
zlabel('|H(z)| [dB]');
axis([-1.2 1.2 -1.2 1.2 -70 10]);

%~ aggiungo la FFT del FIR 
H_ = freqz(h_, 1, N, 1, 'whole'); hold on;
plot3(cos(2*pi*f), sin(2*pi*f), 20*log10(abs(H_)), 'k.');



%~ A(z)
b = [-0.2 0.18 0.4 1]; % [b0 b1 b2]  numeratore 
a = [1 0.4 0.18 -0.2]; % [a0 a1 a2]   denominatore

% Diagramma zeri e poli
[z, p] = tf2zp(b, a);
figure(5);
zplane(z, p);

%~ Griglia piano complesso
N = 100;
punti = linspace(-1.2, 1.2, N);
[re, im] = meshgrid(punti, punti);

%~ Valutazione H(z)
zeta = re + j*im;
Hz = polyval(b, zeta)./polyval(a, zeta);

%~ Diag modulo di H(z)
figure(6);
mesh(punti, punti, 20*log10(abs(Hz)));

%~ FFT con freqz (T=F=1)
N = 1024;
[H, f] = freqz(b, a, N, 1, 'whole');

%~ Aggiungo sopra al graf 3d la FFT
hold on;
plot3(cos(2*pi*f), sin(2*pi*f), 20*log10(abs(H)), 'k.');
xlabel('parte reale');
ylabel('parte immaginaria');
zlabel('|H(z)| [dB]');
axis([-1.2 1.2 -1.2 1.2 -70 10]);

%~ Approssimazione di IIR con un FIR
M = 35;
h_ = filter(b, a, [1, zeros(1, M)]);

%~ Diag zeri poli FIR
z = roots(h_);
figure(7);
zplane(z, []);

%~ Grafico Hz
Hz_ = polyval(h_, zeta).*zeta.^(-M);
figure(8);
%~ modulo
modulo = 20*log10(abs(Hz_));
%~ via i punti esplosivi
modulo( find(modulo>50) ) = NaN;
%~ Visualizzo il modulo in dB
mesh(punti, punti, modulo); 
xlabel('parte reale');
ylabel('parte immaginaria');
zlabel('|H(z)| [dB]');
axis([-1.2 1.2 -1.2 1.2 -70 10]);

%~ aggiungo la FFT del FIR 
H_ = freqz(h_, 1, N, 1, 'whole'); hold on;
plot3(cos(2*pi*f), sin(2*pi*f), 20*log10(abs(H_)), 'k.');


