%pulizia 
close all;
clear all;
clc;

N1=20;
N2=10000;
N3=500;
t=1:N1;
t1=1:N2;
%F=-Fc/2:Fc/N2:Fc/2-Fc/N2;

%definizione della funzione di trasferimento del sistema a fase minima
num_m=[1 0];
den_m=[1 -1/2];
%definizione della funzione di trasferimento del passa-tutto
num_ap=[-1/2 1];
den_ap=[1 -1/2];

%Punto a)
	%tracciamento del diagramma zeri-poli
	zplane(num_m,den_m);
	%creazione della risposta impulsiva h_m(n)
	h_m=impz(num_m,den_m,N1);
	%calcolo del ritardo di gruppo del sistema a fase minima
	[Gd W]=grpdelay(num_m,den_m,N3);
	figure;
	plot(t,h_m)
	%calcolo della trasformata di Fourier del sistema
	H_m=fftshift(fft(h_m,N2));
	figure;
	plot(abs(H_m));
	figure;
	plot(phase(H_m));

%Punto b)
	%definizone della funzione di trasferimento a fase non minima
	num_nm=conv(num_m,num_ap);
	den_nm=conv(den_m,den_ap);
	%tracciamento del diagramma zeri-poli
	figure;
	zplane(num_nm,den_nm);
	%creazione della risposta impulsiva h_nm(n)
	h_nm=impz(num_nm,den_nm,N1);
	%calcolo del ritardo di gruppo del sistema a fase minima
	[Gd W]=grpdelay(num_nm,den_nm,N3);
	figure;
	plot(t,h_nm)
	%calcolo della trasformata di Fourier del sistema
	H_nm=fftshift(fft(h_nm,N2));
	figure;
	plot(abs(H_nm));
	figure;
	plot(phase(H_nm));
	
%Punto c)
	
	for i=1:size(h_m) 
		E_m(i)=norm(h_m(1:i))^2;
	end
	
	for i=1:size(h_nm) 
		E_nm(i)=norm(h_nm(1:i))^2;
	end
	
	figure;
	plot(t,E_m,t,E_nm);
	
		
