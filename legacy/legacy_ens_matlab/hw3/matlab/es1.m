% pulizia environment
clear all;
close all;
clc;

disp('HW 3 es 1 punto a');
%~ coefficienti p (x(n))
num= [0.0108 -0.0168 0.0108];
%~ num= [1 2 3];
% coefficienti d (y(n))
den= [1 -1.9198 0.9252];
%~ den= [1 2 -1];
% # campioni da calcolare
n_c=201;

num
den
% calcolo della risposta impulsiva:
%~ [h,m]=impz(num,den,n_c);

% grafico risposta impulsiva:
%~ figure;
%~ stem(m, h);               
%~ xlabel('n');                  
%~ ylabel('h(n)');               
%~ title('Risp. implusiva del sistema');     
%~ legend('h(n)');


%~ H(z)=N(z)/D(z)=R(z)+Q(z)/D(z)

%~ Ottenimento di R(z) e Q(z):
%~ NB SERVE IL REVERSAL!!!! 
[rem, quot]=deconv(fliplr(num), fliplr(den));
%~ [rem, quot]=deconv(num, den);
%~ [quot, rem]=deconv(num, den);

disp('H(z)=R(z)+Q(z)/D(z)');
quot=fliplr([quot(2) quot(3)]);
quot
rem=fliplr(rem);

den

%~ ES 6.13
%~ num = [2 0.8 0.5 0.3];
%~ den = [1 0.8 0.2];
%~ [r,p,k] = residuez(num,den)

%~ REVERSAL
%~ num = [0.3 0.5 0.8 2];
%~ den = [0.2 0.8 1];
%~ [quot, rem]=deconv(num, den)

%~ disp('Versione impropria:')
%~ [r,p,k] = residuez(num,den)
%~ disp('Versione propria:')
%~ [r,p,k] = residuez(quot,den)
%~ rem

poles=roots(den)
inv_poles=[1/poles(1); 1/poles(2)]
rem
alpha=(quot(1)+quot(2)*inv_poles(1))/(1-poles(2)*inv_poles(1))
beta=(quot(1)+quot(2)*inv_poles(2))/(1-poles(1)*inv_poles(2))




disp('HW 3 es 1 punto b');
%~ coefficienti p (x(n))
num= [1 -1.9949 1];
%~ num= [1 2 3];
% coefficienti d (y(n))
den= [1 -1.9921 0.9972];
%~ den= [1 2 -1];
% # campioni da calcolare
n_c=201;

num
den
% calcolo della risposta impulsiva:
%~ [h,m]=impz(num,den,n_c);

% grafico risposta impulsiva:
%~ figure;
%~ stem(m, h);               
%~ xlabel('n');                  
%~ ylabel('h(n)');               
%~ title('Risp. implusiva del sistema');     
%~ legend('h(n)');


%~ H(z)=N(z)/D(z)=R(z)+Q(z)/D(z)

%~ Ottenimento di R(z) e Q(z):
%~ NB SERVE IL REVERSAL!!!! 
[rem, quot]=deconv(fliplr(num), fliplr(den));
%~ [rem, quot]=deconv(num, den);
%~ [quot, rem]=deconv(num, den);

disp('H(z)=R(z)+Q(z)/D(z)');
quot=fliplr([quot(2) quot(3)]);
quot
rem=fliplr(rem);

den

%~ ES 6.13
%~ num = [2 0.8 0.5 0.3];
%~ den = [1 0.8 0.2];
%~ [r,p,k] = residuez(num,den)

%~ REVERSAL
%~ num = [0.3 0.5 0.8 2];
%~ den = [0.2 0.8 1];
%~ [quot, rem]=deconv(num, den)

disp('Versione impropria:')
[r,p,k] = residuez(num,den)
%~ disp('Versione propria:')
%~ [r,p,k] = residuez(quot,den)
%~ rem

poles=roots(den)
inv_poles=[1/poles(1); 1/poles(2)]
rem
alpha=(quot(1)+quot(2)*inv_poles(1))/(1-poles(2)*inv_poles(1))
beta=(quot(1)+quot(2)*inv_poles(2))/(1-poles(1)*inv_poles(2))
