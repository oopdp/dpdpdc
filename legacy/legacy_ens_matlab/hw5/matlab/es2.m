
% TRACCIA_es2_HWK5_IIRnotch.m
% Eliminazione di un disturbo a banda stretta con filtro notch.

close all; 
clear all;

% carica l'immagine da "lena.mat" e la mette in "segnale"
% l'immagine e' 512x512


segnale=imread('lena.tif');
%~ imshow(segnale);

%~ load lena; 
 
% "segnale" e` di tipo uint8 e va convertito in double!!
segnale = double(segnale);
F = 1;
T = 1/F;

% Aggiungo rumore a banda stretta
f_int = 1/50; % frequenza interferente
n = 0 : size(segnale, 2) - 1;
rumore = 80 * cos(2*pi* f_int *n*T);

% 
figure;
plot(n,rumore);
axis([0 511 -85 +85]);
title('Rumore sinusoidale generato');
xlabel('x');

% 
figure;
plot(n, segnale(1,:));
axis([0 511 -10 260]);
title('La prima riga dell''immagine');
xlabel('x');


% replico il rumore su tutte le righe
rumore = rumore(ones(1, size(segnale,1)), : );
% aggiungo il rumore al segnale

segnale_r = segnale + rumore;


% 
figure;
plot(n, segnale_r(1,:));
axis([0 511 -50 300]);
title('La prima riga dell''immagine disturbata');
xlabel('x');


% Visulizzo l'immagine originale
figure;
imshow(uint8(segnale));

% Visulizzo il disturbo
figure;
imshow(uint8(rumore));

% Visulizzo l'immagine corrotta
figure;
imshow(uint8(segnale_r));

% Progetto il filtro notch
f0 = f_int;
theta0 = 2*pi * f0 * T;

% scelgo un delta_f3dB abbastanza piccolo per non rimuovere troppe
% componenti utili del segnale, ma nemmeno troppo piccolo perche'
% altrimenti non riusciamo a togliere l'interferenza se vi sono
% approssimazioni nell'individuazione della frequenza
delta_f3dB = 1/200;

delta = 2*pi * (delta_f3dB/F) / 2;
r = 1 - delta;
p = r * exp(j*theta0);
z = exp(j*theta0);

b = conv([1, -z],[1, -conj(z)]);
a = conv([1, -p],[1, -conj(p)]);

% Visualizzo la risposta impulsiva e in frequenza
figure;
imp_resp = filter(b, a, [1 zeros(1, 100)]);
[H, f] = freqz(b, a, 1024, F, 'whole');
subplot(3,1,1);
stem(0:100, imp_resp, 'k.');
grid
subplot(3,1,2);
plot(f, abs(H));
grid
subplot(3,1,3);
plot(f, unwrap(angle(H)));
grid

% Filtro il segnale per righe
pulito = zeros(size(segnale));
for row = 1:size(segnale, 1)
    
    pulito(row, :) = filter(b, a, segnale_r(row, :));
    
end

% Visulizzo l'immagine pulita
figure;
imshow(uint8(pulito));

% In alternativa si può usare  
%                   pulito = filter(b, a, segnale_r');  
% attenzione che filter filtra per colonne per cui come ingresso bisogna
% dargli segnale_r' al fine di filtrare segnale_r per righe;
% attenzione anche che l' usita pulito = filter(b, a, segnale_r') 
% è trasposta ripetto all' immagine e per visualizzare l' immagine non
% ruotatat bisogna trasporre nuovamente pulito, ossa bisogna fare
%                  imshow(uint8(pulito'));
% provate queste due istruzioni al posto del ciclo sopra indicato !

