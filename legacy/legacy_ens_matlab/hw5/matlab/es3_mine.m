% Traccia Es. 3 HWK5

close all; 
clear all;
clc;

%~ Frequenza di campionamento
Fc=10e3;


%~ Freq Edges
f_p=1e3;
f_s=1.5e3;

%~ Ripple stopband: 42dB
%~ Ripple passband: 0.2dB
Rs=42;
Rp=0.5;

fprintf('Passband edge: %f\n',f_p);
fprintf('Stopband edge: %f\n',f_s);
fprintf('Passband ripple: %f\n',Rp);
fprintf('Stopband ripple: %f\n',Rs);

fp=f_p;
fs=f_s;

% Pulsazioni normalizzate w/pi
wp=2*f_p/Fc;
ws=2*f_s/Fc;


%%%%%%%%%%%%%%%%%%%%
% Ottimizzo Rp     %
%%%%%%%%%%%%%%%%%%%%


[n0,wn0]=ellipord(wp,ws,Rp,Rs); % calcolo l`ordine del filtro con i parametri di ingresso

n=n0;
Rp_opt = Rp;
step_Rp = 0.001;                        % passo di miglioramento
while(n==n0)
    Rp_opt = Rp_opt - step_Rp;          % Riduce ripple
    [n,wn] = ellipord(wp,ws,Rp_opt,Rs); % ricalcola ordine del filtro
end;
Rp_opt = Rp_opt + step_Rp;              % Ultimo valore prima del cambio dell` ordine

[n,wn]=ellipord(wp,ws,Rp_opt,Rs);       % Filtro ottimo in B.P
fprintf('\nOttimizzazione Rp:\n');
fprintf('Rs = %f \n',Rs);
fprintf('Rp_opt  = %f \n',Rp_opt);



%%%%%%%%%%%%%%%%%%%%
% Ottimizzo Rs     %
%%%%%%%%%%%%%%%%%%%%
[n0,wn0]=ellipord(wp,ws,Rp,Rs); % calcolo l`ordine del filtro con i parametri di ingresso

n=n0;
Rs_opt = Rs;
step_Rs = 0.001;                        % passo di miglioramento
while(n==n0)
    Rs_opt = Rs_opt - step_Rs;          % Riduce ripple
    [n,wn] = ellipord(wp,ws,Rp,Rs_opt); % ricalcola ordine del filtro
end;
Rs_opt = Rs_opt + step_Rs;              % Ultimo valore prima del cambio dell` ordine

[n,wn]=ellipord(wp,ws,Rp,Rs_opt);       % Filtro ottimo in B.P
fprintf('\nOttimizzazione Rs:\n');
fprintf('Rs_opt  = %f \n',Rs_opt);
fprintf('Rp = %f \n',Rp);



% progetto i filtri



% filtro da specifica
[n,wn]=ellipord(wp,ws,Rp,Rs);
[B1,A1]=ellip(n,Rp,Rs,wn);   
[H1,f] = freqz(B1,A1,4096,'whole',Fc); 

% filtro con Rs ottimo
[n,wn]=ellipord(wp,ws,Rp,Rs_opt);
[B2,A2]=ellip(n,Rp,Rs_opt,wn);   
[H2,f2] = freqz(B2,A2,4096,'whole',Fc);         % calcolo la risposta in frequenza

% filtro con Rp ottimo
[n,wn]=ellipord(wp,ws,Rp_opt,Rs);
[B3,A3]=ellip(n,Rp_opt,Rs,wn);   
[H3,f3] = freqz(B3,A3,4096,'whole',Fc);         % calcolo la risposta in frequenza


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%  GRAFICI     %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

f = f(1:length(f)/2);
nf = length(f);

figure(1);
plot(f,20*log10(abs(H1(1:nf))),f,20*log10(abs(H2(1:nf))),f,20*log10(abs(H3(1:nf))));
title('Confronto in Banda attenuata');
xlabel('f Hz');
ylabel('|H(f)|');
axis([0 Fc/2 -100 5]);
legend('Ellipord','Ottimo in B.A','Ottimo in B.P');
line([0 Fc/2],[0 0],'LineStyle',':','Color','k');
line([0 Fc/2],[-Rs -Rs],'LineStyle',':','Color','k');
line([0 Fc/2],[-Rs_opt -Rs_opt],'LineStyle',':','Color','k');
line([fp fp],[-100 5],'LineStyle',':','Color','k');
line([fs fs],[-100 5],'LineStyle',':','Color','k');
text(100,-43,'R_s');
text(100,-59,'R_s^{opt}');
text(fp-100,-90,'f_p');
text(fs-100,-90,'f_s');

figure(2);
plot(f,20*log10(abs(H1(1:nf))),f,20*log10(abs(H2(1:nf))),f,20*log10(abs(H3(1:nf))));
title('Confronto in Banda passante');
xlabel('f Hz');
ylabel('|H(f)|');
axis([0 fp+100 -0.26 0.01]);
legend('Ellipord','Ottimo in B.A','Ottimo in B.P');
line([0 Fc/2],[0 0],'LineStyle',':','Color','k');
line([0 Fc/2],[-Rp_opt -Rp_opt],'LineStyle',':','Color','k');
line([0 Fc/2],[-Rp -Rp],'LineStyle',':','Color','k');
line([fp fp],[-100 5],'LineStyle',':','Color','k');

text(200,-0.02,'R_p^{opt}');
text(100,-0.24,'R_p');
text(fp-50,-0.24,'f_p');



