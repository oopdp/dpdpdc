close all; 
clear all;
clc;

%~ Frequenza di campionamento
Fc=10e3;

%~ Passband: 0 - 1khz
f_0=0;
f_pb=1e3;

%~ Stopband: 1.5kHz - 4kHz
f_s=1.5e3;
f_e=4e3;

%~ Ripple stopband: 42dB
%~ Ripple passband: 0.2dB
sb_ripple=42;
pb_ripple=0.5;

% Pulsazioni normalizzate w/pi
w_pb=2*f_pb/Fc;
w_sb=2*f_s/Fc;

%~ %Ellittico
[N_ell,Wn_ell]=ellipord(w_pb, w_sb, pb_ripple, sb_ripple);
[num_ell, den_ell]=ellip(N_ell,pb_ripple,sb_ripple,Wn_ell);
fprintf('Ordine del filtro ellittico: %d\n',N_ell);
fprintf('Passband edge: %f\n',w_pb);
fprintf('Stopband edge: %f\n',w_sb);

N=1024;
[H_ell,w_ell]=freqz(num_ell,den_ell,N);

% Visualizzo la risposta in frequenza
figure

subplot(3,1,1);
plot(w_ell/pi, abs(H_ell));
title ('|H_{ell}(e^{j\omega})| - Filtro ellittico');
grid;
%~ xlabel('\omega');
%~ ylabel('|H_{ell}(e^{j\omega})|');

subplot(3,1,2);
plot(w_ell/pi, unwrap(angle(H_ell)));
title ('fase H_{ell}(e^{j\omega}) - Filtro ellittico');
grid;
%~ xlabel('\omega')
%~ ylabel('fase H_{ell}(e^{j\omega})');

subplot(3,1,3);
[gd_ell,w_gd_ell]=grpdelay (num_ell, den_ell, N);
plot(w_gd_ell/pi, gd_ell);
title ('group delay H_{ell}(e^{j\omega}) - Filtro ellittico');
grid;
xlabel('\omega/\pi');
%~ ylabel('group delay H_{ell}(e^{j\omega})');

