close all; 
clear all;
clc;

wp = .1      
ws = .2
Rs=40
Rp=0.5

disp('Chebyshev ordine 1');
[n,wn]=cheb1ord(wp,ws,Rp,Rs)
disp('Elliptic');
[n,wn]=ellipord(wp,ws,Rp,Rs)
disp('Butterworth');
[n,wn]=buttord(wp,ws,Rp,Rs)


% Pulisce il workspace
% Pulisce il command window
% Chiude le figure


%~ close all;
%~ clear all;
%~ clc;
%~ 
%~ f_p=(15e3);
%~ f_t=(44.1e3);
%~ f_s=f_t-f_p;
%~ r_p=1;
%~ r_s=40;
%~ 
%~ %Butterworth
%~ [N_bu,Wn_bu]=buttord(2*pi*f_p,2*pi*f_s,r_p,r_s,'s');
%~ [num_bu, den_bu]=butter(N_bu,Wn_bu,'s');
%~ disp('Ordine del filtro di Butterworth');
%~ N_bu
%~ 
%~ %Chebychev di tipo 1
%~ [N_Ch1,Wn_Ch1]=cheb1ord(2*pi*f_p,2*pi*f_s,r_p,r_s,'s');
%~ [num_ch1, den_ch1]=cheby1(N_Ch1,r_p,Wn_Ch1,'s');
%~ disp('Ordine del filtro di Chebychev di tipo 1');
%~ N_Ch1
%~ 
%~ %Chebychev di tipo 2
%~ [N_Ch2,Wn_Ch2]=cheb2ord(2*pi*f_p,2*pi*f_s,r_p,r_s,'s');
%~ [num_ch2, den_ch2]=cheby2(N_Ch2,r_s,Wn_Ch2,'s');
%~ disp('Ordine del filtro di Chebychev di tipo 2');
%~ N_Ch2
%~ 
%~ %Ellittico
%~ [N_E,Wn_E]=ellipord(2*pi*f_p,2*pi*f_s,r_p,r_s,'s');
%~ [num_e, den_e]=ellip(N_E,r_p,r_s,Wn_E,'s');
%~ disp('Ordine del filtro ellittico');
%~ N_E
