% Program 9_4
% Group-delay equalization of an IIR filter.

clear all;
close all;
clc;

% 1) Design a transfer function in oder to equalize its group delay

[n,d] = ellip(4,1,35,0.3); %   [num,den] = ellip(N,Rp,Rs,Wn,'high') 
% The cutoff frequency Wn must be 0.0 < Wn < 1.0, with 1.0 corresponding 
% to half the sample rate.

% Plot magnitude and group delay

[H,omega] = freqz(n,d,256);
plot (omega/pi,20*log10(abs(H)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title('IIR Elliptic Lowpass Filter');

[GdH,w] = grpdelay(n,d,512);
figure
plot(w/pi,GdH); grid
xlabel('\omega/\pi'); ylabel('Group delay, samples');
title('Original Filter');

figure
[Z,P,K] = TF2ZP(n,d);
ZPLANE(Z,P);
title('Zeros and poles of the IIR Elliptic Lowpass Filter ');

% Interval whre the group delay must be equalized (typicallly the passband
% as in this case)

F = 0:0.001:0.3;         % passband  frequencies
g = grpdelay(n,d,F,2);   % Equalize the passband 
% Gd = GRPDELAY(B,A,F,Fs) return the group delay
% evaluated at the points in  F (in Hz)
% in the statement Fs=2
figure
plot(F,g); grid
xlabel('\omega/\pi'); ylabel('Group delay, samples');
title('Original Filter Passband');

Gd = max(g)-g;

% 2) Design the allpass delay equalizer of order 8 
N= 8;            % N must be even
EDGES= [0 0.3];  % passband where the group delay must be equalized 
[num,den,tau] = iirgrpdelay(N, F, EDGES, Gd);

    %[NUM,DEN] = IIRGRPDELAY(N,F,EDGES,Gd) returns an allpass IIR filter of
    %order N (must be even) which is the best approximation to the relative
    %group-delay response described by F and Gd in the least-Pth sense. F is
    %a vector of frequencies between 0 and 1 and Gd is a vector of the desired
    %group-delay specified in samples.
 
    %The vector EDGES specifies the band-edge frequencies for multi-band
    %designs. A constrained Newton-type algorithm is employed.
    %Always check the resulting filter using GRPDELAY or FREQZ.
 
    %F and Gd must have the same number of elements, which can exceed the
    %number of elements in EDGES.  This allows for the specification of
    %filter having any group-delay contour within each band.
    
    %[NUM,DEN,TAU] = IIRGRPDELAY(...) returns the resulting group delay
    %offset.  In all cases, the resulting filter has a group delay that
    %approximates [Gd + TAU].  This is because the allpass filter can only
    %have positive  group delay and a non-zero value of TAU accounts for any
    %additional group delay that is needed to meet the shape of the contour
    %specified by (F,Gd).
 
    
% Plot the group delay     
figure
[GdA,w] = grpdelay(num,den,512);
plot(w/pi,GdH+GdA); grid
xlabel('\omega/\pi');ylabel('Group delay, samples');
title('Group Delay Equalized Filter N=8 ');

figure
[Z,P,K] = TF2ZP(num,den);
ZPLANE(Z,P);
title('Zeros and poles of the G.D. equalizer, N=8 ');

% Design the allpass delay equalizer of order 10 
[num2,den2,tau] = iirgrpdelay(10, F, [0 0.3], Gd);
[GdA,w] = grpdelay(num2,den2,512);
figure
plot(w/pi,GdH+GdA); grid
xlabel('\omega/\pi');ylabel('Group delay, samples');
title('Group Delay Equalized Filter N=10');

figure
[Z,P,K] = TF2ZP(num,den);
ZPLANE(Z,P);
title('Zeros and poles of the G.D. equalizer, N=10 ');



