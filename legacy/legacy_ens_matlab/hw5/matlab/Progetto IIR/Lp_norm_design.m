% Lp_norm_design.m
% Designof generic IIR filters in Lp norm (ona may ise a diferent
% optimality criterion, e.g., minimax)
% 
% Advantages:
% 1) Numerator and denominator degree can be different 
% 2) One can design ANY  behaviour for the magnitude of the transfer
%    fubction (not just band-select filters)


    %[NUM,DEN] = IIRLPNORM(N,D,F,EDGES,A) returns a filter having a numerator
    %order N and denominator order D which is the best approximation to the
    %desired  frequency response described by F and A in the least-Pth
    %sense.
    %The vector EDGES specifies the band-edge frequency points, i.e., the point
    %where a frequency band starts/stops and a don't care regions stops/starts.
    %EDGES must always contain the first and last frequency points in F.
    %An unconstrained quasi-Newton algorithm is employed and any poles or
    %zeros that lie outside of the unit circle are reflected back inside.
    %N and D should be chosen so that the zeros and poles are used effectively.
    %See the hints below.  Always check the resulting filter using FREQZ.
    
    %[NUM,DEN] = IIRLPNORM(N,D,F,EDGES,A,W) uses the weights in W to weight the
    %error.  W has one entry per frequency point (the same length as F and A)
    %which tells IIRLPNORM how much emphasis to put on minimizing the error in
    %the vicinity of each frequency point relative to the other points.
    
    
    % see HELP for other details, eg.g, choice of value P and so on.
    
clc;
clear all;
close all;

%1) Low-pass with enphasis of 1.6 at 0.15

N= 5;                  % numerator degree
D = 12;                % denominator degree
F=[0 .15 .4 .5 1];     % list of frequency values (the sampling frequency is as usual Fs=2)at which 
                       % the frequency response magnitude or amplitude assumes the values indicated in 
                       % list A
EDGES= [0 .4 .5 1];    % passband and stopband edges
A = [1 1.6 1 0 0];     % list of frequency response magnitude or amplitude values corresponding to the 
                       % frequency values  of list F (the sampling frequency is as usual Fs=2)at which
W= [1 1 1 1 1];        % list of weights associated to the frequency response magnitude or amplitude 
                       % values at the frequency values  of list F 
                       

[b,a] = iirlpnorm(N,D,F,EDGES,A,W);     % no weigths W, in this case it is also o.k. not includiing W 
                                        % among the parameters, i.e., 
                                        % [b,a] = iirlpnorm(N,D,F,EDGES,A); 
fvtool(b,a)
legend ('N=5, D=12, no W ')

%2) Relevance of the weights with the same filter as above

N= 5;                  % numerator degree
D = 12;                % denominator degree
F=[0 .15 .4 .5 1];     % list of frequency values (the sampling frequency is as usual Fs=2)at which 
                       % the frequency response magnitude or amplitude assumes the values indicated in 
                       % list A
EDGES= [0 .4 .5 1];    % passband and stopband edges
A = [1 1.6 1 0 0];     % list of frequency response magnitude or amplitude values corresponding to the 
                       % frequency values  of list F (the sampling
                       % frequency is as usual Fs=2)at which
W= [1 1 1 10 10];      % list of weights associated to the frequency response magnitude or amplitude 
                       % values at the frequency values  of list F 
                       
[b,a] = iirlpnorm(N,D,F,EDGES,A,W);
fvtool(b,a)
legend ('N=5, D=12, W= [1 1 1 10 10] ')


%3) Regular low pass with different  N and D (stopband wighted more than
%passband)

N= 5;                  % numerator degree
D = 7;                 % denominator degree
F=[0  .4 .5 1];        % list of frequency values (the sampling frequency is as usual Fs=2)at which 
                       % the frequency response magnitude or amplitude assumes the values indicated in 
                       % list A
EDGES= [0 .4 .5 1];    % passband and stopband edges
A = [1  1 0 0];        % list of frequency response magnitude or amplitude values corresponding to the 
                       % frequency values  of list F (the sampling
                       % frequency is as usual Fs=2)at which
W= [1  1 10 10];       % list of weights associated to the frequency response magnitude or amplitude 
                       % values at the frequency values  of list F 
                       
[b,a] = iirlpnorm(N,D,F,EDGES,A,W);
fvtool(b,a)
legend ('L.P., N=5, D=7, W= [1 1 10 10] ')



%4) Relevance of the weights with the same filter as above (passband wighted more than
% stopband)
 

N= 5;                  % numerator degree
D = 7;                 % denominator degree
F=[0  .4 .5 1];        % list of frequency values (the sampling frequency is as usual Fs=2)at which 
                       % the frequency response magnitude or amplitude assumes the values indicated in 
                       % list A
EDGES= [0 .4 .5 1];    % passband and stopband edges
A = [1  1 0 0];        % list of frequency response magnitude or amplitude values corresponding to the 
                       % frequency values  of list F (the sampling
                       % frequency is as usual Fs=2)at which
W= [100  100 1 1];       % list of weights associated to the frequency response magnitude or amplitude 
                       % values at the frequency values  of list F 
                       
[b,a] = iirlpnorm(N,D,F,EDGES,A,W);
fvtool(b,a)
legend ('L.P., N=5, D=7, W= [100 100 1 1] ')