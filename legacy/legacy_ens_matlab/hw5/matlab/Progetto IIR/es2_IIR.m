% es2_IIR.m
% Esempio 2:
% Progetto di un filtro Passa Basso e passa banda IIR ellittici.

clc; clear all; close all;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Progetto di un filtro passa basso IIR Ellittico
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%
%%% SPECIFICHE %%%
%%%%%%%%%%%%%%%%%%

% Frequenza di campionamento:   8kHz
% Banda Passante:               0Hz<= f <= 1kHz
% DBR:                          <= 0.2dB
% Banda Attenuata:              1.5 kHz <= f <= 4 kHz
% Attenuazione:                 >= 42dB


%%%% Definisco le costanti di progetto
Fc=8000;

fp=1000;
fs=1500;

Rp=0.2;               % DBR
Rs=42;                % Attenuazione
Wp=fp/(Fc/2);       % Frequenza superiore Banda pass NORMALIZZATA!!!!
Ws=fs/(Fc/2);       % Frequenza inferiore Banda stop NORMALIZZATA!!!!

%%%%%%%%%%%%%%%%%%
%%%% PROGETTO %%%%
%%%%%%%%%%%%%%%%%%

% Iniziamo con lo stimare l'ordine del filtro
% N � l'ordine del filtro, Wn � la frequenza superiore in banda passante 
% alla quale il modulo vale -Rp db (in forma normalizzata!!!) ossia Wn vale
% esattamente Wp
[N,Wn]=ellipord(Wp,Ws,Rp,Rs);       
  
% Progettiamo il filtro ellittico
[B,A]=ellip(N,Rp,Rs,Wn);                    
% calcolo la risposta in frequenza
[H,f] = freqz(B,A,4096,'whole',Fc); 
% calcolo la risposta impulsiva
h=impz(B,A,100);                                                                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           GRAFICI                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure(1);
subplot(3,1,1);
plot(f,20*log10(abs(H)));
title('Andamento in frequenza del modulo');
xlabel('f Hz');
ylabel('|H(f)| dB');
line([0 fp],[0 0],'LineStyle','--','Color','r');
line([0 fp],[-Rp -Rp],'LineStyle','--','Color','r');
line([fp fp ],[-100 0],'LineStyle','--','Color','r');
line([fs Fc/2],[-Rs -Rs],'LineStyle','--','Color','r');
line([fs fs],[-100 -Rs],'LineStyle','--','Color','r');
axis([0 Fc/2 -70 5]);
text(fp+40,-60,'f_p');text(fs+40,-60,'f_s');

subplot(3,1,2);
plot(f,20*log10(abs(H)));
title('Andamento in frequenza del modulo: Ingrandimento in banda attenuata');
xlabel('f Hz');
ylabel('|H(f)| dB');
line([fs fs],[-200 -Rs],'LineStyle','--','Color','r');
line([fs Fc/2],[-Rs -Rs],'LineStyle','--','Color','r');
axis([(fs-100), Fc/2, (-Rs-30), (-Rs+10)]);
text(fs+40,-60,'f_s');text(fs+40,-Rs+5,'-R_s');

subplot(3,1,3);
plot(f,20*log10(abs(H)));
title('Andamento in frequenza del modulo: Ingrandimento in banda passante');
xlabel('f Hz');
ylabel('|H(f)| dB');
line([0 fp],[0 0],'LineStyle','--','Color','r');
line([0 fp],[-Rp -Rp],'LineStyle','--','Color','r');
line([fp fp ],[-100 0],'LineStyle','--','Color','r');
axis([0, fp+100, -Rp-0.2, 0.2]);
text(100,-Rp-0.1,'R_p');text(fp+50,-Rp-0.1,'f_p');


figure(2);
subplot(3,1,1);
plot(f,20*log10(abs(H)));
axis([0 Fc/2 -70 5 ]);
title('Andamento in frequenza del modulo');
xlabel('f Hz');
ylabel('|H(f)| dB');
subplot(3,1,2);
plot(f,unwrap(angle(H)));
title('Andamento in frequenza della fase');
axis([0 Fc/2 min(unwrap(angle(H)))  max( unwrap(angle(H)))]);
xlabel('f Hz');
ylabel('arg(H(h)) rad');
subplot(3,1,3);
stem([0:99]*1/Fc,h)
title('Andamento della risposta impulsiva');
xlabel('t s');
ylabel('Ampiezza');

pause;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Progetto di un filtro passa banda IIR Ellittico
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%
%%% SPECIFICHE %%%
%%%%%%%%%%%%%%%%%%

% Frequenza di campionamento:   8kHz
% Banda Passante:               1kHz<= f <= 2kHz
% DBR:                          <= 0.2dB
% Banda Attenuata:              0Hz <= f <= 500 Hz e 2.5Hz <= f <= 4 kHz
% Attenuazione:                 >= 42dB


%%%% Definisco le costanti di progetto
Fc=8000;

fp=[1000, 2000];
fs=[500 2500];

Rp=0.2;               % DBR
Rs=42;                % Attenuazione
Wp=fp./(Fc/2);        % Frequenza superiore Banda pass NORMALIZZATA!!!!
Ws=fs./(Fc/2);        % Frequenza inferiore Banda stop NORMALIZZATA!!!!

%%%%%%%%%%%%%%%%%%
%%%% PROGETTO %%%%
%%%%%%%%%%%%%%%%%%

% Iniziamo con lo stimare l'ordine del filtro 
[N,Wn]=ellipord(Wp,Ws,Rp,Rs);       % N � l'ordine del filtro

% progetto il filtro ellittico
[B,A]=ellip(N,Rp,Rs,Wn);    
% calcolo la risposta in frequenza
[H,f] = freqz(B,A,4096,'whole',Fc);         
% calcolo la risposta impulsiva
h=impz(B,A,100);                            


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           GRAFICI                             %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


figure(3);
subplot(3,1,1);
plot(f,20*log10(abs(H)));
title('Andamento in frequenza del modulo');
xlabel('f Hz');
ylabel('|H(f)| dB');
subplot(3,1,2);
plot(f,unwrap(angle(H)));
title('Andamento in frequenza della fase');
xlabel('f Hz');
ylabel('arg(H(h)) rad');
subplot(3,1,3);
stem([0:99]*1/Fc,h)
title('Andamento della risposta impulsiva');
xlabel('t s');
ylabel('Ampiezza');
