% P4_fixed_specs_tutti_i_tipi_Butterwoth.m
% Esemplifica il progetti di filtri di Butterworth passa-alto, passa-banda e elimina-banda
% date le specifiche 

%La prima volta che si vede il codice si osservi che il progetto avviene
%tramite i seguenti comandi 
% 1) Passa-basso
% 
%[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num, den]=butter(N,Wn,'s');              %         Lowpass:    Wp < Ws
%
% 2) Passa-alto
% 
%[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num, den]=butter(N,Wn,'s');              %         Highpass:    Wp > Ws
%
% 3) Passa-banda
% 
%[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num, den]=butter(N,Wn,'s');              %         Bandpass:    Ws1< Wp1 < Wp2 < Ws2
%
% 4) Elimina-banda
% 
%[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num, den]=butter(N,Wn,'s');              %         Bandstop:    Wp1< Ws1
%< Ws2 < Wp2


% tutti le altre istruzioni servono solo a fare i grafici



%
close all
clear all
clc

%
% 1)  Passa-basso
% 
% Specifiche 
%
Fp=1000;    % [0, Fp] banda passante in Hz
Fs=3000;    % [Fs, inf] banda attenuata in Hz
Rp=1;       % Ripple in dB in  banda passante 
Rs=40;      % Ripple in dB in  banda attenuata
format long
%
%Progetto
%
[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num, den]=butter(N,Wn,'s');          % il progeto richiede 2 parametri N,Wn


%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num,den,omega);
h1=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Butterworth = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num)
p=roots(den);
figure;
zplane(z,p);
title( sprintf('Poli di L.P. Butterworth = %d', N) );
Legend ('Zeri  x Poli')

%
% 2)  Passa-alto
% 
% 
% Specifiche 
%
Fp=3000;    % [Fp, inf] banda passante in Hz
Fs=1000;    % [0, Fs] banda attenuata in Hz
Rp=1;       % Ripple in dB in  banda passante 
Rs=40;      % Ripple in dB in  banda attenuata
%
%Progetto
%
[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num, den]=butter(N,Wn,'high','s');          % il progeto richiede 2 parametri N,Wn

%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num,den,omega);
h2=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('H.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('H.P. Butterworth = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num)
p=roots(den);
figure;
zplane(z,p);
title( sprintf('Poli di H.P. Butterworth = %d', N) );
Legend ('Zeri  x Poli')

%
% 3)  Passa-banda        Ws1< Wp1 < Wp2 < Ws2
% 
% 
% Specifiche 
%
Fp1=[3000,4000];    % [3000,4000] banda passante in Hz
Fs1=[1000;4500];    % [0, 1000] U [4500, inf]  banda attenuata in Hz
Rp=1;       % Ripple in dB in  banda passante 
Rs=40;      % Ripple in dB in  banda attenuata
%
%Progetto
%
[N,Wn]= buttord(2*pi*Fp1,2*pi*Fs1,Rp,Rs,'s');
[num, den]=butter(N,Wn,'s');                    % il progetto richiede 2 parametri N,Wn

%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num,den,omega);
h3=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('B.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('B.P.  Butterworth = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num)
p=roots(den);
figure;
zplane(z,p);
title( sprintf('Poli di B.P.  Butterworth = %d', N) );
Legend ('Zeri  x Poli')

%
% 4)  Elimina-banda        Wp1< Ws1 < Ws2 < Wp2
% 
% 
% Specifiche 
%
Fs1=[3000,4000];    % [3000,4000] banda attenuata in Hz
Fp1=[1000;4500];    % [0, 1000] U [4500, inf]  banda passante in Hz
Rp=1;       % Ripple in dB in  banda passante 
Rs=40;      % Ripple in dB in  banda attenuata
%
%Progetto
%
[N,Wn]= buttord(2*pi*Fp1,2*pi*Fs1,Rp,Rs,'s');
[num, den]=butter(N,Wn,'stop','s');                    % il progetto richiede 2 parametri N,Wn

%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num,den,omega);
h4=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('B.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('B.P.  Butterworth = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num)
p=roots(den);
figure;
zplane(z,p);
title( sprintf('Poli di B.P.  Butterworth = %d', N) );
Legend ('Zeri  x Poli')
% 
%Comparison
%
figure
plot (omega,20*log10(abs(h1)),omega,20*log10(abs(h2)),omega,20*log10(abs(h3)),omega,20*log10(abs(h4)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. N=6');
% frequency is normalized with respect cut-off frequency;
% indeed cut-off frequency is 1
figure;
plot (omega,angle(h1),omega,angle(h2),omega,angle(h3),omega,angle(h4));grid
xlabel('Frequency, Hz'); ylabel('Phase');
legend ('L.P. filters for Fp=1000, Fs=3000, Rp=1, Rs=40');

