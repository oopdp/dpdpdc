% P3_fixed_order_tutti_i_Tipi_Butterworth.m
% Programma che mostra tutti i tipi di filtri analogici di  Butterworth di
% ordine 4 ottenuti direttamente da N e Wn (le altre specifiche sono secondarie)
%
% Ilustrazione delle istruzioni per ottenere Passa-alto,
% Passa_banda,Elimina_banda
%
clc;
close all;
clear all;

% 1) Passa-basso
format long

% Specifiche in termni di ordine N e banda passante Wn
N=4;
Wn=1*2*pi;      %pass-band [0, Wn]

% Determine zeros and poles
[z,p,k] = butter(N,Wn,'s');
disp('Poles are at');disp(p);
%  Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Butterworth = %d', N) );
Legend ('Zeri   x  Poli ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5*2*pi];
% Compute and plot frequency response
h = freqs(pz,pp,omega);
h1=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 5 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 5 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Butterworth  = %d', N) );

% 2) Passa-alto
format long

% Specifiche in termni di ordine N e banda attenuata Wn
N=4;
Wn=1*2*pi;      %stop-band [0, Wn]

% Determine zeros and poles
[z,p,k] = butter(N,Wn,'high','s');
%  Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Butterworth = %d', N) );
Legend ('Zeri   x  Poli ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5*2*pi];
% Compute and plot frequency response
h = freqs(pz,pp,omega);
h2=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 5 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('H.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 5 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('H.P. Butterworth  = %d', N) );



%3) Passa-banda
% Specifiche in termni di ordine N e banda passante Wn
%
N=4;
Wn=[1 2]*2*pi ;      %stop-pass: Wn=[1 2]*2*pi ;  pass-band: [0, 1 - Delta] U [2 + Delta, inf] 
                     %                            Delta non si conosce a priori

% Determine zeros and poles
[z,p,k] = butter(N,Wn,'s');
disp('Poles are at');disp(p);
%  Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Butterworth = %d', N) );
Legend ('Zeri   x  Poli ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5*2*pi];
% Compute and plot frequency response
h = freqs(pz,pp,omega);
h3=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 5 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('B.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 5 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('B.P. Butterworth  = %d', N) );

%Stop-band (eliminabanda)


%3) Passa-banda
% Specifiche in termni di ordine N e banda passante Wn
%
N=4;
Wn=[1 2]*2*pi ;      %stop-pass: Wn=[1 2]*2*pi ;  pass-band: [0, 1 - Delta] U [2 + Delta, inf] 
                     %                            Delta non si conosce a priori
                     
% Determine zeros and poles
[z,p,k] = butter(N,Wn,'stop','s');
disp('Poles are at');disp(p);
%  Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Butterworth = %d', N) );
Legend ('Zeri   x  Poli ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5*2*pi];
% Compute and plot frequency response
h = freqs(pz,pp,omega);
h4=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 5 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('B.S. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 5 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('B.S. Butterworth  = %d', N) );

% 
%Comparison
%
figure
plot (omega,20*log10(abs(h1)),omega,20*log10(abs(h2)),omega,20*log10(abs(h3)),omega,20*log10(abs(h4)));grid
xlabel('Frequency, Hz'); ylabel('Gain, dB');
legend ('Butterworth  N=6');
% frequency is normalized with respect cut-off frequency;
% indeed cut-off frequency is 1
figure;
plot (omega,angle(h1),omega,angle(h2),omega,angle(h3),omega,angle(h4));grid
xlabel('Frequency, Hz'); ylabel('Phase');
legend ('Butterworth  N=6');
