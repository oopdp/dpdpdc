% P2_fixed_oder_comparison.m
% Compaison of 6-th Order Analog Lowpass Filters of variious type  
%
% In questo caso � fissato l' ordine dei filtri e la banda passante (posta a 1 Hz); il resto di specifiche 
% vengono come vengono. Questa situazione si incontra quando appunto N e Wn sono specifiche fondamentali e 
% le altre sono secondarie 
%
%Pich� si forza l' ordine e la banda passante si usano le istruzioni per
%generare i filtri  "prototipi" con (banda passante fissata a) Wn=1, ossia: 

%[z,p,k] = buttap(N);
%[z,p,k] = cheb1ap(N);
%[z,p,k] = cheb2ap(N);
%[z,p,k] = ellipap(N);
%[z,p,k] = besselap(N);

%Queste istruzini producno filtri con cut-off a -3 dB in Wn=2*pi*fn=1 
%Pertanto l'ascissa dei grafici � W/Wn=2*pi*f/Wn, indicata con il termine di"normalized pulation"
%
close all
clear all
clc


N=6;
format long
% Determine zeros and poles
[z,p,k] = buttap(N);
disp('Poles are at');disp(p);
% Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Butterworth = %d', N) );
Legend ('Zeri   x Poli  ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5];
% Compute and plot frequency response
h1 = freqs(pz,pp,omega);
figure;
plot (omega,20*log10(abs(h1)));
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. Butterworth N=6');
figure;
plot (omega,angle(h1));
xlabel('Normalized pulsation'); ylabel('Phase');
legend ('L.P. Butterworth N=6');
%
% 6-th Order Analog Chebychev1 Lowpass Filter Design
%
format long
% Determine zeros and poles
[z,p,k] = cheb1ap(N,1);
disp('Poles are at');disp(p);
% Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Chebychev1 = %d', N) );
Legend ('Zeri   x Poli  ');
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5];
% Compute and plot frequency response
h2 = freqs(pz,pp,omega);
figure;
plot (omega,20*log10(abs(h2)));
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. Chebychev1 N=6');
figure;
plot (omega,angle(h2));
xlabel('Normalized pulsation'); ylabel('Phase');
legend ('L.P. Chebychev1 N=6');


% 6-th Order Analog Elliptic Lowpass Filter Design
%
format long
% Determine zeros and poles
[z,p,k] = ellipap(N,1,40);
% Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Elliptic  = %d', N) );
Legend ('Zeri     x Poli  ');
disp('Poles are at');disp(p);
% Determine transfer function coefficients
[pz, pp] = zp2tf(z, p, k);
% Print coefficients in descending powers of s
disp('Numerator polynomial'); disp(pz)
disp('Denominator polynomial'); disp(real(pp))
omega = [0: 0.01: 5];
% Compute and plot frequency response
h3 = freqs(pz,pp,omega);
figure;
plot (omega,20*log10(abs(h3)));
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. Elliptic N=6');
figure;
plot (omega,angle(h3));
xlabel('Normalized pulsation'); ylabel('Phase');
legend ('L.P. Elliptic N=6');


% Uso di Besselap:
%The cutoff or 3dB frequency is equal to 1 
%for N = 1 and decreases as N increases.

% Determine the coefficients of the transfer function
[z,p,k] = besselap(N);
% Plot zeros and poles 
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Bessel = %d', N) );
Legend ('Zeri     x  Poli ');
% Determine transfer function coefficients
[num, den] = zp2tf(z, p, k);
omega = [0: 0.01: 5];% questa � una frequenza anche se indicata con "omega"
% Compute and plot frequency response
h5 = freqs(num,den,omega);
figure;
plot (omega,20*log10(abs(h5)));
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. Bessel N=6');
figure;
plot (omega,angle(h5));
xlabel('Normalized pulsation'); ylabel('Phase');
legend ('L.P. Bessel N=6');
figure;
% 
%Comparison
%
plot (omega,20*log10(abs(h1)),omega,20*log10(abs(h2)),omega,20*log10(abs(h3)),omega,20*log10(abs(h5)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. N=6');
% frequency is normalized with respect cut-off frequency;
% indeed cut-off frequency is 1
figure;
plot (omega,angle(h1),omega,angle(h2),omega,angle(h3),omega,angle(h5));grid
xlabel('Normalized pulsation'); ylabel('Phase');
legend ('L.P. N=6');

