% P1_Lowpass_design.m
% Esemplifica il progetti di un filtro passabasso di specifiche note tramire 
% filtri di vario tipo;

%La prima volta che si vede il codice si osservi che il progetto avviene
%tramite i seguenti comandi 
% 1)  Butterworth 
% 
%[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num_bu, den_bu]=butter(N,Wn,'s');          % il progeto richiede 2 parametri N,Wn
%
% 2)  Chebychev 1
% 
%[N,Wn]= cheb1ord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num_c, den_c]=cheby1(N,Rp,Wn,'s');          % il progeto richiede 3 parametri N,Rp,Wn
%
% 3)  Chebychev 2
% 
%[N,Wn]= cheb2ord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num_c2, den_c2]=cheby2(N,Rp,Wn,'s');          % il progeto richiede 3 parametri N,Rp,Wn
%
% 4)  Elliptic
% 
%[N,Wn]= ellipord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
%[num_e, den_e]=ellip(N,Rp,Rs,Wn,'s');          % il progeto richiede 4 parametri N,Rp,Rs,Wn
%
% tutti le altre istruzioni servono solo a fare i grafici



%
close all
clear all
clc

%
% Specifiche 
%
Fp=1000;    % [0, Fp] banda passante in Hz
Fs=3000;     % [Fs, inf] banda attenuata in Hz
Rp=1;       % Ripple in dB in  banda passante 
Rs=40;      % Ripple in dB in  banda attenuata
format long
%
% 1)  Butterworth 
% 
[N,Wn]= buttord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num_bu, den_bu]=butter(N,Wn,'s');          % il progeto richiede 2 parametri N,Wn


%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num_bu,den_bu,omega);
h1=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Butterworth = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Butterworth = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num_bu)
p=roots(den_bu);
figure;
zplane(z,p);
title( sprintf('Poli di L.P. Butterworth = %d', N) );
Legend ('Zeri  x Poli')

%
% 2)  Chebychev 1
% 
[N,Wn]= cheb1ord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num_c, den_c]=cheby1(N,Rp,Wn,'s');          % il progeto richiede 3 parametri N,Rp,Wn


%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num_c,den_c,omega);
h2=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Chebychev 1 = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Chebychev 1 = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num_c)
p=roots(den_c);
figure;
zplane(z,p);
title( sprintf('Poli di L.P. Chebychev 1 = %d', N) );
Legend ('Zeri  x Poli')

%
% 3)  Chebychev 2
% 
[N,Wn]= cheb2ord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num_c2, den_c2]=cheby2(N,Rp,Wn,'s');          % il progeto richiede 3 parametri N,Rp,Wn


%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num_c2,den_c2,omega);
h3=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Chebychev 2 = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Chebychev 2 = %d', N) );

%  Compute and plot zeros and poles 
z=roots(num_c2)
p=roots(den_c2);
figure;
zplane(z,p);
title( sprintf('Poli di L.P. Chebychev 2 = %d', N) );
Legend ('Zeri  x Poli')
%
% 4)  Elliptic
% 
[N,Wn]= ellipord(2*pi*Fp,2*pi*Fs,Rp,Rs,'s');
[num_e, den_e]=ellip(N,Rp,Rs,Wn,'s');          % il progeto richiede 4 parametri N,Rp,Rs,Wn


%  Compute and plot frequency response 
omega = [0: 200: 2*pi*6000];
h = freqs(num_e,den_e,omega);
h4=h;

figure;
plot (omega/(2*pi),20*log10(abs(h)));grid
axis([0 6000 min(20*log10(abs(h)))  max(20*log10(abs(h)))]);  
xlabel('Frequency, Hz'); ylabel('Gain, dB');
title( sprintf('L.P. Elliptic = %d', N) );
figure;
plot (omega/(2*pi),angle(h));
axis([0 6000 min(angle(h))  max(angle(h))]);  
xlabel('Frequency, Hz'); ylabel('Phase');
title( sprintf('L.P. Elliptic = %d', N) );

%  Compute and plot zeros and poles 

%  Compute and plot zeros and poles 
p=roots(den_e);
z=roots(num_e);
figure;
zplane(z,p);
title( sprintf('Poli e zeri di L.P. Elliptic = %d', N) );
Legend ('Zeri  x Poli')

% 
%Comparison
%
figure
plot (omega,20*log10(abs(h1)),omega,20*log10(abs(h2)),omega,20*log10(abs(h3)),omega,20*log10(abs(h4)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P.');
% frequency is normalized with respect cut-off frequency;
% indeed cut-off frequency is 1
figure;
plot (omega,angle(h1),omega,angle(h2),omega,angle(h3),omega,angle(h4));grid
xlabel('Frequency, Hz'); ylabel('Phase');
legend ('L.P. filters for Fp=1000, Fs=3000, Rp=1, Rs=40');

