% Pulisce il workspace
% Pulisce il command window
% Chiude le figure

clear all;                      
clc;                         
close all;                      

% punto a:
% x_1(t)=sin(2pif_1t) f_1=1 kHz
% x_2(t)=sin(2pif_2t) f_2=4 kHz
% F_c=10kHz

% Frequenza di campionamento
F_c = 1e4;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 1e3;
f_2 = 4e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=1 kHz, F_c=10 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=4 kHz, F_c=10 kHz');





%punto b:
% x_1(t)=sin(2pif_1t) f_1=6 kHz
% x_2(t)=sin(2pif_2t) f_2=4 kHz
% F_c=10kHz

% Frequenza di campionamento
F_c = 1e4;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 6e3;
f_2 = 4e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=6 kHz, F_c=10 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=4 kHz, F_c=10 kHz');



%punto c:
% x_1(t)=sin(2pif_1t) f_1=1 kHz
% x_2(t)=sin(2pif_2t) f_2=9 kHz
% F_c=10kHz

% Frequenza di campionamento
F_c = 1e4;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 1e3;
f_2 = 9e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=1 kHz, F_c=10 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=9 kHz, F_c=10 kHz');




%punto d:
% x_1(t)=sin(2pif_1t) f_1=1 kHz
% x_2(t)=sin(2pif_2t) f_2=4 kHz
% F_c=100 kHz

% Frequenza di campionamento
F_c = 1e5;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 1e3;
f_2 = 4e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=1 kHz, F_c=100 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=4 kHz, F_c=100 kHz');




%punto e:
% x_1(t)=sin(2pif_1t) f_1=6 kHz
% x_2(t)=sin(2pif_2t) f_2=4 kHz
% F_c=100 kHz

% Frequenza di campionamento
F_c = 1e5;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 6e3;
f_2 = 4e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=6 kHz, F_c=100 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=4 kHz, F_c=100 kHz');




%punto f:
% x_1(t)=sin(2pif_1t) f_1=1 kHz
% x_2(t)=sin(2pif_2t) f_2=9 kHz
% F_c=100 kHz

% Frequenza di campionamento
F_c = 1e5;
T_c = 1/F_c;
n = 0:50;

% Frequenza del segnale sinusoidale
f_1 = 1e3;
f_2 = 9e3;

% Definizione del segnale sinusoidale
x_1 = sin(2 * pi * f_1 * n * T_c);
x_2 = sin(2 * pi * f_2 * n * T_c); 

% Creazione grafici
figure

% Rappresentazione del segnale x_1
subplot (1,2,1); 
stem(n, x_1);                    
xlabel('n');                   
ylabel('x_1(n)');              
title('f_1=1 kHz, F_c=100 kHz');

% Rappresentazione del segnale x_2
subplot (1,2,2); 
stem(n, x_2);                     
xlabel('n');                    
ylabel('x_2(n)');               
title('f_2=9 kHz, F_c=100 kHz');

