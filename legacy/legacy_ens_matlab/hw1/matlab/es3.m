% pulizia environment
clear all;
close all;
clc;

% equazione alle differenze:
% y(n) - 0.3y(n - 1) - 0.04y(n - 2) = x(n) + 2x(n - 1)

% obiettivo: calcolo della risposta impulsiva, cioe' x(n)=d(n)

% coefficienti d (y(n))
%~ d= [1 -0.3 -0.04];
d = [1 0 0 0 0 0 0 -1];
% coefficienti p (x(n))
p= [1 1 1 1];
% # campioni da calcolare
n_c=101;

% calcolo della risposta impulsiva:
[h,m]=impz(p,d,n_c);


% grafico risposta impulsiva:
n = 0:100;
my_h = 0.4*(2).^n +0.6*(-3).^n;


% grafico risposta impulsiva:
figure;
subplot(2,1,1);
stem(h, m);               
xlabel('n');                  
ylabel('h(n)');               
title('Risp. implusiva del sistema via impz');     
legend('h(n)');


subplot(2,1,2);
stem(my_h, n);               
xlabel('n');                  
ylabel('h(n)');               
title('Risp. implusiva del sistema -  Calcolo manuale');     
legend('h(n)');


