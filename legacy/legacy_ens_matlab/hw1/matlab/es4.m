% pulizia environment
clear all;
close all;
clc;

% equazione alle differenze del sistema:
% y(n) - 1.9921y(n - 1) + 0.9972y(n - 2) = x(n) - 1.9949x(n - 1) + x(n - 2).

% obiettivo 1: calcolo della risposta impulsiva, cioe' x(n)=d(n)
% coefficienti d (y(n))
d_k= [1 -1.9921 0.9972];
% coefficienti p (x(n))
p_k= [1 -1.9949 1];
% # campioni da calcolare
n_c=5000;

% calcolo della risposta impulsiva:
[h,m]=impz(p_k,d_k,n_c);

% grafico risposta impulsiva:
figure;
plot(m, h);               
xlabel('n');                  
ylabel('h(n)');               
title('h(n) - Risp. implusiva del sistema');     
legend('h(n)');
grid;
axis([-100 2000 -0.1 1.1]);
% obiettivo 2: lettura file audio
% File audio di ingresso
% vettore campioni s
% Fs frequenza campionamento wav
% bit codifica campioni
[s,Fs,Nbits] = wavread('bach_gould.wav');
% n campioni file wav
disp('Numero di campioni file wav:')
n_samp=size(s)
disp('Frequenza di campionamento file wav:')
Fs

% obiettivo 3: aggiunta disturbo sinusoidale
% Numero di campioni
n = 0:(n_samp-1);
% Frequenza del segnale sinusoidale
f0 = 500;
Ts=1/Fs;
% Definizione del disturbo sinusoidale
d = sin(2 * pi * f0 * n * Ts); 

%grafico disturbo sinusoidale
figure;
plot(n(1,1:500), d(1,1:500));
xlabel('n');
ylabel('d(n)');
legend('d(n)');
grid;               
title('d(n) - Distrubo sinusoidale (primi 500 campioni)');

%grafico segnale
figure;
% plot(n(1,1:500), s(1:500,1));
plot(n(1,1:500), s(10000:10499,1));
xlabel('n');                    
ylabel('s(n)');
legend('s(n)');
grid;
title('s(n) - Segnale audio (campioni da 10.000T_s a 10500T_s)');

x=s'+d;

%grafico segnale disturbato
figure;
% plot(n(1,1:500), x(1,1:500));
plot(n(1,1:500), x(1,10000:10499));
xlabel('n');                    
ylabel('x(n)');
legend('x(n)');               
title('x(n) - Segnale audio distrubato (campioni da 10.000T_s a 10500T_s)');
grid;

% obiettivo 4: filtraggio del segnale disturbato attraverso il
% sistema con eq alle differenze precedente

% condizioni iniziali NULLE 
si=[0 0];
%~ [y,sf] = filter (p_k,d_k,x,si);
[y,sf] = filter (p_k,d_k,x);
figure;
plot(n(1,1:500), y(1,10000:10499));
xlabel('n');                    
ylabel('y(n)');
legend('y(n)');
grid;               
title('y(n) - Segnale audio filtrato (campioni da 10.000T_s a 10500T_s)');

% obiettivo 5: salvataggio x(n), y(n) formato wav e riproduzione
% File audio di uscita
disp('Riproduzione segnale audio originario:')
%wavplay(s,Fs);
soundsc(s,Fs,Nbits);
disp('Riproduzione segnale audio disturbato:')
%wavplay(x,Fs);
soundsc(x,Fs,Nbits);
disp('Riproduzione segnale audio filtrato:')
%wavplay(y,Fs);
soundsc(y,Fs,Nbits);

disp('Salvataggio tracce elaborate..')
wavwrite(y,Fs,Nbits,'filtered_signal.wav');
wavwrite(x,Fs,Nbits,'noised_signal.wav');


% obiettivo 6: calcolo trasformate e stampa relativi moduli

% campioni fft e asse frequenze
N=4000; 
F = -Fs/2:Fs/N:Fs/2-Fs/N;

% modulo trasformata di x 
X = fftshift(fft(x,N));
figure;
plot(F,abs(X));
grid;
axis([-22000 22000 0 max(abs(X))]);
legend('|X(j\Omega)|');
title('|X(j\Omega)| - modulo della trasformata del segnale originario');

% modulo trasformata di s
S = fftshift(fft(s,N));
figure;
plot(F,abs(S));
grid;
axis([-22000 22000 0 max(abs(S))]);
legend('|S(j\Omega)|');
title('|S(j\Omega)| - modulo della trasformata del segnale distrurbato');

% modulo trasformata di h
H = fftshift(fft(h,N));
figure;
plot(F,abs(H));
grid;
axis([-2000 2000 0 max(abs(H))]);
legend('|H(j\Omega)|');
title('|H(j\Omega)| - modulo della risposta in frequenza del sistema');

% modulo trasformata di y
Y = fftshift(fft(y,N));
figure;
plot(F,abs(Y));
grid;
axis([-22000 22000 0 max(abs(Y))]);
legend('|Y(j\Omega)|');
title('|Y(j\Omega)| - modulo della trasformata del segnale filtrato');
