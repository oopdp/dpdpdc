% minim_phase_7.m
% Design of a minimum-phase lowpass FIR filter according to the specs of 
% Example 10.22 of Mitra. Eritten from Program 10_3.m of Mitra
% 
% !!! Operation needs function minphase_GUIDO.m
%
clear all;
close all;
clc;

Wp = 0.45; Ws = 0.6; Rp = 2; Rs = 26;
% Desired ripple values of minimum-phase filter
dp = 1- 10^(-Rp/20); ds = 10^(-Rs/20);
% Compute ripple values of prototype linear-phase filter
Ds = (ds*ds)/(2 - ds*ds);
Dp = (1 + Ds)*((dp + 1)*(dp + 1) - 1);
% Estimate filter order
[N,fpts,mag,wt] = remezord([Wp Ws], [1 0], [Dp Ds]);
% Design the prototype linear-phase filter H(z)
[b,err,res] = remez(N, fpts, mag, wt);
K = N/2;
b1 = b(1:K);
% Design the linear-phase filter G(z)
c = [b1 (b(K+1) + res.error(length(res.error))) fliplr(b1)]/(1+Ds);
figure
zplane(c) % Plot the zeros of G(z)

% variazine del come in Program 10_3 oriinale di MITRA che evita la
% chiamata alla funziona  minpase( ) perch� non funzina !!
c1 = c(K+1:N+1);  
[y, ssp, iter] = minphase_GUIDO(c1);

% [y] = firminphase(c) % firminphase(c) � un'alternativa a c1 = c(K+1:N+1); [y, ssp, iter] = minphase(c1)


% Check results with  Gremez (II modo) 


% Maximum phase filter 

