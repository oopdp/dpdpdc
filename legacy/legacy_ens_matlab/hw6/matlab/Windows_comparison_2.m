% Windows_comparison_2.m
%
% Kaiser Window Generation: MITRA, Example 10.24 and 10.25 + Comparison with
% all other window

% -Estimate the Kaiser window length order + beta
% -Kaiser window generation 
% -Generation of the FIR filter h(n)= sin(omega_c * n)/ (pi * n) * w(n) via fir1
   % B = FIR1(N,Wn,WIN) designs an N-th order FIR filter using 
   % the N+1 length vector WIN to window the impulse response.
   % If empty or omitted, FIR1 uses a Hamming window of length N+1.
   % For a complete list of available windows, see the help for the
   % WINDOW function. KAISER and CHEBWIN can be specified with an 
   % optional trailing argument.  For example, B = FIR1(N,Wn,kaiser(N+1,4)) 
   % uses a Kaiser window with beta=4. B = FIR1(N,Wn,'high',chebwin(N+1,R)) 
   % uses a Chebyshev window with R decibels of relative sidelobe 
   % attenuation.
% - repeat the above for all other windows  
%
clc;
clear all;
close all;

% 1) Filter specs:

%a) bandedges
Wp = 0.3 ; Ws = 0.4 ;   
fpts =[Wp Ws];
%b) desired magnitude values
mag = [ 1  0 ];         
%d) desired magnitude tolerances
Rs = 50;                % Desired stop-band attenuation in dB ( Note that Rp  cannot be specified since because of  the  
%the  windows symmetry it must be dp=ds, i.e. Rp will be a value leading to dp=ds) ; 
ds = 10^(-Rs/20);       % Desired stop-band ripple values 
dp=ds;                  % because of the above noted symmetry of windows 
dev=[dp ds];

% 2)  Kaiser Window
% 2.1) Estimate the Kaiser window length order + beta
[N,Wn,beta,ftype] = kaiserord(fpts,mag,dev)

% 2.2) Kaiser window generation 

w = kaiser(N+1,beta); w = w/sum(w); 

% Amplitude response plot
[W,omega] = freqz(w,1,512);
plot(omega/pi,20*log10(abs(W)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf('Required Kaiser Window with N = %d',N )) 
Legend ( sprintf('Beta = %d', beta) );

% 2.3) Generation of the FIR filter h(n)= sin(omega_c*n)/ (pi * n) * w(n) 

h = fir1(N,Wn,'low',w);   %o.k. also  h = fir1(N,Wn,'low',kaiser(N+1,beta))

% Amplitude response plot
[H,omega] = freqz(h,1,512);
% Amplitude response plot
figure
plot(omega/pi,20*log10(abs(H)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf('FIR filter obtained from the Kaiser Window with N = %d',N )) 


figure
zplane(h,1);
title(sprintf('zeros of the FIR filter with N = %d',N ))

% 4) FIR design by way of the other windows a Hamming window (default in fir1) 

% 2) indow generation 

w1 = hamming(N+1); w1 = w1/sum(w1); 
w2 = blackman(N+1); w2 = w2/sum(w2); 
w3 = hanning(N+1); w3 = w3/sum(w3); 
w4 = chebwin(N+1,Rs); w4 = w4/sum(w4);  

[W1,omega] = freqz(w1,1,512);
[W2,omega] = freqz(w2,1,512);
[W3,omega] = freqz(w3,1,512);
[W4,omega] = freqz(w4,1,512);


% 
%Comparison
%
%Comparison
%
figure
plot (omega,20*log10(abs(W)),omega,20*log10(abs(W1)),omega,20*log10(abs(W2)),omega,20*log10(abs(W3)),omega,20*log10(abs(W4)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. FIR filter obtained from the various windows');




h1 = fir1(N,Wn);          % Hamming window (default in fir1) 
h2 = fir1(N,Wn,'low',w2);   % Blackman window  
h3 = fir1(N,Wn,'low',w3);   % Hanning window  
h4 = fir1(N,Wn,'low',w4);   % Dolph-Chebycev window 

[H1,omega] = freqz(h1,1,512);
[H2,omega] = freqz(h2,1,512);
[H3,omega] = freqz(h3,1,512);
[H4,omega] = freqz(h4,1,512);

% 
%Comparison
%
figure
plot (omega,20*log10(abs(H)),omega,20*log10(abs(H1)),omega,20*log10(abs(H2)),omega,20*log10(abs(H3)),omega,20*log10(abs(H4)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend ('L.P. FIR filter obtained from the various windows');







