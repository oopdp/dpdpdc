clear all;
close all;
clc;

Wp = 0.45; Ws = 0.6; Rp = 2; Rs = 26;
dp = 1- 10^(-Rp/20); ds = 10^(-Rs/20);
Ds = (ds*ds)/(2 - ds*ds);
Dp = (1 + Ds)*((dp + 1)*(dp + 1) - 1);
[N,fpts,mag,wt] = remezord([Wp Ws], [1 0], [Dp Ds]);
[b,err,res] = remez(N, fpts, mag, wt);
K = N/2;
b1 = b(1:K);
c = [b1 (b(K+1) + res.error(length(res.error))) fliplr(b1)]/(1+Ds);
figure
zplane(c)

c1 = c(K+1:N+1);  
[y, ssp, iter] = minphase_GUIDO(c1);

my_g = gremez(N, fpts, mag, wt);
figure
zplane(my_g)

my_grem = gremez(N, fpts, mag, wt, 'minphase');
figure
zplane(my_grem)

figure
zplane(y)

my_grem_max = gremez(N, fpts, mag, wt, 'maxphase');
figure
zplane(my_grem_max)
