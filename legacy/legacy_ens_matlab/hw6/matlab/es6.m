% TRACCIA_HWK_6_ES_5.m
% ( data from MITRA Es. M 10.30 )
%
% Example of IFIR design 
%
clc;
clear all;
close all;

% 1)  Narrow-band low-pass filter specs 

FT=16000;
fp=800;
fs=1200;
delta_p=0.001;
delta_s=0.001;

% Translate frequency specs with respect to the Matlab conventions
% (sampling freq.=2)and creation of the parameters required by firgr

[N,fpts,mag,wt] = remezord([fp fs], [1 0], [delta_p delta_s], FT);

%~ fpts =  [0 Wp Ws  1];        % bandedges already normalized to sampling frequency FT=2
%~ mag = [1 1  0 0];            % desired magnitude values in each band 
%~ dev = [delta_p  delta_s];    % tolerance values in each band   

% 1A) Filter design
b=remez(N,fpts,mag,wt);

N_SS= length(b);              % number of coefficients of the rerquired single stage filter
R_SS= ceil(N_SS/2);           % number of multiplications per output sample of the rerquired single stage filter
disp(sprintf('Single stage filte number of multiplications per output, R_SS = %d', R_SS ))


% 1B) Visual inspection 

% Implulse response

N = N_SS -1;
figure
x=[0:N];  
stem(x,b);          %  STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Pass-band zoom of the requency response 
Np1=ceil(256* fpts(1)) + 1;
Np2=ceil(256* fpts(2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend ('Pass-band error')

% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 


% 2)  Design of IFIR G(z)= F (z^{(L)} I(z) according to the above specs

% 2A) Design of F(z) 

% Specs of  F(z) 
%~ .                                    %  determination of L fron constraint: L < pi/omega_s
%~ .
%~ .
w_p=fpts(2)
w_s=fpts(3)
L=floor(w_s^-1)
disp(sprintf('IFIR with L = %d', L ))

%~ .
%~ .                                %  determine specs of F(z)
%~ .
%~ .

w_p_f=w_p*L;
w_s_f=w_s*L;
delta_p_f=delta_p/2;
delta_s_f=delta_s;

% Filter design of F(z)

%~ .
%~ .
%~ .
%~ .
[N_F,fpts,mag,wt] = remezord([w_p_f w_s_f], [1 0], [delta_p_f delta_s_f]);
b_F=remez(N_F,fpts,mag,wt);

N_F= length(b_F);           % number of coefficients of F(z)
R_F= ceil(N_F/2);           % number of multiplications per output sample of F(z)
disp(sprintf('IFIR: F(z) number of multiplications per output, R_F = %d', R_F ))


% Create the impulse response of F(z^{(L)}

%~ .
%~ .
%~ .
%~ .
N_F_L=N_F*L;
b_F_L=0*[0:N_F_L];
for k=1:length(b_F)
	b_F_L(k*L)=b_F(k);
end

% Visual inspection 

% Implulse response

N = length(b_F_L)-1; 
b= b_F_L;
x=[0:N];  

figure
stem(x,b);                  % STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');
legend(sprintf('IFIR: F(z^{(L)}) with L = %d', L))

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
h_F_L= h;
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 
legend(sprintf('IFIR: F(z^{(L)}) with L = %d', L))

% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 

% 2B) Design of I(z) 

% Specs of  I(z) 
omega_s=w_s;
Wp=w_p;

omega_s_I= 2*pi/L - omega_s ;   %  stop-band of I(z)

w_p_I= Wp;                       %  determine specs of I(z)
w_s_I= omega_s_I/pi; 
delta_p_I=delta_p/2;
delta_s_I=delta_s;



% Filter design of I(z)

[N_I,fpts,mag,wt] = remezord([w_p_I w_s_I], [1 0], [delta_p_I delta_s_I]);
b_I=remez(N_I,fpts,mag,wt);

%~ .
%~ .
%~ .
%~ .
N_I= length(b_I);           % number of coefficients of the rerquired single stage filter
R_I= ceil(N_I/2);           % number of multiplications per output sample of the rerquired single stage filter
disp(sprintf('IFIR: I(z) number of multiplications per output, R_I = %d', R_I ))
R_IFIR= R_F + R_I;
disp(sprintf('IFIR: total number of multiplications per output, R_IFIR = %d', R_IFIR  ))

% Visual inspection 

% Implulse response

N = N_I - 1;                % since the computatio of the coefficients begins from 0 
b= b_I;

figure
x=[0:N];  
stem(x,b);                  % STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');
legend(sprintf('IFIR: I(z}) with L = %d', L))

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 
legend(sprintf('IFIR: I(z}) with L = %d', L))

h_I=h;

% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 


% 2C)  Visual inspection of the IFIR cascade G(z)= F(z^{(L)} I(z)

% Filters separately plotted

figure
plot(w/pi,20*log10(abs(h_F_L)),'-r',w/pi,20*log10(abs(h_I)),'--b') ;
grid;
title(' Single filters')
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend(' F(z^{6})','I(z)')

% Magnitude of the requency response of the IFIR cascade G(z)= F(z^{(L)} I(z) 

h1= h_I .* h_F_L;                 % composite frequency response
h=h1;
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title('Cascade') 
legend(sprintf('IFIR: G(z)= F(z^{(L)})* I(z),  with L = %d', L))

% Pass-band zoom of the composite frequency response
Np1=ceil(256* fpts(1)) + 1;
Np2=ceil(256* fpts(2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend ('Pass-band error')
legend(sprintf('IFIR: G(z)= F(z^{(L)})* I(z),  with L = %d', L))


% 3) Design via Matlab command IFIR 

% IFIR  Interpolated FIR filter design.
%    [H,G] = IFIR(L,TYPE,F,DEV) finds a periodic filter H(z^L) where L is
%    the interpolation factor and an image-suppressor filter G(z) such that
%    the cascade of the two filters represents the optimal minimax FIR
%    approximation to the desired response specified by TYPE with bandedge
%    frequencies contained in vector F while not exceeding the maximum
%    deviations or ripples (in linear units) specified in vector DEV.
% 
%    TYPE must be a string with either 'low' for lowpass designs or 'high'
%    for highpass designs.  F must be a two-element vector with passband and
%    stopband edge frequency values. For narrowband lowpass filters and
%    wideband highpass filters, L*F(2) should be less than 1. For wideband
%    lowpass filters and narrowband highpass filters, L*(1-F(1)) should be
%    less than 1.
% 
%    DEV must contain the peak ripple or deviation (in linear units) allowed
%    for both the passband and the stopband, i.e., it also must be a
%    two-element vector.


% 3A) design via Matlab command  IFIR

%~ .
%~ .
%~ .
%~ .

w_p=2*fp/FT;
w_s=2*fs/FT;

[f,i]=ifir(L,'low',[w_p, w_s],[delta_p, delta_s],'advanced');

N_f_L= length(f);           % number of coefficients of the periodic section
N_f=N_f_L/L ;
R_f= ceil(N_f/2);         % number of multiplications of the periodic section
disp(sprintf('IFIR: F(z) number of multiplications per output, R_f = %d', R_f ))

N_i= length(i);           % number of coefficients of I(z)
R_i= ceil(N_i/2);         % number of multiplications per output sample of I(z)
disp(sprintf('IFIR: I(z) number of multiplications per output, R_i = %d', R_i ))

R_IFIR= R_f + R_i;
disp(sprintf('IFIR: total number of multiplications per output, R_IFIR = %d', R_IFIR  ))

% 3B) Visual inspection of F(z^{(L)}

% Implulse response

N = N_f_L -1;                 % since the computatio of the coefficients begins from 0 
b= f;

figure
x=[0:N];  
stem(x,b);                  % STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');
legend(sprintf('MATLAB IFIR: F(z^{(L)}) with L = %d', L))

% Magnitude of the frequency response 
[h,w] = freqz(b,1,256);
h_F_L= h;
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 
legend(sprintf('MATLAB IFIR: F(z^{(L)}) with L = %d', L))

% Zeros plot
figure 
zplane(b,1)
title(sprintf(' MATLAB IFIR zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 


% 3C) Visual inspection of I(z) 
  
% Implulse response

N = N_i -1;                    % since the computatio of the coefficients begins from 0 
b= i;

figure
x=[0:N];  
stem(x,b);                  % STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');
legend(sprintf('IFIR: I(z}) with L = %d', L))

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 
legend(sprintf('IFIR: I(z}) with L = %d', L))

h_I=h;

% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 

% 3C)  Visual inspection of the IFIR cascade G(z)= F(z^{(L)} I(z)

% Filters separately plotted

figure
plot(w/pi,20*log10(abs(h_F_L)),'-r',w/pi,20*log10(abs(h_I)),'--b') ;
grid;
title(' Single filters')
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend(' F(z^{6})','I(z)')

% Magnitude of the requency response of the IFIR cascade G(z)= F(z^{(L)} I(z) 

h2= h_I .* h_F_L;                 % composite frequency response
h=h2;
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title('Cascade') 
legend(sprintf('IFIR: G(z)= F(z^{(L)})* I(z),  with L = %d', L))

% Pass-band zoom of the composite frequency response
Np1=ceil(256* fpts(1)) + 1;
Np2=ceil(256* fpts(2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend ('Pass-band error')
legend(sprintf('IFIR: G(z)= F(z^{(L)})* I(z),  with L = %d', L))

% 4) Comparison between the two IFIR designs

% Comparison between the frequency response of the two IFIR 

figure
plot(w/pi,20*log10(abs(h1)),'-r',w/pi,20*log10(abs(h2)),'--b') ;
grid;
title(' Single filters')
xlabel('\omega/\pi'); ylabel('Gain, dB');
legend('IFIR by hand','IFIR by MATLAB')

% Error between the frequency response of the two IFIR 

e1=abs(h1(Np1:Np2))-abs(h2(Np1:Np2));
w1=wp(Np1:Np2);

figure
plot(w1/pi,e1);grid;
title(sprintf(' Pass-band error betwwen the 2 IFIR')) 
xlabel('\omega/\pi'); ylabel('|G1| - |G2| ');



