% Kaiser_Hamming_1.m
%
% Kaiser Window Generation: MITRA, Example 10.24 and 10.25: Comparison with
% the Hamming window

% -Estimate the Kaiser window length order + beta
% -Kaiser window generation 
% -Generation of the FIR filter h(n)= sin(omega_c*n)/ (pi * n) * w(n) via fir1
   % B = FIR1(N,Wn,WIN) designs an N-th order FIR filter using 
   % the N+1 length vector WIN to window the impulse response.
   % If empty or omitted, FIR1 uses a Hamming window of length N+1.
   % For a complete list of available windows, see the help for the
   % WINDOW function. KAISER and CHEBWIN can be specified with an 
   % optional trailing argument.  For example, B = FIR1(N,Wn,kaiser(N+1,4)) 
   % uses a Kaiser window with beta=4. B = FIR1(N,Wn,'high',chebwin(N+1,R)) 
   % uses a Chebyshev window with R decibels of relative sidelobe 
   % attenuation.
%
clc;
clear all;
close all;

% 1) Filter specs:

%a) bandedges
Wp = 0.3 ; Ws = 0.4 ;   
fpts =[Wp Ws];
%b) desired magnitude values
mag = [ 1  0 ];         
%d) desired magnitude tolerances
Rs = 50;                % Desired stop-band attenuation in dB ( Note that Rp  cannot be specified since because of  the  
%the  windows symmetry it must be dp=ds, i.e. Rp will be a value leading to dp=ds) ; 
ds = 10^(-Rs/20);       % Desired stop-band ripple values 
dp=ds;                  % because of the above noted symmetry of windows 
dev=[dp ds];

% 2) Haiser window
% 2.1) Estimate the Kaiser window length order + beta
[N,Wn,beta,ftype] = kaiserord(fpts,mag,dev)

% 2.2) Kaiser window generation 

w = kaiser(N+1,beta); w = w/sum(w); 

% Amplitude response plot
[W,omega] = freqz(w,1,256);
plot(omega/pi,20*log10(abs(W)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf('Required Kaiser Window with N = %d',N )) 
Legend ( sprintf('Beta = %d', beta) );

% 2.3) Generation of the FIR filter h(n)= sin(omega_c*n)/ (pi * n) * w(n) 

h = fir1(N,Wn,'low',w);   %o.k. also  h = fir1(N,Wn,'low',kaiser(N+1,beta))

% Amplitude response plot
[H,omega] = freqz(h,1,512);
% Amplitude response plot
figure
plot(omega/pi,20*log10(abs(H)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf('FIR filter obtained from the Kaiser Window with N = %d',N )) 

figure
zplane(h,1);
title(sprintf('zeros of the FIR filter with N = %d',N ))

% 3) FIR design by way of a Hamming window (default in fir1) 

% 3.2) Hamming window generation 

w1 = hamming(N+1); w1 = w1/sum(w1); 

% Amplitude response plot
[W1,omega] = freqz(w1,1,256);
figure
plot (omega,20*log10(abs(W)),omega,20*log10(abs(W1)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend (sprintf('Kaiser vs. Hamming windows for N = %d',N ) );

% 2.3) Generation of the FIR filter h(n)= sin(omega_c*n)/ (pi * n) * w(n) with w(n) Hamming window 

h1 = fir1(N,Wn);    % Hamming window default window for fir1

% Amplitude response plot
[H1,omega] = freqz(h1,1,512);
figure
plot (omega,20*log10(abs(H)),omega,20*log10(abs(H1)));grid
xlabel('Normalized pulsation'); ylabel('Gain, dB');
legend (sprintf('FIR deseigner by Kaiser vs. FIR designed by Hamming windows for N = %d',N ) );

figure
zplane(h1,1);
title(sprintf('zeros of the FIR filter with N = %d',N ))




