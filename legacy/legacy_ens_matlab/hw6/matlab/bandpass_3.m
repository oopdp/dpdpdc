% bandpass_3.m
%
% Example of how influence of the filter order N and of the weight w 
% in the design of equiripple Linear-Phase FIR filters
% Data from MITRA Example 10.16
%
clc;
clear all;
close all;


% 1)  First  bandpass filter specs  (MITRA Example 10.16)

N= 26;                             % filter order 
FT=2;
fpts =  [0 0.25 0.3 0.5 0.55 1];  % bandedges already normalized to sampling frequency FT=2
mag = [0 0 1 1  0 0];             % desired magnitude values in each band 
wt = [1 1 1];                     % weights in each band 

% Filter design

b = remez(N,fpts,mag,wt);

% Visual inspection 

% Implulse response
figure
x=[0:N];  
stem(x,b);          %  STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Pass-band zoom of the requency response 
Np1=ceil(256* fpts(3)/(FT/2));
Np2=ceil(256* fpts(4)/(FT/2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');

% Error between the frequency response magnitude and the desired target
% behaviour 
h1=abs(h(1:ceil(257* fpts(2)/(FT/2))));
w1=w(1:ceil(257* fpts(2)/(FT/2)));

h2=abs(h(Np1:Np2))-1;

h3=abs(h(ceil(257* fpts(5)/(FT/2)):256));
w3=w(ceil(257* fpts(5)/(FT/2)):256);

h4 =[ h1; h2; h3];
w4= [ w1; wp; w3];

figure
plot(w4/pi,h4);grid;
title(sprintf(' Magnitude error, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('|H| - D ');


% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 


% 2)  Second  bandpass filter specs  (higher  N= 110)

N= 110;                           % filter order 
FT=2;
fpts =  [0 0.25 0.3 0.5 0.55 1];  % bandedges already normalized to sampling frequency FT=2
mag = [0 0 1 1  0 0];             % desired magnitude values in each band 
wt = [1 1 1];                     % weights in each band 

% Filter design

b = remez(N,fpts,mag,wt);

% Visual inspection 

% Implulse response
figure
x=[0:N];  
stem(x,b);          %  STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Pass-band zoom of the requency response 
Np1=ceil(256* fpts(3)/(FT/2));
Np2=ceil(256* fpts(4)/(FT/2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');

% Error between the frequency response magnitude and the desired target
% behaviour 
h1=abs(h(1:ceil(257* fpts(2)/(FT/2))));
w1=w(1:ceil(257* fpts(2)/(FT/2)));

h2=abs(h(Np1:Np2))-1;

h3=abs(h(ceil(257* fpts(5)/(FT/2)):256));
w3=w(ceil(257* fpts(5)/(FT/2)):256);

h4 =[ h1; h2; h3];
w4= [ w1; wp; w3];

figure
plot(w4/pi,h4);grid;
title(sprintf(' Magnitude error, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('|H| - D ');


% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 


% Third  bandpass filter specs (same N as above but differnet  wt) 

N= 110;                           % filter order 
FT=2;
fpts =  [0 0.25 0.3 0.5 0.55 1];  % bandedges already normalized to sampling frequency FT=2
mag = [0 0 1 1 0 0];             % desired magnitude values in each band 
wt = [1 0.1 1];                   % weights in each band 

% Filter design

b = remez(N,fpts,mag,wt);

% Visual inspection 

% Implulse response
figure
x=[0:N];  
stem(x,b);          %  STEM(X,Y) plots the data sequence Y at the values specified in X.
title(sprintf('Implulse response for N = %d', N )) % to verify symmetry of impulse response 
xlabel('n'); ylabel('h(n)');

% Magnitude of the requency response 
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Pass-band zoom of the requency response 
Np1=ceil(256* fpts(3)/(FT/2));
Np2=ceil(256* fpts(4)/(FT/2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');

% Error between the frequency response magnitude and the desired target
% behaviour 
h1=abs(h(1:ceil(257* fpts(2)/(FT/2))));
w1=w(1:ceil(257* fpts(2)/(FT/2)));

h2=abs(h(Np1:Np2))-1;

h3=abs(h(ceil(257* fpts(5)/(FT/2)):256));
w3=w(ceil(257* fpts(5)/(FT/2)):256);

h4 =[ h1; h2; h3];
w4= [ w1; wp; w3];

figure
plot(w4/pi,h4);grid;
title(sprintf(' Magnitude error, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('|H| - D ');


% Zeros plot
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) % to verify the mirror image symmetry of the zeros 



