clc;
clear all;
close all;

%~ specifiche del filtro
Wp = 0.3 ; Ws = 0.4 ;   
fpts =[Wp Ws];
mag = [ 1  0 ];         
Rs = 50;                
ds = 10^(-Rs/20);       % Desired stop-band ripple values 
dp=ds;                  % because of the above noted symmetry of windows 
dev=[dp ds];

%~ realizzazione con finestra di Kaiser
[N,Wn,beta,ftype] = kaiserord(fpts,mag,dev) 
w = kaiser(N+1,beta);
w = w/sum(w); 

%~ plottaggio del modulo della funzione della trasferimento della finestra
[W,omega] = freqz(w,1,256);
%~ plot(omega/pi,20*log10(abs(W)));
%~ grid;
%~ xlabel('\omega/\pi'); 
%~ ylabel('Gain, dB');
%~ title(sprintf('Required Kaiser Window with N = %d',N )) 

%~ realizzazione del filtro
h = fir1(N,Wn,'low',w);   %o.k. also  h = fir1(N,Wn,'low',kaiser(N+1,beta))

%~ plottaggio del modulo della funzione della trasferimento del filtro
[H,omega] = freqz(h,1,512);
%~ figure
%~ plot(omega/pi,20*log10(abs(H)));
%~ grid;
%~ xlabel('\omega/\pi');
%~ ylabel('Gain, dB');
%~ title(sprintf('FIR filter obtained from the Kaiser Window with N = %d',N )) 
%~ figure
%~ zplane(h,1);
%~ title(sprintf('zeros of the FIR filter with N = %d',N ))

%~ realizzazione con finestra di Hamming
w1 = hamming(N+1); 
w1 = w1/sum(w1); 

%~ plottaggio del modulo della funzione della trasferimento della finestra
[W1,omega] = freqz(w1,1,256);
%~ figure
%~ plot (omega/pi,20*log10(abs(W1)));
%~ grid;
%~ xlabel('Normalized pulsation'); 
%~ ylabel('Gain, dB');
%~ title(sprintf('Required Hamming Window with N = %d',N )) 

%~ realizzazione del filtro
h1 = fir1(N,Wn);    % Hamming window default window for fir1

%~ plottaggio del modulo della funzione della trasferimento del filtro
[H1,omega] = freqz(h1,1,512);
%~ figure
%~ plot (omega/pi,20*log10(abs(H1)));grid
%~ xlabel('Normalized pulsation'); ylabel('Gain, dB');
%~ title(sprintf('FIR filter obtained from the Hamming Window with N = %d',N )) 
%~ figure
%~ zplane(h1,1);
%~ title(sprintf('zeros of the FIR filter with N = %d',N ))

%~ realizzazione con finestra di Blackman
w2 = blackman(N+1); w2 = w2/sum(w2); 

%~ plottaggio del modulo della funzione della trasferimento della finestra
[W2,omega] = freqz(w2,1,256);
%~ figure
%~ plot (omega/pi,20*log10(abs(W2)));
%~ grid;
%~ xlabel('Normalized pulsation'); 
%~ ylabel('Gain, dB');
%~ title(sprintf('Required Blackman Window with N = %d',N )) 

%~ realizzazione del filtro
h2 = fir1(N,Wn,w2);    

%~ plottaggio del modulo della funzione della trasferimento del filtro
[H2,omega] = freqz(h2,1,512);
%~ figure
%~ plot (omega/pi,20*log10(abs(H2)));grid
%~ xlabel('Normalized pulsation'); ylabel('Gain, dB');
%~ title(sprintf('FIR filter obtained from the Blackman Window with N = %d',N )) 
%~ figure
%~ zplane(h2,1);
%~ title(sprintf('zeros of the FIR filter with N = %d',N ))

%~ realizzazione con finestra di Dolph-Chebychev
w3= chebwin(N+1); w3 = w3/sum(w3); 

%~ plottaggio del modulo della funzione della trasferimento della finestra
[W3,omega] = freqz(w3,1,256);
%~ figure
%~ plot (omega/pi,20*log10(abs(W3)));
%~ grid;
%~ xlabel('Normalized pulsation'); 
%~ ylabel('Gain, dB');
%~ title(sprintf('Required Dolph-Chebychev Window with N = %d',N )) 

%~ realizzazione del filtro
h3 = fir1(N,Wn,w3);    

%~ plottaggio del modulo della funzione della trasferimento del filtro
[H3,omega] = freqz(h3,1,512);
%~ figure
%~ plot (omega/pi,20*log10(abs(H3)));grid
%~ xlabel('Normalized pulsation'); ylabel('Gain, dB');
%~ title(sprintf('FIR filter obtained from the Dolph-Chebychev Window with N = %d',N )) 
%~ figure
%~ zplane(h3,1);
%~ title(sprintf('zeros of the FIR filter with N = %d',N ))

%~ realizzazione con finestra di Van Hann
w4 = hann(N+1); w4 = w4/sum(w4); 
w5 = hanning(N+1); w5 = w5/sum(w5); 

%~ plottaggio del modulo della funzione della trasferimento della finestra
[W4,omega] = freqz(w4,1,256);
[W5,omega] = freqz(w5,1,256);
%~ figure
%~ plot (omega/pi,20*log10(abs(W5)),omega/pi,20*log10(abs(W4)));
%~ grid;
%~ xlabel('Normalized pulsation'); 
%~ ylabel('Gain, dB');
%~ title(sprintf('Required Van Hann/Hanning Window with N = %d',N )) 

%~ realizzazione del filtro
h4 = fir1(N,Wn,w4);    
h5 = fir1(N,Wn,w5);

%~ plottaggio del modulo della funzione della trasferimento del filtro
[H4,omega] = freqz(h4,1,512);
[H5,omega] = freqz(h5,1,512);
%~ figure
%~ plot (omega/pi,20*log10(abs(H4)),omega/pi,20*log10(abs(H5)));
%~ grid
%~ xlabel('Normalized pulsation');
%~ ylabel('Gain, dB');
%~ title(sprintf('FIR filter obtained from the Van Hann/Hanning Window with N = %d',N )) 
%~ figure
%~ zplane(h4,1);
%~ figure
%~ zplane(h5,1);
%~ title(sprintf('zeros of the FIR filter with N = %d',N ))

%~ plottaggio per confronto
%~ finestra di Blackman Vs. Dolph-Chebychev
figure
plot (omega/pi,20*log10(abs(H2)),omega/pi,20*log10(abs(H3)));
title('Blackman Vs. Dolph-Chebychev')
ylabel('Gain dB');
xlabel('\omega/\pi');
grid;
legend('Blackman','Dolph-Chebychev');

%~ finestra di Van Hann Vs. Hamming
%~ figure
%~ plot (omega/pi,20*log10(abs(H1)),omega/pi,20*log10(abs(H4)));
%~ title('Van Hann Vs. Hamming');
%~ ylabel('Gain dB');
%~ xlabel('\omega/\pi');
%~ grid;
%~ legend('Van Hann','Hamming');
