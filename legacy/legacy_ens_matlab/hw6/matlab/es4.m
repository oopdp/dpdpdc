clc;
clear all;
close all;

%~ Specifiche filtro 1

N= 60;
FT=2;
fpts = [0 0.25 0.3 0.5 0.6 1];
mag = [0 0 1 1 0 0];
wt = [3 3 1];

% Progetto del filtro
b = remez(N,fpts,mag,wt);

%~ Risposta impulsiva
figure
x=[0:N];  
stem(x,b);
title(sprintf('Implulse response for N = %d', N ));
xlabel('n'); ylabel('h(n)');

% Modulo risposta frequenza
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Zoom banda passante
Np1=ceil(256* fpts(3)/(FT/2));
Np2=ceil(256* fpts(4)/(FT/2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');


%~ Errore tra desiderata e progettata
h1=abs(h(1:ceil(257* fpts(2)/(FT/2))));
w1=w(1:ceil(257* fpts(2)/(FT/2)));
h2=abs(h(Np1:Np2))-1;
h3=abs(h(ceil(257* fpts(5)/(FT/2)):256));
w3=w(ceil(257* fpts(5)/(FT/2)):256);
h4 =[ h1; h2; h3];
w4= [ w1; wp; w3];
figure
plot(w4/pi,h4);grid;
title(sprintf(' Magnitude error, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('|H| - D ');


% Diagramma zeri poli
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) 

%~ -------------------------------------------


%~ Specifiche filtro 2
N= 60;
FT=2;
fpts = [0 0.25 0.3 0.5 0.55 1];
mag = [0 0 1 1 0 0];
wt = [1 1 1];

% Progetto del filtro
b = remez(N,fpts,mag,wt);

%~ Risposta impulsiva
figure
x=[0:N];  
stem(x,b);
title(sprintf('Implulse response for N = %d', N ));
xlabel('n'); ylabel('h(n)');

% Modulo risposta frequenza
[h,w] = freqz(b,1,256);
figure
plot(w/pi,20*log10(abs(h)));grid;
xlabel('\omega/\pi'); ylabel('Gain, dB');
title(sprintf(' N = %d',N )) 

% Zoom banda passante
Np1=ceil(256* fpts(3)/(FT/2));
Np2=ceil(256* fpts(4)/(FT/2))+ 2;
hp=h(Np1:Np2);
wp=w(Np1:Np2);
figure
plot(wp/pi,20*log10(abs(hp)));grid;
title(sprintf(' Pass-band, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('Gain, dB');


%~ Errore tra desiderata e progettata
h1=abs(h(1:ceil(257* fpts(2)/(FT/2))));
w1=w(1:ceil(257* fpts(2)/(FT/2)));
h2=abs(h(Np1:Np2))-1;
h3=abs(h(ceil(257* fpts(5)/(FT/2)):256));
w3=w(ceil(257* fpts(5)/(FT/2)):256);
h4 =[ h1; h2; h3];
w4= [ w1; wp; w3];
figure
plot(w4/pi,h4);grid;
title(sprintf(' Magnitude error, N = %d',N )) 
xlabel('\omega/\pi'); ylabel('|H| - D ');


% Diagramma zeri poli
figure 
zplane(b,1)
title(sprintf(' zeros for N = %d',N )) 
