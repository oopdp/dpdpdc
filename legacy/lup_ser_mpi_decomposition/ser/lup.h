/*
 *      main.h
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


//standard libs
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <mpi.h>

//symbols
#define MAX_ELEMENT_VALUE 100
#define MIN_ELEMENT_VALUE -100
#define MAX_MATRIX_SIZE 4096 //num righe
#define MY_EPSILON 1e-5

//matrix file descriptor
struct mfile_desc{
	char mat_type;
	int rows;
	int columns;	
};
//~ written in the beginning of a file containing a matrix
//~ legal values for mat_type are:
//~ s: square matrix;
//~ r: rectangular matrix;
//~ l: lower triangular matrix;
//~ u: upper triangular matrix;


//local libs

//utils.c
void print_help();
void do_some_stuff();
int give_a_number();
void gimme_a_value(int *);
float bounded_rand(int, int);
int* int_vector_alloc(int);void int_vector_dealloc(int *);
void int_vector_visualization(int *, int);

//matrix-utils.c
float** matrix_alloc(int);
void matrix_dealloc(float **, int);
float** matrix_generation(int);
void matrix_visualization(float **, int);
void matrix_modification(float **, int);
float** matrix_transposition(float **, int);
float** naive_matrix_multiplication(float **, float **, int);
float** matrix_compare(float **, float **, int);
int matrix_count_different_elements(float **, float **, int);
int matrix_count_different_elements_with_epsilon(float **, float **, int);
int matrix_count_different_elements_with_my_epsilon(float **, float **, int, float);
float** matrix_dummy_fill(int);
float** matrix_difference(float **, float **, int);
float** matrix_compare_with_epsilon(float **, float **, int);

//matrix-io.c
void matrix_disk_generation();
float** matrix_read(char *, int *);
void matrix_write(float **, char *, int);
float** matrix_random_write(char *, int);

//lup-ser.c
float** lu_decomposition(float **, int);
void lu_separation(float **, float **, float **, int );
float** lu_decomposition_inplace(float **, int);
float* vector_alloc(int);
void vector_dealloc(float *);
int lup_decomposition(float **, float **, float *, int);
void vector_visualization(float *, int);
void permutation_visualization(float *, int);
float* permutation_alloc(int);
void vector_shuffle(float *, int);
float** matrix_permutation(float **, float *, int);
