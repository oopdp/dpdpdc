/*
 *      utils.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "lup.h"

void print_help(){
	printf("******************\n");
	printf("*                *\n");
	printf("* LUP-Decomposer *\n");
	printf("*                *\n");
	printf("******************\n\n");	
	
	printf("usage:\n");
	printf("\tlup w - write to disk 5 square matrix of different sizes! ~85MB required!\n");
	printf("\tlup g N - generate a square N matrix!\n");
	printf("\tlup t - for development purpose\n");
	printf("\th - this help\n");
	
	printf("\n");
	printf("you can run anyways for testing purpose:\n");
	printf("\tlup n\n");
	printf("with n = 1...3\n");
}

void do_some_stuff(){
	printf("Do some stuff..\n");
}

int give_a_number(){
	return 	42;
}

void gimme_a_value(int *value){
	*value=50;
}
	
float bounded_rand(int min,int max){
	float random;
	int diff=max-min;
	random=rand();
	random=(random/RAND_MAX)*diff;
	random += min;
	return random;
}


//~ allocate memory for an n vector
int* int_vector_alloc(int N){
	//fare i check sulla reale allocazione!!!
	int *vector;
	vector=(int *)malloc(N*sizeof(int));
	return vector;
}

//~ deallocate memory of an n vector
void int_vector_dealloc(int *vector){
	free(vector);
}

//~ print on stdout a vector of size n
void int_vector_visualization(int *vector, int N){
	int i=0;
	for(; i<N; i++){
		printf("%d -> %d\n", i, vector[i]);
	}
}
