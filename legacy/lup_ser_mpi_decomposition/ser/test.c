/*
 *      test.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


#include "lup.h"

void test_lu(int N){
	float **mat_a, **simple_prod, **e_M, **mat_lu, **mat_u, **mat_l;
	int differences;
	mat_a=matrix_generation(N);	
	//~ mat_a = matrix_alloc(N);

	//~ testing with a known matrix
	//~ cormen example
	//~ mat_a[0][0] = 2;
	//~ mat_a[0][1] = 3;
	//~ mat_a[0][2] = 1;
	//~ mat_a[0][3] = 5;
	//~ mat_a[1][0] = 6;
	//~ mat_a[1][1] = 13;
	//~ mat_a[1][2] = 5;
	//~ mat_a[1][3] = 19;
	//~ mat_a[2][0] = 2;
	//~ mat_a[2][1] = 19;
	//~ mat_a[2][2] = 10;
	//~ mat_a[2][3] = 23;
	//~ mat_a[3][0] = 4;
	//~ mat_a[3][1] = 10;
	//~ mat_a[3][2] = 11;
	//~ mat_a[3][3] = 31;
	//~ 
	printf("Matrice generata di taglia %d\n", N);
	matrix_visualization(mat_a,N);
	printf("Procedo con la decomposizione in-palce..\n");
	mat_lu = lu_decomposition(mat_a, N);
	printf("Matrice decomposta!\n");
	//~ matrix_visualization(mat_lu, N);

	mat_u = matrix_alloc(N);
	mat_l = matrix_alloc(N);

	lu_separation(mat_lu, mat_l, mat_u, N);
	printf("Generate matrici L e U\n");
	//~ printf("Matrice L\n");
	//~ matrix_visualization(mat_l, N);
	//~ printf("Matrice U\n");
	//~ matrix_visualization(mat_u, N);

	printf("Esecuziuone prodotto LU\n");
	simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
	//~ printf("Prodotto LU\n");
	//~ matrix_visualization(simple_prod, N);


	e_M=matrix_compare_with_epsilon(mat_a, simple_prod, N);
	printf("Matrice errori:\n");
	matrix_visualization(e_M, N);

	e_M=matrix_difference(mat_a, simple_prod, N);
	printf("Matrice differenze:\n");
	matrix_visualization(e_M, N);

	printf("Risultati confronto A=LU  con A di taglia %d\n",N);
	differences=matrix_count_different_elements_with_epsilon(mat_a, simple_prod, N);
	printf("Rilevati %d elementi diversi!\n", differences);
	if(differences > 0) printf("XXXX\n");
	printf("\n");


	printf("Precisone: %f\n", MY_EPSILON);

	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	matrix_dealloc(mat_l, N);
	matrix_dealloc(mat_u, N);
	matrix_dealloc(simple_prod, N);	
}


void test_lu_s(int N){
	float **mat_a, **simple_prod, **mat_lu, **mat_u, **mat_l;
	int differences;
	mat_a=matrix_generation(N);	
	mat_lu = lu_decomposition(mat_a, N);
	mat_u = matrix_alloc(N);
	mat_l = matrix_alloc(N);
	lu_separation(mat_lu, mat_l, mat_u, N);
	simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
	differences=matrix_count_different_elements_with_epsilon(mat_a, simple_prod, N);
	//~ printf("Rilevati %d elementi diversi con taglia %d\n", differences, N);
	printf("%d\n", differences);
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	matrix_dealloc(mat_l, N);
	matrix_dealloc(mat_u, N);
	matrix_dealloc(simple_prod, N);	
}

void test_lu_s_epsilon(int N, float epsilon){
	float **mat_a, **simple_prod, **mat_lu, **mat_u, **mat_l;
	int differences;
	mat_a=matrix_generation(N);	
	mat_lu = lu_decomposition(mat_a, N);
	mat_u = matrix_alloc(N);
	mat_l = matrix_alloc(N);
	lu_separation(mat_lu, mat_l, mat_u, N);
	simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
	differences=matrix_count_different_elements_with_my_epsilon(mat_a, simple_prod, N, epsilon);
	//~ printf("Rilevati %d elementi diversi con taglia %d\n", differences, N);
	printf("%d\n", differences);
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	matrix_dealloc(mat_l, N);
	matrix_dealloc(mat_u, N);
	matrix_dealloc(simple_prod, N);	
}


void test_lup_s(int N){
	float **mat_a, **mat_a_perm, **simple_prod, **mat_lu, **mat_u, **mat_l;
	float *perm;
	int singular, differences;
	mat_a=matrix_generation(N);	
	mat_lu = matrix_alloc(N);
	perm = permutation_alloc(N);
	singular=lup_decomposition(mat_a, mat_lu, perm, N);
	if(singular>0){
		printf("-1\n");
	}
	else{
		mat_u = matrix_alloc(N);
		mat_l = matrix_alloc(N);
		lu_separation(mat_lu, mat_l, mat_u, N);
		simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
		mat_a_perm=matrix_permutation(mat_a, perm, N);
		differences=matrix_count_different_elements_with_epsilon(mat_a_perm, simple_prod, N);
		printf("%d\n", differences); 
		matrix_dealloc(mat_l, N);
		matrix_dealloc(mat_u, N);
		matrix_dealloc(mat_a_perm, N);
	}
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	vector_dealloc(perm);
}

void test_lup_s_epsilon(int N, float epsilon){
	float **mat_a, **mat_a_perm, **simple_prod, **mat_lu, **mat_u, **mat_l;
	float *perm;
	int singular, differences;
	mat_a=matrix_generation(N);	
	mat_lu = matrix_alloc(N);
	perm = permutation_alloc(N);
	singular=lup_decomposition(mat_a, mat_lu, perm, N);
	if(singular>0){
		printf("-1\n");
	}
	else{
		mat_u = matrix_alloc(N);
		mat_l = matrix_alloc(N);
		lu_separation(mat_lu, mat_l, mat_u, N);
		simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
		mat_a_perm=matrix_permutation(mat_a, perm, N);
		//~ differences=matrix_count_different_elements_with_epsilon(mat_a_perm, simple_prod, N);
		differences=matrix_count_different_elements_with_my_epsilon(mat_a_perm, simple_prod, N, epsilon);
		printf("%d\n", differences); 
		matrix_dealloc(mat_l, N);
		matrix_dealloc(mat_u, N);
		matrix_dealloc(mat_a_perm, N);
	}
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	vector_dealloc(perm);
}

void test_lu_time(int N){
	float **mat_a, **mat_lu;
	mat_a=matrix_generation(N);	
	clock_t start, end;
	double cpu_time_used;
    start = clock();
	mat_lu = lu_decomposition(mat_a, N);
	end = clock();
	cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
	printf("%.10f\n", cpu_time_used);
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
}


void test_read_write(int test_size){
	char * filename = "test.dat";
	int discovered_size, differences;
	float **r_M, **w_M;
	
	//~ scrittura di matrice random su file
	w_M=matrix_random_write(filename,test_size);
	printf("Matrice random scritta in %s\n", filename);
	//~ matrix_visualization(w_M, test_size);
	
	//~ lettura della matrice precedente dal file salvato
	r_M=matrix_read(filename, &discovered_size);
	printf("Matrice letta da %s\n", filename);
	//~ matrix_visualization(r_M, discovered_size);
					
	//~ confronto!
	printf("Risultati per scrittura-lettura matrice di taglia %d\n",test_size);
	if(test_size == discovered_size){
		printf("Taglie concordi!\n");
		differences=matrix_count_different_elements(w_M, r_M, test_size);
		printf("Rilevati %d elementi diversi!\n", differences);
		if(differences > 0) printf("XXXX\n");
	}
	else printf("XXX Taglie discordi\n");
	printf("\n");
	
	//~ liberazione mem
	matrix_dealloc(w_M,test_size);
	matrix_dealloc(r_M,test_size);
	
}

void test_lup(int N){
	float **mat_a, **mat_a_perm, **simple_prod, **e_M, **mat_lu, **mat_u, **mat_l;
	float *perm;
	int singular, differences;
	
	
	mat_a=matrix_generation(N);	
	
	mat_lu = matrix_alloc(N);
	perm = permutation_alloc(N);
	
	
	//~ testing with a known matrix
	//~ cormen example
	//~ mat_a = matrix_alloc(N);
	//~ mat_a[0][0] = 2;
	//~ mat_a[0][1] = 0;
	//~ mat_a[0][2] = 2;
	//~ mat_a[0][3] = 0.6;
	//~ mat_a[1][0] = 3;
	//~ mat_a[1][1] = 3;
	//~ mat_a[1][2] = 4;
	//~ mat_a[1][3] = -2;
	//~ mat_a[2][0] = 5;
	//~ mat_a[2][1] = 5;
	//~ mat_a[2][2] = 4;
	//~ mat_a[2][3] = 2;
	//~ mat_a[3][0] = -1;
	//~ mat_a[3][1] = -2;
	//~ mat_a[3][2] = 3.4;
	//~ mat_a[3][3] = -1;

	printf("Matrice A generata di taglia %d\n", N);
	matrix_visualization(mat_a,N);
	
	printf("Procedo con la decomposizione di A..\n");
	singular=lup_decomposition(mat_a, mat_lu, perm, N);
	if(singular>0){
		printf("Matrice singolare! Impossibile trovare L, U, P\n");
		printf("Esco...\n");
		//~ matrix_visualization(mat_a, N);
		//~ matrix_visualization(mat_lu, N);
		
	}
	else{
		printf("Matrice decomposta!\n");
		matrix_visualization(mat_lu, N);

		mat_u = matrix_alloc(N);
		mat_l = matrix_alloc(N);
		//~ mat_a_perm = matrix_alloc(N);

		lu_separation(mat_lu, mat_l, mat_u, N);
		printf("Generate matrici L e U\n");
		printf("Matrice L\n");
		matrix_visualization(mat_l, N);
		printf("Matrice U\n");
		matrix_visualization(mat_u, N);
		printf("Matrice P\n");
		permutation_visualization(perm ,N);
		
		printf("Permutazione di A secondo P\n");
		mat_a_perm=matrix_permutation(mat_a, perm, N);
		matrix_visualization(mat_a_perm, N);
		
		printf("Esecuzione prodotto LU....\n");
		simple_prod = naive_matrix_multiplication(mat_l, mat_u, N);
		printf("Prodotto LU\n");
		matrix_visualization(simple_prod, N);


		e_M=matrix_compare_with_epsilon(mat_a_perm, simple_prod, N);
		printf("Matrice errori:\n");
		matrix_visualization(e_M, N);

		e_M=matrix_difference(mat_a_perm, simple_prod, N);
		printf("Matrice differenze:\n");
		matrix_visualization(e_M, N);

		printf("Risultati confronto PA=LU  con A di taglia %d\n",N);
		differences=matrix_count_different_elements_with_epsilon(mat_a_perm, simple_prod, N);
		printf("Rilevati %d elementi diversi!\n", differences);
		if(differences > 0) printf("XXXX\n");
		printf("Precisone: %f\n", MY_EPSILON);
		printf("\n");
		//~ 
		matrix_dealloc(mat_l, N);
		matrix_dealloc(mat_u, N);
		matrix_dealloc(mat_a_perm, N);
		matrix_dealloc(simple_prod, N);
	}
	
	matrix_dealloc(mat_a, N);
	matrix_dealloc(mat_lu, N);
	vector_dealloc(perm);


}

void test_vector(int N){
	float *test;
	test = permutation_alloc(N);
	printf("Vettore di permutazione di taglia %d\n", N);
	vector_visualization(test, N);
	printf("Vettore di permutazione di taglia %d layout matriciale\n", N);
	permutation_visualization(test, N);
	printf("Permutazione in corso...\n\n");
	
	vector_shuffle(test, N);
	printf("Generata una permutazione\n");
	printf("Vettore di permutazione di taglia %d\n", N);
	vector_visualization(test, N);
	printf("Vettore di permutazione di taglia %d layout matriciale\n", N);
	permutation_visualization(test, N);
	vector_dealloc(test);
}

void test_read(){
	
}

void test_write(){
	
}

void test_random(){
	
}	

