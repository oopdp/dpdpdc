/*
 *      lup-ser.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "lup.h"

//return a matrix containing both l and u!
//that is lu decomposition of matrix
float** lu_decomposition(float **matrix, int N){	
	int i, j;
	float **mat_lu;
	
	mat_lu = matrix_alloc(N);
	//~ copia di matrix su mat_lu
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			mat_lu[i][j] = matrix[i][j];
		}
	}	
	mat_lu = lu_decomposition_inplace(mat_lu, N);
	return mat_lu;
}

//work inplace on matrix producing a matrix containining both l and u for matrix mat_lu
float** lu_decomposition_inplace(float **mat_lu, int N){
    int i, j, k;
	float temp;
	for(k=0; k<N; k++){
		for(i=k+1; i<N; i++){
			temp = mat_lu[i][k] / mat_lu[k][k];
			mat_lu[i][k] = temp;
			mat_lu[k][i] = mat_lu[k][i];
		}
		for(i=k+1; i<N; i++){
			for(j=k+1; j<N; j++){
				mat_lu[i][j] = mat_lu[i][j] - mat_lu[i][k]*mat_lu[k][j];
			}	
		}
	}
	return mat_lu;
}

//~ given a matrix containing l and u, produces matrix l and u separately
void lu_separation(float **matrix, float **l, float **u, int N){
	int i, j;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(i>j) l[i][j] = matrix[i][j];
			else u[i][j] = matrix[i][j];
			if(i==j) l[i][j] = 1; //diagonale unitaria di l
		}
	}
}

//~ allocate memory for an n vector
float* vector_alloc(int N){
	//fare i check sulla reale allocazione!!!
	float *vector;
	vector=(float *)malloc(N*sizeof(float));
	return vector;
}

//~ permutation vector allocation and initialization
float* permutation_alloc(int N){
	//fare i check sulla reale allocazione!!!
	float *vector;
	int i=0;
	vector=(float *)malloc(N*sizeof(float));
	for(; i<N; i++){
		vector[i]=i;
	}
	return vector;
}

//~ deallocate memory of an n vector
void vector_dealloc(float *vector){
	free(vector);
}

//~ print on stdout a vector of size n
void vector_visualization(float *vector, int N){
	int i=0;
	for(; i<N; i++){
		printf("%d -> %5.2f\n", i, vector[i]+1);
	}
}

//~ print permutation vector like a matrix
void permutation_visualization(float *vector, int N){
	int i,j;
	for(i=0; i<N; i++){
		printf("|");
		for(j=0; j<N; j++){
			if((int)vector[i]==j) printf("%d ", 1);
			else printf("%d ", 0);
		}
		printf("|\n");
	}
}

//~ shuffle a vector
void vector_shuffle(float *vector, int N){
	float temp;
	int i;
	int new_n = ceil(N/2);
	for(i=0; i< new_n; i++){
		temp=vector[i];
		vector[i]=vector[i+new_n];
		vector[i+new_n]=temp;
	}
}

//~ perform row-permutations on matrix according to permutation
float** matrix_permutation(float **matrix, float *permutation, int N){
	int i,j;
	float **matrix_p;
	matrix_p=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix_p[i][j]=matrix[(int)permutation[i]][j];
		}	
	}
	return matrix_p;
}


//~ perform lup decomposition
int lup_decomposition(float **matrix, float **lu, float *perm, int N){
	int i,j,k, k_i, temp;
	float pivot, abs, *e_row;

	//~ copia di matrix su lu
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			lu[i][j] = matrix[i][j];
		}
	}
	for(k=0; k<N; k++){
		pivot=0;
		for(i=k; i<N; i++){
			abs=fabs(lu[i][k]);
			if(abs>pivot){
				pivot=abs;
				k_i=i;
			}
			
		}
		if(pivot==0){
			return 1; //esco, matrice singolare!		
		}
		
		//~ scambio elementi nel vettore di permutazione
		temp=perm[k];
		perm[k]=perm[k_i];
		perm[k_i]=temp;
		
		//~ W IL C!
		//~ scambio le righe della matrice
		e_row=lu[k];
		lu[k]=lu[k_i];
		lu[k_i]=e_row;
				
		for(i=k+1; i<N; i++){
			lu[i][k] = lu[i][k] / lu[k][k];
			for(j=k+1; j<N; j++){
				lu[i][j] = lu[i][j] -lu[i][k]*lu[k][j];
			}		
		}
	}
	return 0;
}
