/*
 *      matrix-io.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "lup.h"

//~ read a matrix from filename
float** matrix_read(char *filename, int *discovered_size){
	struct mfile_desc *desc;
	float** matrix;
	int m_size,i;
	
	desc = (struct mfile_desc *)malloc(sizeof(struct mfile_desc));
	FILE *fin = fopen(filename, "r");
	fread(desc, sizeof(struct mfile_desc), 1, fin);
	m_size=desc->rows;
	*discovered_size = m_size;
	matrix = matrix_alloc(m_size);
	for(i=0; i<m_size; i++){
		fread(matrix[i], sizeof(float), m_size, fin);
	}
	fclose(fin);
	return matrix;
}

//~ write N square matrix onto filename
void matrix_write(float **matrix, char *filename, int N){
	int i;
	struct mfile_desc *desc;
	desc = (struct mfile_desc *)malloc(sizeof(struct mfile_desc));
	desc->mat_type = 's';
	desc->rows = N;
	desc->columns = N;
	FILE *fout=fopen(filename, "w");
	fwrite(desc, sizeof(struct mfile_desc), 1, fout);
	for(i=0; i<N; i++){
		fwrite(matrix[i], sizeof(float), N, fout);
	}
	fclose(fout);
	free(desc);
}

//~ write a random N square matrix onto filename
float** matrix_random_write(char * filename, int N){
	float **matrix;
	matrix=matrix_generation(N);
	matrix_write(matrix,filename,N);
	return matrix;
}

//~ useful for creating some square matrixes of various sizes
void matrix_disk_generation(){
	int i;
	float **matrix;
	char *filenames[]={"gen-mat-0004.dat", "gen-mat-0016.dat", "gen-mat-0128.dat", "gen-mat-0512.dat", "gen-mat-2048.dat", "gen-mat-4096.dat"};
	int sizes_of_matrix[]={4, 16, 128, 512, 2048, 4096};
	for(i=0; i<6; i++){
		matrix = matrix_random_write(filenames[i], sizes_of_matrix[i]);
		matrix_dealloc(matrix, sizes_of_matrix[i]);	
	}
}

//~ 
//~ void matrix_twrite(float **matrix, int N){
	//~ 
//~ }

//~ 
//~ float** matrix_tread(){
	//~ float** matrix;
	//~ return matrix;
//~ }
//~ 
