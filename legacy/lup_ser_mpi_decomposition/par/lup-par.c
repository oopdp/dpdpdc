/*
 *      lup-ser.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "lup.h"

float** work_matrix_alloc(int I, int N){
	//fare i check sulla reale allocazione!!!
	float **matrix;
	int i=0;
	matrix=(float **)malloc(I*sizeof(float*)); //numero di righe!
	for(; i<I; i++){
		matrix[i]=(float *)malloc(N*sizeof(float)); //lunghezza colonne
	}
	return matrix;
}

//~ deallocate memory for a I*xx matrix
void work_matrix_dealloc(float **matrix, int I){
	int i;
	for(i=0; i<I; i++){
		free(matrix[i]);
	}
	free(matrix);
}

void copy_matrix_portion(float **matrix, float **portion, int I, int N){
	//~ printf("Devo copiare da questa\n");
	//~ matrix_visualization(matrix,N);
	//fare i check sulla reale allocazione!!!
	int i,j;
	for(i=0; i<I; i++){
		for(j=0; j<N; j++){
			//~ printf("matrix[i][j]=%f\n", matrix[i][j]);
			portion[i][j]=matrix[i][j];
		}	
	}
}

void work_matrix_visualization(float **matrix, int I, int N){
	int i,j;
	for(i=0; i<I; i++){
		printf("|");
		for(j=0; j<N; j++){
			printf("%+8.2f ", matrix[i][j]);
		}
		printf("|\n");
	}
}


void work_matrix_visualization_check(float **matrix, int I, int N, int proci, int rank){
	int i,j,k;
	for(k=0; k< proci; k++){
		if(rank==k){
			MPI_Barrier(MPI_COMM_WORLD);
			printf("Sono il processore %d e posseggo:\n",rank);
			for(i=0; i<I; i++){
				printf("|");
				for(j=0; j<N; j++){
					printf("%+8.2f ", matrix[i][j]);
				}
				printf("|\n");
			}
		}
	}
}

float matrix_search_max(float **matrix, int size, int row, int col, int *max_index){
	float max;
	int i;
	*max_index=row;

	
	//~ max=fabs(matrix[row][col]);
	max=matrix[row][col];

	for(i=row;i<size;i++){
		if(fabs(matrix[i][col])>fabs(max)){
			//~ max=fabs(matrix[i][col]);
			max=matrix[i][col];
			*max_index=i;
		}
	}
	return max;
}

void who(int my_rank){
	MPI_Barrier(MPI_COMM_WORLD);
	printf("Sono il processore p%d ", my_rank);
}

void bar(){
	MPI_Barrier(MPI_COMM_WORLD);
}


void max_vector_visualization(float *vector, int N){
	int i=0;
	for(; i<N; i++){
		printf("%d -> %5.2f\n", i, vector[i]);
	}
}

void int_max_vector_visualization(int *vector, int N){
	int i=0;
	for(; i<N; i++){
		printf("%d -> %d\n", i, vector[i]);
	}
}

float find_max_vector(float *vector, int *index, int N){
	int i;
	float max;
	
	//~ printf("Sono in find_max_vector\n");
	//~ printf("Devo cercare il max qua:\n");
	//~ vector_visualization(vector, N);

	
	*index=0;
	max=0;
	for(i=0; i<N; i++){
		if(fabs(vector[i])>fabs(max)){
			max=vector[i];
			//~ printf("Il max locale vale %f\n", vector[i]);
			*index=i;
		}		
	//~ printf("%d -> %d\n", i, vector[i]);
	}
	//~ printf("\n");
	//printf("Il MAX vale %f\n", max);
	return max;
}

int* int_permutation_alloc(int N){
	//fare i check sulla reale allocazione!!!
	int *vector;
	int i=0;
	vector=(int *)malloc(N*sizeof(int));
	for(; i<N; i++){
		vector[i]=i;
	}
	return vector;
}


int lup_dec_base(float **lu, int *perm, int N){
	int i,j,k, k_i, temp;
	float pivot, abs, *e_row;
	
	for(k=0; k<N; k++){
		pivot=0;
		for(i=k; i<N; i++){
			abs=fabs(lu[i][k]);
			if(abs>pivot){
				pivot=abs;
				k_i=i;
			}
			
		}
		if(pivot==0){
			return 1; //esco, matrice singolare!		
		}
		
		//~ scambio elementi nel vettore di permutazione
		temp=perm[k];
		perm[k]=perm[k_i];
		perm[k_i]=temp;
		
		//~ W IL C!
		//~ scambio le righe della matrice
		e_row=lu[k];
		lu[k]=lu[k_i];
		lu[k_i]=e_row;
				
		for(i=k+1; i<N; i++){
			lu[i][k] = lu[i][k] / lu[k][k];
			for(j=k+1; j<N; j++){
				lu[i][j] = lu[i][j] -lu[i][k]*lu[k][j];
			}		
		}
	}
	return 0;
}

void lup_par_matrix_permutation(float **matrix, int *permutation, int N){
	int i,j;
	float **matrix_p;
	matrix_p=matrix_alloc(N);
	//~ int_vector_visualization(permutation, N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix_p[i][j]=matrix[permutation[i]][j];
		}
	}
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix[i][j]=matrix_p[i][j];
		}
	}
	matrix_dealloc(matrix_p, N);
}



void int_permutation_visualization(int *vector, int N){
	int i,j;
	for(i=0; i<N; i++){
		printf("|");
		for(j=0; j<N; j++){
			if(vector[i]==j) printf("%d ", 1);
			else printf("%d ", 0);
		}
		printf("|\n");
	}
}
