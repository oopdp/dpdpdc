/*
 *      matrix-utils.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */

#include "lup.h"


//~ allocate memory for a N*N square matrix
float** matrix_alloc(int N){
	//fare i check sulla reale allocazione!!!
	float **matrix;
	int i=0;
	matrix=(float **)malloc(N*sizeof(float*));
	for(; i<N; i++){
		matrix[i]=(float *)malloc(N*sizeof(float));
	}
	return matrix;
}

//~ deallocate memory for a N*N square matrix
void matrix_dealloc(float **matrix, int N){
	int i;
	for(i=0; i<N; i++){
		free(matrix[i]);
	}
	free(matrix);
}

//~ generate a random square matrix of size N*N
float** matrix_generation(int N){
	int i,j;
	float **matrix=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix[i][j]=bounded_rand(MIN_ELEMENT_VALUE, MAX_ELEMENT_VALUE);
			//~ matrix[i][j]=rand()%100; //simple generation!
		}
	}
	return matrix;
}

//~ fill a matrix with row-major ascending integers
//~ 1 2 3
//~ 4 5 6
//~ 7 8 9
float** matrix_dummy_fill(int N){
	int i,j;
	float **matrix=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix[i][j]= (i*N)+(j+1);
		}
	}
	return matrix;
}

//~ print on stdout a square matrix of size N
void matrix_visualization(float **matrix, int N){
	int i,j;
	for(i=0; i<N; i++){
		printf("|");
		for(j=0; j<N; j++){
			printf("%+8.2f ", matrix[i][j]);
		}
		printf("|\n");
	}
}

//~ transpose a N square matrix
float** matrix_transposition(float **matrix, int N){
	int i,j;
	float **matrix_t;
	matrix_t=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix_t[j][i]=matrix[i][j];
		}
	}
	return matrix_t;
}

//~ compare two N square matrix, return a matrix E with e[i][j] = 1 if
//~ a[i][j] != b[i][j]
float** matrix_compare(float **matrix_a, float **matrix_b, int N){
	int i,j;
	float **matrix_e;
	matrix_e=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(matrix_a[i][j]==matrix_b[i][j]) matrix_e[i][j]=0;
			else matrix_e[i][j]=1;
		}
	}
	return matrix_e;
}

float** matrix_compare_with_epsilon(float **matrix_a, float **matrix_b, int N){
	int i,j;
	float **matrix_e;
	matrix_e=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(fabs(matrix_a[i][j]-matrix_b[i][j]) > MY_EPSILON) matrix_e[i][j]=1;
			else matrix_e[i][j]=0;
		}
	}
	return matrix_e;
}

//return R = matrix_a - matrix_b
float** matrix_difference(float **matrix_a, float **matrix_b, int N){
	int i,j;
	float **matrix_e;
	matrix_e=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			matrix_e[i][j]=matrix_a[i][j]-matrix_b[i][j];
		}
	}
	return matrix_e;
}

//~ compare two N square matrix, and return the number of different elements
int matrix_count_different_elements(float **matrix_a, float **matrix_b, int N){
	int i,j;
	int differences=0;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(matrix_a[i][j]!=matrix_b[i][j]) differences++;
		}
	}
	return differences;
}

int matrix_count_different_elements_with_epsilon(float **matrix_a, float **matrix_b, int N){
	int i,j;
	int differences=0;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(fabs(matrix_a[i][j]-matrix_b[i][j]) > MY_EPSILON) differences++;
		}
	}
	return differences;
}

int matrix_count_different_elements_with_my_epsilon(float **matrix_a, float **matrix_b, int N, float my_epsilon){
	int i,j;
	int differences=0;
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			if(fabs(matrix_a[i][j]-matrix_b[i][j]) > my_epsilon) differences++;
		}
	}
	return differences;
}


//~ naive matrix multiplication
float** naive_matrix_multiplication(float **mat_a, float **mat_b, int N){
	float **mat_c;
	int i,j,k;
	
	mat_c=matrix_alloc(N);
	for(i=0; i<N; i++){
		for(j=0; j<N; j++){
			mat_c[i][j]=0;
			for(k=0; k<N; k++){
				mat_c[i][j] += mat_a[i][k] * mat_b[k][j];
			}
		}
	}
	return mat_c;
}

//~ matrix access testing function
void matrix_modification(float **matrix, int N){
	int i;
	for(i=0; i<N; i++) matrix[i][i]=1;	
}
