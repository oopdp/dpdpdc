/*
 *      s_main.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


#include "lup.h"
#include "test.h"
#include "test_par.h"


int main(int argc, char** argv){
	
	char c;
	int N;

	//srand(time(NULL));
	if(argc<2){
		print_help();
		return 1;
	}
	else{
		
		c=argv[1][0];
		
		switch(c){
			case 'w':
				matrix_disk_generation();
				return 0;
			break;
			case 't':
				N=atoi(argv[2]);
				test_par_lup(N, argc, argv);
				return 0;
			break;
			case 's':
				N=atoi(argv[2]);
				test_lup(N);
				return 0;
			break;
			default:
				print_help();
				return 0;
			break;
		}
	}

	return 0;
	
}
