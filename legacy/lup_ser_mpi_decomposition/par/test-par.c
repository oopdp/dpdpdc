/*
 *      test_par.c
 *      
 *      Copyright 2009 Salvatore Brundo <salvo85@gmail.com>
 *      
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *      
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *      
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */


#include "lup.h"

void test_par_lup(int N, int argc, char **argv){
	float **mat_a, **mat_lu, **mat_work, **mat_b_copy, **mat_work_to_send, \
	 **b_dx, **b_dec, **pivot_vector, **pivot_temp,  \
	 **mat_l, **mat_u, **mat_lu_prod;
	float *max_vector, *temp_perm;
	int *max_vector_position, *perm, *b_dx_perm;
	int I, my_rank, t_proci, pivot_row, perm_temp, pivot_processor, pivot_relative_position;
	const int p_0=0;
	MPI_Comm comm;
	MPI_Status status;
	int i,j,k,master,t,s;
	

	float max_rel, max_abs;
	int index, max_abs_index;
	
	double ti,tf,t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12;
	double timetot, timecomp;
	timetot=0;
	timecomp=0;
	
	ti=0;
	tf=0;
	t0=0;
	t1=0;
	t2=0;
	t3=0;
	t4=0;
	t5=0;
	t6=0;	
	t7=0;
	t8=0;
	t9=0;
	t10=0;
	t11=0;
	
	MPI_Init(&argc, &argv);	
	comm = MPI_COMM_WORLD;
	
	MPI_Comm_rank(comm, &my_rank);
	MPI_Comm_size(comm, &t_proci);
	
	//printf("Decomposizione LUP parallela:\n");
	if(t_proci==1){
		
		mat_a=matrix_generation(N);
		perm = int_permutation_alloc(N);
		ti=MPI_Wtime();
		ti=MPI_Wtime();
		lup_dec_base(mat_a, perm, N);
		tf=MPI_Wtime();
		matrix_dealloc(mat_a, N);
		int_vector_dealloc(perm);
		timetot=tf-ti;
		timecomp=0;
		printf("----tt_max----%f\n", timetot);
		printf("----te_max----%f\n", timetot);
		
	}
	else{
		if((N%t_proci)==0){
			I=N/t_proci;
			//~ INIZION LAVORI
			//~ sono p_0
			
			//~ il numero di righe della matrice di lavoro �:
			
			if(my_rank == p_0){
				
				mat_a=matrix_generation(N);
				mat_lu = matrix_alloc(N);
				mat_l = matrix_alloc(N);
				mat_u = matrix_alloc(N);
				mat_lu_prod = matrix_alloc(N);
				perm = int_permutation_alloc(N);
				max_vector = vector_alloc(t_proci);
				max_vector_position = int_vector_alloc(t_proci);

					//~ cormen example
					//~ mat_a[0][0] = 2;
					//~ mat_a[0][1] = 0;
					//~ mat_a[0][2] = 2;
					//~ mat_a[0][3] = 0.6;
					//~ mat_a[1][0] = 3;
					//~ mat_a[1][1] = 3;
					//~ mat_a[1][2] = 4;
					//~ mat_a[1][3] = -2;
					//~ mat_a[2][0] = 5;
					//~ mat_a[2][1] = 5;
					//~ mat_a[2][2] = 4;
					//~ mat_a[2][3] = 2;
					//~ mat_a[3][0] = -1;
					//~ mat_a[3][1] = -2;
					//~ mat_a[3][2] = 3.4;
					//~ mat_a[3][3] = -1;
				//~ int_max_vector_visualization(max_vector_position, t_proci);
				//~ printf("Generata matrice di taglia %d\n", N);
				//~ matrix_visualization(mat_a, N);	
				//~ printf("\n\n");
				
			}
			
			//ciascun processore alloca memoria per contenere la matrice "ciclica" rettangolare...
			
			mat_work=work_matrix_alloc(I,N);
			mat_work_to_send=work_matrix_alloc(I,N);//temporaneo per l'invio il risultato va in quella sopra....
			pivot_vector = work_matrix_alloc(1,N);
			pivot_temp = work_matrix_alloc(1,N);
			//~ matrix_visualization(mat_a, N);	
			//~ Adesso il processore p_0 manda le I righe a ciascun processore!
			//~ e ciscun processore le diverso da p_0 le riceve
			ti=MPI_Wtime();
			if(my_rank == p_0){
				//~ printf("Ongi processore alloca una matrice di lavoro di taglia %dx%d\n",I,N);
				
				//~ copy_matrix_portion(mat_a, mat_work_to_send, I, N);
				for(k=0; k<t_proci;k++){
					for(i=0; i<I; i++){
						for(j=0; j<N;j++){
							mat_work_to_send[i][j]=mat_a[i*t_proci+k][j];
						}	
					}
					if(k==0){
						//~ sono p_0 copio la mia parte...
						for(i=0; i<I; i++){
							for(j=0; j<N;j++){
								mat_work[i][j]=mat_work_to_send[i][j];
							}
						}
					}
					
					
					else{
						for(i=0; i<I; i++){
							MPI_Send(mat_work_to_send[i], N, MPI_FLOAT, k, 0, comm);
						}
					}
					
					
					//~ printf("La porzione di lavoro di p_%d\n",k);
					//~ work_matrix_visualization(mat_work_to_send,I,N);
					//~ 
				}
				//~ copy_matrix_portion(mat_a, mat_work_to_send, I, N);
				//~ dopodich� facciamo gli invii
			t9=MPI_Wtime();	
			}
			else{
				for(i=0; i<I; i++){
					MPI_Recv(mat_work[i], N, MPI_FLOAT, 0, 0, comm, &status);
				}
				//~ non sono p_0 devo ricevere
			}
			//~ work_matrix_visualization_check(mat_work, I, N, t_proci, my_rank);
			bar();
			
			i=0;
			for(k=0; k<N-t_proci; k++){
				if(k==(i*t_proci+my_rank)){
					//~ printf("Sono il procio P%d e sono master all'iterazione %d", my_rank, k);

					master=my_rank;
					//~ printf(", mi rimangono in coda %d righe\n", I-i);
				}
				master=k%t_proci;
				
				//~ ricerca del max relativo
				//~ printf("Ricerca del max..\n");
				//~ work_matrix_visualization(mat_work, I, N);
				//~ printf("Con taglia %d, a partire dalla riga %d e dalla colonna %d\n", I-i, i, k);
				t1=MPI_Wtime();
				max_rel=matrix_search_max(mat_work, I, i, k, &index); 
				t2=MPI_Wtime();
				//~ printf("Il massimo locale di p%d all'iterazione %d: %f\n", my_rank, k, max_rel);
				if(my_rank==p_0){
					max_vector[0]=max_rel;
					max_vector_position[0]=index;
					
					
					for(j=1; j<t_proci; j++){
						//tag 1 sono i max...
						MPI_Recv(&max_vector[j], 1, MPI_FLOAT, j, 1, comm, &status);
						//tag 2 sono le posizione dei max...
						MPI_Recv(&max_vector_position[j], 1, MPI_INT, j, 2, comm, &status);
					}
					t3=MPI_Wtime();
					max_abs=find_max_vector(max_vector, &max_abs_index, t_proci);
					t4=MPI_Wtime();
					//~ printf("Max assoluto %f contenuto nella riga %d di p_%d\n", max_abs, max_vector_position[max_abs_index], max_abs_index);
					pivot_row=t_proci*max_vector_position[max_abs_index]+max_abs_index;
					//~ printf("perci� il max assoluto era contenuto nella riga di %d A \n",pivot_row);
					//aggiorno il pivot
					perm_temp=perm[k];
					perm[k]=perm[pivot_row];
					perm[pivot_row]=perm_temp;
					//~ int_max_vector_visualization(perm, N);
					//~ printf("\n\n");
					

					
					
				}
				
				else{
					//~ who(my_rank);
					//~ printf("e invio il  max %.2f  trovato in poszione %d...\n", max_rel, index);
					//tag 1 sono i max....
					MPI_Send(&max_rel,1,MPI_FLOAT,p_0,1,comm);
					//tag 2 sono le posizione dei max...
					MPI_Send(&index,1,MPI_INT,p_0,2,comm);
				}
				
				//adesso comunico al processore possessore della riga col max di effettuare lo scambio con il master
				//comunico al master di fare lo scambio della riga col procio sopra
				bar();
				//~ se sono p_0 so chi � master e chi ha il max;
				if(my_rank==p_0){
					//~ quindi mando a tutti un messaggio per dire quale procio � il pivot e in quale delle sue righe � contenuto
					for(j=1; j<t_proci; j++){
						MPI_Send(&max_abs, 1, MPI_INT, j, 99, comm);
						MPI_Send(&max_abs_index, 1, MPI_INT, j, 100, comm);
						MPI_Send(&max_vector_position[max_abs_index], 1, MPI_INT, j, 101, comm);
						//~ printf("Sono p0, mandato messaggio a p_%d\n",j);
						
					}
					pivot_processor=max_abs_index;
					pivot_relative_position=max_vector_position[max_abs_index];
				}
				else{
					//tutti scoprono chi � il pivot!
					MPI_Recv(&max_abs, 1, MPI_INT, p_0, 99, comm, &status);
					MPI_Recv(&pivot_processor, 1, MPI_INT, p_0, 100, comm, &status);
					MPI_Recv(&pivot_relative_position, 1, MPI_INT, p_0, 101, comm, &status);

				}
				//~ printf("p%d: il pivot � %d\n", my_rank, pivot_processor);
				//~ printf("p%d: il master � %d\n", my_rank, master);
				
				if(my_rank!= pivot_processor){
					//~ MPI_Recv(mat_work[i], N, MPI_FLOAT, 0, 0, comm, &status);
					MPI_Recv(pivot_vector[0], N, MPI_FLOAT, pivot_processor, 110, comm, &status);
					
					//~ printf("Sono il procio p%d, ricevuta la riga pivot\n", my_rank);
					//~ work_matrix_visualization(pivot_vector, 1, N);
				}
				else{
					for(j=0; j<N; j++){
						pivot_vector[0][j]=mat_work[pivot_relative_position][j];
					}
					for(j=0;j<t_proci; j++){
						//~ printf("%d\n",pivot_relative_position);
						if(j!=my_rank) MPI_Send(pivot_vector[0], N, MPI_FLOAT, j, 110, comm);
					}
					//~ printf("Spedita a tutti la riga pivot\n");
					//~ work_matrix_visualization(pivot_vector, 1, N);				
				}
				
				if(my_rank==master){
					//~ devo mandare la mia prima riga al pivot	
					MPI_Send(mat_work[i], N, MPI_FLOAT, pivot_processor, 17, comm);
					//~ printf("Inviata\n");
					//~ work_matrix_visualization(mat_work, 1, N);
					for(t=0; t<N; t++){
						mat_work[i][t]=pivot_vector[0][t];
					}
				}
				if(my_rank==pivot_processor){
					MPI_Recv(pivot_temp[0], N, MPI_FLOAT, master, 17, comm, &status);
					//~ printf("Ricevuta\n");
					for(t=0; t<N; t++){
						mat_work[pivot_relative_position][t]=pivot_temp[0][t];
					}
					//~ work_matrix_visualization(pivot_vector, 1, N);
				}
				//~ printf("Righe scambiate\n");
				
				

				//printf("max_abs%f P%d\n", max_abs,my_rank);
				t5=MPI_Wtime();
				if(my_rank==master){
					//~ printf("max_abs%f\n", max_abs);
					int r,c;
					//~ printf("Il max assoluto all'iterzione %d vale %f\n\n", k, max_abs);
					for(r=i+1;r<I;r++){
						mat_work[r][k] = mat_work[r][k] / max_abs;
						for(c=k+1;c<N;c++){
							mat_work[r][c] = mat_work[r][c] - mat_work[r][k]*pivot_vector[0][c]; 
							
						}
					}
				}
				
				else {
					int r,c;
					//~ printf("Il max assoluto all'iterzione %d vale %f del procio p_%d\n", k, max_abs, my_rank);
					for(r=i;r<I;r++){
						mat_work[r][k] = mat_work[r][k] / max_abs;
						for(c=k+1;c<N;c++){
							mat_work[r][c] = mat_work[r][c] - mat_work[r][k]*pivot_vector[0][c]; 
						}
					}
				}
				t6=MPI_Wtime();
				//~ printf("Sono il procio P%d e ho scoperto che il master � %d\n", my_rank, master);
				bar();
				//~ max_rel=matrix_search_max(mat_work, I-k, k, k, &index);
				if(my_rank==master) i++;
				timecomp+=(t2-t1)+(t4-t3)+(t6-t5);
			}
			
			
			
			
			
			//~ si rimonta la matrice
			
			bar();
			if(my_rank != p_0){
				//~ TUTTI I PROCI MANDANO A P0 LA MATRICE DI WORK
				for(j=0; j<I;j++){
					MPI_Send(mat_work[j], N, MPI_FLOAT, p_0, 10+j, comm);
				}
			t0=MPI_Wtime();
			}
			else{
				for(j=1; j<t_proci; j++){
					for(t=0; t<I; t++){
						for(s=0; s<N; s++){
							mat_lu[t*t_proci][s]=mat_work[t][s];
						}
					}
					for(k=0; k<I; k++){
						MPI_Recv(mat_work_to_send[k], N, MPI_FLOAT, j, 10+k, comm, &status);
					}
					//~ printf("Ricevuta da p%d la matrice:\n", j);
					//~ work_matrix_visualization(mat_work_to_send, I, N);
					for(t=0; t<I; t++){
						for(s=0; s<N; s++){
							mat_lu[t*t_proci+j][s]=mat_work_to_send[t][s];
						}
					}				
				}
				

				//~ Inizio calcolo lup matrice pxp in basso a dx
			
				//~ printf("Matrice con angolo da calcolare in p_0!\n");
				//~ matrix_visualization(mat_lu, N);
				t7=MPI_Wtime();
				for(k=N-t_proci; k<N; k++){
					max_abs=0;
					for(i=k; i<N; i++){
						max_rel=fabs(mat_lu[i][k]);
						if(max_rel>max_abs){
							max_abs=max_rel;
							max_abs_index=i;
						}
						
					}
					if(max_abs==0){
						return; //esco, matrice singolare!		
					}
					
					//~ scambio elementi nel vettore di permutazione
					index=perm[k];
					perm[k]=perm[max_abs_index];
					perm[max_abs_index]=index;
					
					//~ W IL C!
					//~ scambio le righe della matrice
					temp_perm=mat_lu[k];
					mat_lu[k]=mat_lu[max_abs_index];
					mat_lu[max_abs_index]=temp_perm;
							
					for(i=k+1; i<N; i++){
						mat_lu[i][k] = mat_lu[i][k] / mat_lu[k][k];
						for(j=k+1; j<N; j++){
							mat_lu[i][j] = mat_lu[i][j] -mat_lu[i][k]*mat_lu[k][j];
						}		
					}
				}
				//~ printf("Matrice decomposta LU:\n");
				//~ matrix_visualization(mat_lu, N);
				tf=MPI_Wtime();
				//~ FINE LUP
				

				//~ TEST
				//~ printf("Matrice originale A:\n");
				//~ matrix_visualization(mat_a, N);
	//~ 
				//~ lu_separation(mat_lu, mat_l, mat_u, N);
	//~ 
				//~ printf("Matrice L:\n");
				//~ matrix_visualization(mat_l, N);		
	//~ 
				//~ printf("Matrice U:\n");
				//~ matrix_visualization(mat_u, N);
	//~ 
				//~ printf("Matrice P:\n");
				//~ int_permutation_visualization(perm, N);
	//~ 
				//~ printf("Prodotto LU:\n");
				//~ mat_lu_prod = naive_matrix_multiplication(mat_l, mat_u, N);
				//~ matrix_visualization(mat_lu_prod, N);
			//~ 
				//~ printf("Matrice A permutata:\n");
				//~ lup_par_matrix_permutation(mat_a, perm, N);
				//~ matrix_visualization(mat_a, N);
	//~ 
				//~ printf("Matrice A di taglia %d, rilevati %d errori di fattorizzazione\n", \
				N, matrix_count_different_elements_with_my_epsilon(mat_a, mat_lu_prod, N, 1e-3) \
				);
			}
			
			
			
			//~ Salvo su file
			
			
			//DEALLOCAZIONI
			work_matrix_dealloc(mat_work,I);		
			if(my_rank == p_0){
				matrix_dealloc(mat_a, N);
				matrix_dealloc(mat_lu, N);
				matrix_dealloc(mat_lu_prod, N);
				matrix_dealloc(mat_l, N);
				matrix_dealloc(mat_u, N);


				
				vector_dealloc(max_vector);
				work_matrix_dealloc(mat_work_to_send,I);	
				int_vector_dealloc(max_vector_position);
				int_vector_dealloc(perm);
			}
			
		}
		else{
			printf("Matrice non partizionabile..esco\n");
			//~ printf("Ciao, io sono il numero %d di %d e dovrei aspettare qua\n", my_rank, t_proci);
			MPI_Barrier(comm);
			MPI_Finalize( );
			return;
		}
		//~ printf("Ciao, io sono il numero %d di %d e dovrei aspettare qua\n", my_rank, t_proci);
		MPI_Barrier(comm);
		
				
		if(my_rank==p_0){
			timecomp+=(tf-t7)+(t9-ti);
			timetot=tf-ti;
			
			printf("----tt_max----%f\n", timetot);
			printf("----te_max----%f\n", timecomp);
			//~ printf("Tempo totale di esecuzione di P0 = %f \n",timetot);
			//~ printf("Tempo occupato dalle computazione per P%d = %f\n",my_rank ,timecomp);
		}
		
		if(my_rank==1){
			timetot=t0-ti;
			
			
			printf("----tt_med----%f\n", timetot);
			printf("----te_med----%f\n", timecomp);
			//~ printf("Tempo totale di esecuzione di P1 = %f \n",timetot);	
			//~ printf("Tempo occupato dalla computazione per P%d = %f\n",my_rank ,timecomp);
		}
		
		//~ printf("%f\n",timetot); //totale
		//~ printf("%f\n",timecom); //comunicazioni
	}
	MPI_Finalize( );
}
