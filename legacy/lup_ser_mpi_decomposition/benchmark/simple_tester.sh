#! /bin/sh

#pessimo ma pratico!
cp ../ser/lup ./lup_t

usage(){
	echo "Uso di lup-benchmarker:"
	echo ""
	echo "	Parametri possibili:"
	echo "		lu_er n : conteggia gli errori commessi da lu"
	echo "			per matrici di taglia da 1 a n e li salva in errori_lu.log"
	echo ""
	echo "		lup_er n : conteggia gli errori commessi da lup"
	echo "			per matrici di taglia da 1 a n e li salva in errori_lup.log"
	echo ""
	echo "		er n : conteggia gli errori commessi da lu e da lup"
	echo "			per matrici di taglia da 1 a n e li salva rispettivamente"
	echo "			in errori_lu.log errori_lup.log"
	echo ""
	echo "		ed n : conteggia gli errori commessi da lu e da lup con precisioni differenti"
	echo "			per matrici di taglia da 1 a n e li salva rispettivamente"
	echo "			in errori_lu-dist-{PRECISIONE}.log errori_lu-dist-{PRECISIONE}.log"
	echo "			in errori_lu-dist-{PRECISIONE}.log errori_lu-dist-{PRECISIONE}.log"
	echo "			con precisioni: 1e-5, 1e-4, 1e-3"
	echo ""	
	echo "		plot-er : mostra grafico di er (errori di lu e lup insieme)"
	echo ""
	echo "		plot-ed : mostra grafico di errori di lu e lup a precisioni differenti"
}

clean(){
	rm errori_*.log >/dev/null 2>/dev/null
}

clean_distrib(){
	rm errori_*dist*.log >/dev/null 2>/dev/null
}

lu(){
	for i in `seq 1 $1`; do
		./lup_t 1 $i >> errori_lu.log;
	done
}

lup(){
	for i in `seq 1 $1`; do
		./lup_t 2 $i >> errori_lup.log;
	done
}

errors_distribution(){
	for j in 1e-5 1e-4 1e-3; do
		for i in `seq 1 $1`; do
			./lup_t 3 $i $j >> errori_lup-dist-$j.log;
			./lup_t 4 $i $j >> errori_lu-dist-$j.log
		done;
	done
}

gnuplot_error(){
	echo '
		set title "# Errori all'aumentare della taglia dell'istanza"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Numero di elementi > Epsilon"
		set terminal png size 1000,800
		set output "err.png"
		plot "errori_lu.log" title "Errori LU" with lines,\
		"errori_lup.log" title "Errori LUP" with lines,\
		x**2 title "x^2",\
		0.5*x**2 title "(x^2)/2"
	' | gnuplot
}

gnuplot_distribution(){
	echo '
		set title "# Errori all'aumentare della taglia dell'istanza con pivot '$1'"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Numero di elementi > Epsilon"
		set terminal png size 1000,800
		set output "err-distrib-'$1'.png"
		plot "errori_lu-dist-'$1'.log" title "Errori LU" with lines,\
		"errori_lup-dist-'$1'.log" title "Errori LUP" with lines,\
		x**2 title "x^2",\
		0.5*x**2 title "(x^2)/2",\
		x title "x"
		' | gnuplot
} 

status(){
	sudo mount | grep davfs
}

case "$1" in
'lu_er')
	rm errori_lu.log >/dev/null 2>/dev/null
	lu $2
	;;
'lup_er')
	rm errori_lup.log >/dev/null 2>/dev/null
	lup $2
	;;
'er')
	clean
	lu $2
	lup $2
	;;
'ed')
	clean_distrib
	errors_distribution $2
	;;
'plot-ed')
	for precision in 1e-5 1e-4 1e-3; do 
		gnuplot_distribution $precision;
		display err-distrib-$precision.png
	done
	;;
'plot-er')
	gnuplot_error
	display err.png
	;;
*)
	usage
	;;
esac

exit 0
