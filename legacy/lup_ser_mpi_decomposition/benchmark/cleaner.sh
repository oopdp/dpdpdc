#! /bin/bash

#~ riga pulizia
#~ cat total_exec-4p.log |grep tt_max|cut -d'-' -f9

usage(){
	echo "Uso di lup-cleaner:"
	echo ""
	echo "	Parametri possibili:"
	echo "		exec: estrae dai file di log i tempi di esecuzione totali"
	echo "		comp: estrae dai file di log i tempi di computazione totali"
	echo "		comm: estrae dai file di log i tempi di comunicazione totali"
	echo ""
	echo "		g-exec: produce grafico di exec (richiede gnuplot e i file di log processati mediante $0 exec)"
	echo "		g-comp: produce grafico di comp (richiede gnuplot e i file di log processati mediante $0 comp)"
	echo "		g-comm: produce grafico di comm (richiede gnuplot e i file di log processati mediante $0 comm)"
	echo ""
	echo "		clean: pulisce dai file prodotti"
}

generate_execution(){
	echo "Generazione tempi elaborazione totali"
	for i in *.log;
	do
		echo "Elaborato" $i;
		cat $i |grep tt_max|cut -d'-' -f9 >$i.t_total;
		#~ echo "";
	done
}

generate_computation(){
	echo "Generazione tempi computazione"
	
	for i in *.log;
	do
		echo "Elaborato" $i;
		cat $i |grep te_max|cut -d'-' -f9 >$i.t_exec;
		#~ echo "";
	done
}

generate_communication(){
	echo "Generazione tempi comunicazione"
	
	for i in *.t_total;
	do
		comp=${i%t_total}t_exec
		#~ 
		#~ echo "Letto" $i
		#~ echo "Letto" $comp
		
		n_rows=$(wc -l $i | awk '{print $1}')
		row=0
		current_comm=0
		
		while [ $row -lt $n_rows ]; do
			let row+=1
			#~ current_exec="File: $i riga $row:"$(head -$row $i | tail -1)
			#~ current_comp="File: $comp riga $row:"$(head -$row $comp | tail -1)
			current_exec="$(head -$row $i | tail -1)"
			current_comp="$(head -$row $comp | tail -1)"
			#~ echo $current_exec
			#~ echo $current_comp
			current_comm=`echo "$current_exec-$current_comp"|bc -l`
			echo $current_comm >> ${i%t_total}t_comm
			#~ echo $current_comm
			#~ current_exec=$(head -$riga $i | tail -1)
			#~ current_comp=$(head -$riga $comp | tail -1)
		done
		#~ cat $i;
		#~ cat ${i%t_total}t_exec
		#~ echo "";
	done
}

generate_speedup(){
	config="01p 02p 04p 08p 16p 24p"
	taglia="100 200 300 400 500 600"
	row=1
	pivot=1
	for num_n in $taglia; do
		filename="total_exec_$num_n.speedup"
		echo "Generazione di " $filename
		for num_p in $config; do
			if [ $num_p = "01p" ]; then
				pivot=`cat *$num_p*|head -$row|tail -1`
			else

				current_value=`cat *$num_p*|head -$row|tail -1`
				echo $pivot $current_value >>$filename
			fi
		done
		let row+=1
		echo "Fatto"
		echo ""
	done
}

generate_speedup_intra(){
	config="1-4p 2-4p 4-4p"
	row=50
	pivot=1
	for num_n in `seq 200 200 1000`; do
		filename="total_exec_$num_n.speedup"
		echo "Generazione di " $filename
		for num_p in $config; do
			#~ echo "Leggo la riga " $row "da" `ls *$num_p*`
			if [ $num_p = "1-4p" ]; then
				pivot=`cat *$num_p*|head -$row|tail -1`
				echo $pivot $pivot >>$filename
			else

				current_value=`cat *$num_p*|head -$row|tail -1`
				echo $pivot $current_value >>$filename
			fi
		done
		let row+=50
		echo "Fatto"
		echo ""
	done
}

merge_files(){
	echo "Merging dei tempi per lo speed-up"
	
	first_file=`ls *.t_total|grep 01`
	#~ echo $first_file
	n_rows=$(wc -l "$first_file" | awk '{print $1}')
	row=0
	echo "#Comparazione tempi di esecuzione" >> total_exec.merged
	echo "#1	2	4	8	16	24" >> total_exec.merged
	while [ $row -lt $n_rows ]; do
		let row+=1
		for file in *.t_total; do
			#~ echo $file $row
			value="$(head -$row $file | tail -1)"
			current_row="$current_row $value"
		done
		echo $current_row >> total_exec.merged
		current_row=""
	done
}

histogram_data_generator(){
	a=0
	for file in *.t*; do
		let a+=1 
		value=`cat $file|tail -1`
		current_row="$current_row $value"
		if [ "$a" -eq 3 ]; then
			#~ echo $current_row
			let a=0;
			echo $current_row >>histogram_data.s500.1-24.merged
			current_row=""
        fi
        

		#~ echo $value
		
		
	done
}

gnuplot_files_merged(){
		echo '
		set title "Speedup per p=1,2,4,8,16,24 e n=100, 200, ..., 500"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Speedup"
		set style line 1 lt 2 lc rgb "red" lw 3
		set style line 2 lt 2 lc rgb "green" lw 3
		set style line 3 lt 2 lc rgb "blue" lw 3
		set terminal png size 1000,800
		set output "parallell-speedup-1-24.png"
		plot "total_exec.merged" using ($0*100):($1/$1) title "P=1" with lines ls 1,\
		"" using ($0*100):($1/$2) title "P=2" with lines ls 2,\
		"" using ($0*100):($1/$3) title "P=4" with lines ls 3, \
		"" using ($0*100):($1/$4) title "P=8" with lines,\
		"" using ($0*100):($1/$5) title "P=16" with lines,\
		"" using ($0*100):($1/$6) title "P=24" with lines \
		' | gnuplot
}


gnuplot_total(){
	echo '
		set title "Tempi di esecuzione per p=1,2,4 e n=4, 8, 16, ..., 1000"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-esecuzione.png"
		plot "total_exec_int-1-4p.log.t_total" using ($0*4):1 title "P=1" with lines,\
		"total_exec_int-2-4p.log.t_total" using ($0*4):1 title "P=2" with lines,\
		"total_exec_int-4-4p.log.t_total" using ($0*4):1 title "P=4" with lines \
		' | gnuplot
} 


gnuplot_comp(){
	echo '
		set title "Tempi di computazione per p=1,2,4 e n=4, 8, 16, ..., 1000"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-computazione.png"
		plot "total_exec_int-1-4p.log.t_exec" using ($0*4):1 title "P=1" with lines,\
		"total_exec_int-2-4p.log.t_exec" using ($0*4):1 title "P=2" with lines,\
		"total_exec_int-4-4p.log.t_exec" using ($0*4):1 title "P=4" with lines \
		' | gnuplot
} 

gnuplot_comm(){
	echo '
		set title "Tempi di comunicazione per p=1,2,4 e n=4, 8, 16, ..., 1000"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-comunicazione.png"
		plot 0.001 title "P=1" with lines,\
		"total_exec_int-2-4p.log.t_comm" using ($0*4):1 title "P=2" with lines,\
		"total_exec_int-4-4p.log.t_comm" using ($0*4):1 title "P=4" with lines \
		' | gnuplot
}


gnuplot_ext_total(){
	echo '
		set title "Tempi di esecuzione per p=1,2,4,8,16,24 e n=100, 200, ..., 500"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-esecuzione-1-24.png"
		plot "total_exec-1p.log.t_total" using ($0*100):1 title "P=1" with lines,\
		"total_exec-2p.log.t_total" using ($0*100):1 title "P=2" with lines,\
		"total_exec-4p.log.t_total" using ($0*100):1 title "P=4" with lines, \
		"total_exec-8p.log.t_total" using ($0*100):1 title "P=8" with lines, \
		"total_exec-16p.log.t_total" using ($0*100):1 title "P=16" with lines, \
		"total_exec-24p.log.t_total" using ($0*100):1 title "P=24" with lines \
		' | gnuplot
} 


gnuplot_ext_comp(){
	echo '
		set title "Tempi di computazione per p=1,2,4,8,16,24 e n=100, 200, ..., 500"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-computazione-1-24.png"
		plot "total_exec-1p.log.t_exec" using ($0*100):1 title "P=1" with lines,\
		"total_exec-2p.log.t_exec" using ($0*100):1 title "P=2" with lines,\
		"total_exec-4p.log.t_exec" using ($0*100):1 title "P=4" with lines, \
		"total_exec-8p.log.t_exec" using ($0*100):1 title "P=8" with lines, \
		"total_exec-16p.log.t_exec" using ($0*100):1 title "P=16" with lines, \
		"total_exec-24p.log.t_exec" using ($0*100):1 title "P=24" with lines \
		' | gnuplot
} 

gnuplot_ext_comm(){
	echo '
		set title "Tempi di comunicazione per p=1,2,4,8,16,24 e n=100, 200, ..., 500"
		set grid
		set xlabel "Taglia matrice (in righe)"
		set ylabel "Tempi di esecuzione (in secondi)"
		set terminal png size 1000,800
		set output "tempi-comunicazione-1-24.png"
		plot "total_exec-1p.log.t_comm" using ($0*100):1 title "P=1" with lines,\
		"total_exec-2p.log.t_comm" using ($0*100):1 title "P=2" with lines,\
		"total_exec-4p.log.t_comm" using ($0*100):1 title "P=4" with lines, \
		"total_exec-8p.log.t_comm" using ($0*100):1 title "P=8" with lines, \
		"total_exec-16p.log.t_comm" using ($0*100):1 title "P=16" with lines, \
		"total_exec-24p.log.t_comm" using ($0*100):1 title "P=24" with lines \
		' | gnuplot
}

g_hist(){
		echo '
			set xtics nomirror
			set ytics nomirror
			set boxwidth 0.5
			set style fill solid 1.00 border -1
			set xrange [-1.5:5.5]
			set terminal png size 1000,800
			set output "imp-comunicazione-1-24.png"
			set style fill solid 1 border -1 
			set xlabel "# Processori" 
			set ylabel "% temporale delle comunicazioni" 
			set title "Impatto delle comunicazioni con matrice n=600"
			plot "histogram_data.s500.1-24.merged" using ($3*100/$1):xticlabels(4) with boxes notitle
			' |  gnuplot
}


g_speedup(){
	echo '
		set title "Speedup per p=2,4,8,16,24 e n=100, 200, ..., 600"
		set xlabel "Numero di processori"
		set xtics ("2 p" 0, "4 p" 1, "8 p" 2, "16 p" 3, "24 p" 4)
		set xrange [-0.5:4.5]
		set key left top
		set grid
		set ylabel "Speedup"
		set terminal png size 1000,800
		set output "speedup-2-24.png"
		plot "total_exec_100.speedup" using ($1/$2) title "n=100" with lines, \
		"total_exec_200.speedup" using ($1/$2) title "n=200" with lines, \
		"total_exec_300.speedup" using ($1/$2) title "n=300" with lines, \
		"total_exec_400.speedup" using ($1/$2) title "n=400" with lines, \
		"total_exec_500.speedup" using ($1/$2) title "n=500" with lines, \
		"total_exec_600.speedup" using ($1/$2) title "n=600" with lines lw 3
		' | gnuplot
}

g_speedup_intra(){
	echo '
		set title "Speedup per p=2,4 e n=400, 600, ..., 1000"
		set xlabel "Numero di processori"
		set xtics ("1 p" 0, "2 p" 1, "4 p" 2, "6p" 3)
		set xrange [-0.5:2.5]
		set yrange [0.5:3.5]
		set key left top
		set grid
		set ylabel "Speedup"
		set terminal png size 1000,800
		set output "speedup-1-4.png"
		plot "total_exec_400.speedup" using ($1/$2) title "n=400" with lines, \
		"total_exec_600.speedup" using ($1/$2) title "n=600" with lines, \
		"total_exec_800.speedup" using ($1/$2) title "n=800" with lines, \
		"total_exec_1000.speedup" using ($1/$2) title "n=1000" with lines, \
		x*2 title "f(p) = p" lw 3, \
		x title "f(p) = p/2" lw 3
		' | gnuplot
}

case "$1" in
'clean')
	rm *.t_total *.t_exec *.t_comm >/dev/null 2>/dev/null
	rm *.png >/dev/null 2>/dev/null
	;;
'exec')
	generate_execution
	;;
'comp')
	generate_computation
	;;
'comm')
	generate_communication
	;;
'g-exec')
	gnuplot_total
	;;
'g-comp')
	gnuplot_comp
	;;
'g-comm')
	gnuplot_comm
	;;
'g-speedup')
	g_speedup
	;;
'g-speedup-intra')
	g_speedup_intra
	;;
'temp')
	gnuplot_ext_comm
	;;
'temp2')
	merge_files
	;;
'temp3')	
	gnuplot_files_merged
	;;
'temp4')	
	histogram_data_generator
	;;
'speedup')
	generate_speedup
	;;
'speedup-intra')
	generate_speedup_intra
	;;	
'g-hist')	
	g_hist
	;;
*)
	usage
	;;
esac

exit 0
