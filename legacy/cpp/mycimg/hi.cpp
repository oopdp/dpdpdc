#include "CImg.h"
using namespace cimg_library;

void item_mini_paint() {
  int xo = -1, yo = -1, x = -1, y = -1;
  bool redraw = true;
  CImg<unsigned char> img(256,256+64,1,3,0);
  unsigned char color[] = { 255, 255, 255 };
  cimg_for_inY(img,256,img.height()-1,yy) cimg_forX(img,xx) img.fillC(xx,yy,0,xx,(yy - 256)*4,(3*xx)%256);
  CImgDisplay disp(img.draw_text(5,5,"   ",color,color),"[#8] - Mini-Paint");
  while (!disp.is_closed() && !disp.is_keyQ() && !disp.is_keyESC()) {
    const unsigned int but = disp.button();
    redraw = false;
    xo = x; yo = y; x = disp.mouse_x(); y = disp.mouse_y();
    if (xo>=0 && yo>=0 && x>=0 && y>=0) {
      if (but&1 || but&4) {
        if (y<253) {
          const float tmax = (float)cimg::max(cimg::abs(xo - x),cimg::abs(yo - y)) + 0.1f;
          const int radius = (but&1?3:0) + (but&4?6:0);
          for (float t = 0; t<=tmax; ++t) img.draw_circle((int)(x + t*(xo - x)/tmax),(int)(y + t*(yo - y)/tmax),radius,color);
        }
        if (y>=256) {
          color[0] = img(x,y,0); color[1] = img(x,y,1); color[2] = img(x,y,2);
          img.draw_text(5,5,"   ",color,color);
        }
        redraw = true;
      }
      if (y>=253) y = 252;
      if (disp.button()&2) { img.draw_fill(x,y,color); redraw = true; }
    }
    if (redraw) disp.display(img);
    disp.resize(disp).wait();
    if (disp.key()) cimg_forC(img,k) { img.get_shared_rows(0,255,0,k).fill(0); img.display(disp); }
  }
}



int main() {
	item_mini_paint();
	return 0;
}
