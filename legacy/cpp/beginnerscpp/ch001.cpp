#include <iostream>
#include <cmath>
using namespace std;


namespace p {

	int *allocEmpty(int size)
	{
		int *list = new int[size];
		for(int i = 0; i < size; i++){
			list[i] = false;
		}
		return list;
	}

	void renderSpace(){
		cout << "**************" << endl;
	}

	void renderListQuickly(int *list, int size)
	{
		for(int i = 0; i < size; i++){
			cout << *(list + i) << " "; 
		}
		cout << endl;
	}

	void renderList(int *list, int size)
	{
		for(int i = 0; i < size; i++){
			cout << i << " - " << (list + i) << ": " << *(list + i) << endl;
		}
	}

	short isPrime(int pseudoPrime)
	{
		int countDivisisors = 0;
		int remainder;
		for(int i = 2; i < pseudoPrime; i++){
			remainder = (pseudoPrime % i);
			if (remainder == 0){
				countDivisisors++;
				break;
			}
		}
		return (countDivisisors == 0) ? 1 : -1;
	}

	

	int *primes(int upperBound, int *primesFoundOut)
	{
		int upperBoundLimit = upperBound + 1; // SQRT??
		int primesFound = 0;
		int i, k, m;
		int *compositeTable = p::allocEmpty(upperBoundLimit);
		for (m = 2; m < upperBoundLimit; m++) {
			if (!compositeTable[m]) {
				primesFound++;
				for (k = m + m; k < upperBoundLimit; k += m){
					compositeTable[k] = true;
				}
			}
		}
		int *primes = p::allocEmpty(primesFound);
		k = 0;
		for(i = 2; i < upperBoundLimit; i++){
			if (!compositeTable[i]) {
				primes[k] = i;
				k++; 
			}
		}
		delete compositeTable;
		*primesFoundOut = primesFound;
		return primes;
	}



	int *factors(int pseudoPrime)
	{		
		int primesFound = 0;
		cout << "Decomposing " << pseudoPrime << endl;
		int * primesList = primes(pseudoPrime, &primesFound);
		cout << "Using " << primesFound << " primes : " << endl;
		renderListQuickly(primesList, primesFound);
		int * factors = p::allocEmpty(primesFound);
		int decomposed = pseudoPrime;
		int remainder;
		int test = 1;
		for(int i = 0; i < primesFound; i++){
			while(decomposed > 1 && (decomposed % primesList[i]) == 0){
				decomposed = decomposed / primesList[i];
				factors[i]++;
			}
		}
		cout << "# divisions:" << endl;
		for(int i = 0; i < primesFound; i++){
			if(factors[i] > 0){
				cout << primesList[i] << " : " << factors[i] << endl;
				test = test * pow(primesList[i], factors[i]);
			}
		}
		cout << "G:" << test << " C:" << pseudoPrime << " G:" << pseudoPrime - test << endl;
	}

	
}

void testPrimalityWrapper(int pseudoPrime)
{
	int *pFactors;
	short primalityTest;
	cout << "Received " << pseudoPrime << endl;
	primalityTest = p::isPrime(pseudoPrime);
	if(primalityTest == 1){
		cout << pseudoPrime << " is a prime number" << endl;
	}
	else{
		cout << pseudoPrime << " isn't a prime number! " << endl;
		pFactors = p::factors(pseudoPrime);

	}
}

int main()
{
	int prime;
	p::renderSpace();	
	cout << "Please insert a supposedely prime number" << endl;
	cin >> prime;
	// prime = 152;
	p::renderSpace();
	testPrimalityWrapper(prime);
	return 0;
}

