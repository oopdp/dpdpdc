#include <iostream>
#include <string>
#include <cmath>

using namespace std;

template <class SomeType>
SomeType sum (SomeType a, SomeType b)
{
  return a+b;
}
    
namespace a{
	int function(){
		return 37;
	}

	int rubbish(){
		int a, b, result;
		int x = 0;
		int foo = 0;
		string aString = "A string";	
		
		string aString2 ("A string goes here");
		cout << aString2 << endl;
		cout << aString << endl;
		cout << x << endl;
		a = 3;
		b = 5;
		{
			int x;
			x = 50;
			cout << x << endl;
			int aVar;
			int anVar;
			aVar = 50;
			cout << aVar << endl;
			int *aRefToVar;
			aRefToVar = &aVar;
			anVar = aVar;
			cout << anVar << endl;
			cout << aRefToVar << endl;
			int again;
			again = *aRefToVar;
			cout << again << endl;
		}
		cout << x << endl;


		x = sum<int>(10,20);
		cout << x << endl;
		cout << "Hello World!" << endl;
		cout << "I am a c++ programme " << endl;
		result = a + b*b*b*b*b*b - a * a;
		cout << result << endl;
		cout << endl << endl << endl << endl;
		cout << a::function() << endl;
		int ar [5];
		for(int i = 0; i < 5; i++){
			ar[i] = i;
		}
		for(int i = 0; i < 5; i++){
			cout << i << endl;
		}
		return 0;
	}

	class Point {
		double x;
		double y;
	public:
		Point(){

		}
		Point(double x, double y)
		{
			this->x = x;
			this->y = y;
		}
		void setPosition(double x, double y)
		{
			this->x = x;
			this->y = y;
		} 

		double getX()
		{
			return x;
		}

		double getY()
		{
			return y;
		}

		string print()
		{
			return "(,)";
		}

	};

	class Edge {
		Point start;
		Point end;
	public:
		void setStart(Point p){
			start = p;
		}
		void setEnd(Point p){
			end = p;
		}	
		double size(){
			return sqrt(pow((start.getX() - end.getX()), 2) + pow((start.getY() - end.getY()), 2));
		}
	};
}


int a::function();


int main()
{

	cout << sizeof(long) << endl;

	a::Point * p;
	p = new a::Point (1, 4);
	a::Point p1 (0,0);
	a::Point p2 (0.5,2);
	a::Edge e;
	e.setStart(p1);
	e.setEnd(p2);
	cout << e.size() << endl;

	try {
		long* c = new long[1000000000];
	} 
	catch (std::bad_alloc&) {
	  // Handle error
		cout << "BAD" << endl;
	}
	return 0;
}
